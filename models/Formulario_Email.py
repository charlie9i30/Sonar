# encoding=utf8
import sys
import os
import csv
import pandas as pd
import sonar
from colorama import Fore, Back, Style
import time
from tqdm import tqdm
from utils import Funcoes
from utils import DataFrame
from datetime import date, datetime, timedelta
import imaplib
import base64
import email
from dateutil import parser
import pathlib




class Formulario(object):
    global Flag1;
    global df;

    Flag1 = Funcoes.Flag()
    df = DataFrame.DB()
    def radar(self, mapeado):
        # Verifica se há arquivo novo...
        arquivo = 'Relatorio-Zimbra ' + datetime.today().strftime('%d%m%y')

        flag = Funcoes.Flag()
        dir_path = mapeado
        if not os.path.exists(dir_path + '\\' + arquivo + '.csv'):
            self.baixaremail(mapeado,arquivo)
        if os.path.exists(dir_path):
            for entry in tqdm(os.listdir(dir_path)):
                if entry.replace('.csv', '') == arquivo:
                    self.listarbanco(mapeado + '\\' + arquivo)
                elif entry.replace('.csv', '') != arquivo:
                    continue



    def baixaremail(self,mapeado,arquivo):

        email_user = "develop@gruposerval.com.br"
        email_pass = "!@Serval1970@!"
        mail = imaplib.IMAP4_SSL("mail.gruposerval.com.br")

        mail.login(email_user.__str__(), email_pass.__str__())
        mail.list()
        mail.select('Inbox')
        datestring = ""
        latest_email_uid = ""

        type, data = mail.search(None, '(FROM "admin@gruposerval.com.br")')

        for num in data[0].split():
            typ, data = mail.fetch(num, '(RFC822)')
            raw_email = data[0][1]

            raw = email.message_from_bytes(data[0][1])
            datestring = raw['date']
            datestring = parser.parse(datestring[:31])
            datestring = datestring.strftime("%d%m%y")

            if (datetime.today().strftime('%d%m%y') == datestring):
                # Converter todos os bytes em uma string literal removendo b''
                raw_email_string = raw_email.decode('utf-8')
                email_message = email.message_from_string(raw_email_string)

                # Baixando Anexos
                for part in email_message.walk():
                    # Pulando conteúdo até chegar nos anexos...
                    if part.get_content_maintype() == 'multipart':
                        continue
                    if part.get('Content-Disposition') is None:
                        continue
                    fileName = part.get_filename()
                    if bool(fileName):
                        filePath = os.path.join(mapeado, arquivo + '.csv')
                        if not os.path.isfile(filePath):
                            fp = open(filePath, 'wb')
                            fp.write(part.get_payload(decode=True))
                            fp.close()
            else:
                continue

        if (datetime.today().strftime('%d%m%y') == datestring):
            self.listarbanco(mapeado + '\\' + arquivo)

    def listarbanco(self, arq):
        arq = arq + '.csv'
        if not os.path.exists(arq):
            os.rename(arq, arq + '.csv')
        # Verifique se o arquivo CSV existe
        if os.path.exists(arq):
            # --------------------------------------------------
            # Banco de Dados - Forms Opções
            df_bd = pd.DataFrame(df.select_formsopcoes_113())
            df_sobra = pd.DataFrame(df.select_formsregistrosrespostas113_sobra())

            codigos = "0"
            if len(df_sobra) > 0:
                for i in df_sobra['registro_id']:
                    if len(codigos) == 1:
                        codigos = i.__str__() + ','
                    else:
                        codigos = codigos + i.__str__() + ','

                df.delete_formsregistrosrespostas113_sobra(codigos[:-1])
                df.delete_formsregistros113_sobra

            # --------------------------------------------------
            # Arquivo .csv
            df_csv = pd.read_csv(arq)
            # --------------------------------------------------
            # Apenas o Domínio @gruposerval.com.br
            df_csv = df_csv[df_csv['Nome da Conta'].str.contains('@gruposerval.com.br', na=False)]
            df_bd = df_bd[df_bd['item_nome'].str.contains('@gruposerval.com.br', na=False)]

            try:
                #INSERE OS REGISTROS QUE ESTÃO NO ARQUIVO E NÃO ESTÃO NA LISTA DE OPÇÕES
                for linha in range(len(df_csv.index)):
                    if not df_csv['Nome da Conta'].iloc[linha] in df_bd.item_nome.values and df_csv['Status'].iloc[linha] == 'active':
                        try:
                            login_rede = df_csv['Nome da Conta'].iloc[linha].split('@')
                            login_rede = login_rede[0]
                            nome_exibicao = df_csv['Nome de Exibicao'].iloc[linha]
                            caixa_atual = df_csv['Tamanho da Caixa Atual'].iloc[linha].replace('(', '').replace(')', '')
                            cota = df_csv['Cota'].iloc[linha].replace('(', '').replace(')', '')
                            email = df_csv['Nome da Conta'].iloc[linha]
                        except:
                            continue

                        df.insert_registro_formsopcoes113(df_csv['Nome da Conta'].iloc[linha])

                        df.insert_registro_forms113()

                        item_id_opcoes = pd.DataFrame(df.select_id_formsopcoes113(df_csv['Nome da Conta'].iloc[linha]))
                        item_id_opcoes = item_id_opcoes.loc[0, 'id']
                        registro_id = pd.DataFrame(df.select_id_formsregistros113())
                        registro_id = registro_id.loc[0, 'id']

                        df.insert_registrosrespostas_form113(registro_id,nome_exibicao,email,login_rede,caixa_atual,cota,item_id_opcoes)

                        msg = '     -> Email Cadastrado!'
                        # extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                        if Flag1.visualizarEmail == 1:
                            print(Flag1.linha_separador(" ", "       " + nome_exibicao.__str__() +" "+ email.__str__() + " ", 1))
                            print(Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + msg)

                df_bd_form = pd.DataFrame(df.select_registros_respostas_form113())

                # INSERE OS ARQUIVOS QUE ESTÃO NA LISTA DE OPÇÕES MAS NÃO ESTÃO NO FORMULÁRIO
                for linha in range(len(df_bd.index)):
                    if not df_bd['item_nome'].iloc[linha] in df_bd_form.email.values:
                        df_registro = df_csv[df_csv['Nome da Conta'].str.contains(df_bd['item_nome'].iloc[linha], na=False)]
                        try:
                            login_rede = df_registro.iloc[0]['Nome da Conta'].split('@')
                            login_rede = login_rede[0]
                            nome_exibicao = df_registro.iloc[0]['Nome de Exibicao']
                            caixa_atual = df_registro.iloc[0]['Tamanho da Caixa Atual'].replace('(', '').replace(')','')
                            cota = df_registro.iloc[0]['Cota'].replace('(', '').replace(')', '')
                            email = df_registro.iloc[0]['Nome da Conta']
                        except:
                            continue

                        df.insert_registro_forms113()

                        item_id_opcoes = pd.DataFrame(df.select_id_formsopcoes113(df_registro.iloc[0]['Nome da Conta']))
                        item_id_opcoes = item_id_opcoes.loc[0, 'id']
                        registro_id = pd.DataFrame(df.select_id_formsregistros113())
                        registro_id = registro_id.loc[0, 'id']

                        df.insert_registrosrespostas_form113(registro_id, nome_exibicao, email, login_rede, caixa_atual,cota, item_id_opcoes)
                        msg = '     -> Email Cadastrado!'
                        # extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                        if Flag1.visualizarEmail == 1:
                            print(Flag1.linha_separador(" ",
                                                        "       " + nome_exibicao.__str__() + " " + email.__str__() + " ",
                                                        1))
                            print(Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + msg)

                df_bd_form = pd.DataFrame(df.select_registros_respostas_form113())

                # ATUALIZA OS REGISTROS QUE ESTÃO NO ARQUIVO E SALVOS NO FORMULÁRIO
                for linha in range(len(df_csv.index)):
                    if df_csv['Nome da Conta'].iloc[linha] in df_bd_form.email.values:
                        df_registro = df_bd_form[df_bd_form['email'].str.contains(df_csv['Nome da Conta'].iloc[linha], na=False)]
                        registro_id = df_registro.iloc[0]['id']
                        status_csv = df_csv['Status'].iloc[linha]
                        caixa_atual = df_csv['Tamanho da Caixa Atual'].iloc[linha].replace('(', '').replace(')', '')
                        cota = df_csv['Cota'].iloc[linha].replace('(', '').replace(')', '')
                        nome_exibicao = df_csv['Nome de Exibicao'].iloc[linha]

                        df_resposta = pd.DataFrame(df.select_registros_respostas_by_id(registro_id))

                        for repostalinha in range(len(df_resposta.index)):
                            if(df_resposta['campo_id'].iloc[repostalinha] == 2063 and df_resposta['resposta_texto'].iloc[repostalinha] == 'Ativo' and status_csv == 'closed'):
                                df.update_registros_respostas_form113_status_desativar(registro_id)
                            elif (df_resposta['campo_id'].iloc[repostalinha] == 2063 and df_resposta['resposta_texto'].iloc[repostalinha] == 'Inativo' and status_csv == 'active'):
                                df.update_registros_respostas_form113_status_ativar(registro_id)
                            elif(df_resposta['campo_id'].iloc[repostalinha] == 1976):
                                df.update_registros_respostas_form113_nome_cota(registro_id,cota,caixa_atual,nome_exibicao)
                            else:
                                continue

            except df_bd.ErSror as err:
                # notifica.clear()
                msg = ' -> Opa problema! :(' + err + ')'
                # extrato.write(str(msg) + '\n')
                print(f"Erro: '{err}'")




    def executeFormularioEmail(self,mapeado):
        while True:
            now = datetime.now()
            agora = int(now.strftime("%H"))
            if Flag1.ini <= agora <= Flag1.fim:
                print()
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag1.linha_separador("<"," Iniciando - Formulário Email ",1))
                inicio = time.perf_counter()
                self.radar(mapeado)
                fim = time.perf_counter()
                total = round(fim - inicio, 2)
                print(f'Tempo de execução: {total} segundos')
                print()
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag1.linha_separador(">", " Concluído - Formulário Email ",3))
            else:
                time.sleep(43200)