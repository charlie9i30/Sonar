from utils import DataFrame
from utils import Funcoes
from colorama import Fore, Back, Style
import time
import threading

class ArquivoFolha(object):
    global Flag;
    global df;


    Flag = Funcoes.Flag()
    df = DataFrame.DB()
    def processoArquivoFolha(self):
        listaProcessos = df.select_processo_folha()
        BLO = 0
        listaExecucao = []
        if(len(listaProcessos)> 0):
            matricula = listaProcessos['matricula'].iloc[0] if listaProcessos['matricula'].iloc[0] is not None else ''

        for linha in range(len(listaProcessos.index)):
            print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador("<", " Iniciando Arquivo Folha ",1))
            listaProcessosLinha = listaProcessos.iloc[linha]
            if(listaProcessosLinha['processo']=="arquivo_vt_va"):
                listaBuffer = df.select_buffer_vt_va(listaProcessosLinha['usuario_id'].__str__(),
                                                     listaProcessosLinha['coligada_id'].__str__())
            else:
                listaBuffer = df.select_buffer_folha(listaProcessosLinha['usuario_id'].__str__(),
                                                     listaProcessosLinha['coligada_id'].__str__())

            if(len(listaBuffer)>0):
                usuario_id = listaBuffer['usuario_id'].iloc[0]
                coligada_id = listaBuffer['coligada_id'].iloc[0]
                atual = listaBuffer['atual'].iloc[0]
                bloco = int(listaBuffer['bloco'].iloc[0])
                total = listaBuffer['total'].iloc[0]

                processo = listaProcessosLinha['processo']

                if(usuario_id > 0 and bloco >= 0):
                    df.update_processos_folha(listaProcessos['id'].iloc[linha].__str__())
                    if(processo == "arquivo_folha_1" or processo == "arquivo_folha_variaveis" or processo == "arquivo_vt_va"):
                        if(total <= 100):
                            th = threading.Thread(target=self.executaProcesso, args=[usuario_id, 0, total, total, BLO, processo, matricula,coligada_id])
                            th.daemon = True
                            th.start()
                            th.join()
                        else:
                            for i in range(atual-1,total,bloco):
                                BLO += 1
                                th1 = threading.Thread(target=self.executaProcesso, args=[usuario_id,i+1,i+bloco,total,BLO,processo,matricula,coligada_id])
                                th1.daemon = True
                                th1.start()
                    else:
                        th2 = threading.Thread(target=self.executaProcesso,args=[usuario_id, 0, 0 + bloco, total, BLO, processo, matricula, coligada_id])
                        th2.daemon = True
                        th2.start()
                        th2.join()
            '''else:
                df.delete_processos_usuariosprocessos(listaProcessos['id'].iloc[linha].__str__())'''

    def executaProcesso(self,usuario_id,row_ini,row_fim,total,BLO,processo,matricula,coligada):
        if(row_ini == 0 and BLO > 1 and len(matricula) == 0):
            return

        if(processo != "arquivo_folha_1" and processo != "arquivo_folha_variaveis" and processo != "arquivo_vt_va"):
            comando = processo
            Flag.conexaoV(comando,' Folha-> ')
        else:
            if(processo == "arquivo_folha_variaveis"):
                if(len(matricula)==0):
                    comando = "SET NOCOUNT ON; EXEC proc_txt_arquivofolha_variaveis_blocos_coligadas "+ usuario_id.__str__() +","+ row_ini.__str__() +","+ row_fim.__str__() +",NULL,"+coligada.__str__()+";"
                    Flag.conexaoV(comando,' Folha-> ')
                else:
                    comando = "SET NOCOUNT ON; EXEC proc_txt_arquivofolha_variaveis_blocos_coligadas " + usuario_id.__str__() + "," + row_ini.__str__() + "," + row_fim.__str__() + ",'"+ matricula.__str__() +"'," + coligada.__str__() + ";"
                    Flag.conexaoV(comando,' Folha-> ')
            elif(processo == "arquivo_folha_1"):
                if (len(matricula) == 0):
                    comando = "SET NOCOUNT ON; EXEC proc_txt_arquivofolha_blocos_coligadas " + usuario_id.__str__() + "," + row_ini.__str__() + "," + row_fim.__str__() + ",NULL," + coligada.__str__() + ";"
                    Flag.conexaoV(comando,' Folha-> ')
                else:
                    comando = "SET NOCOUNT ON; EXEC proc_txt_arquivofolha_blocos_coligadas " + usuario_id.__str__() + "," + row_ini.__str__() + "," + row_fim.__str__() + ",'" + matricula.__str__() + "'," + coligada.__str__() + ";"
                    Flag.conexaoV(comando,' Folha-> ')
            elif(processo == "arquivo_vt_va"):
                    comando = "SET NOCOUNT ON; EXEC proc_txt_vt_va_blocos " + usuario_id.__str__() + "," + coligada.__str__() + "," + row_ini.__str__() + "," + row_fim.__str__() + ";"
                    Flag.conexaoV(comando,' Folha (VT/VA)-> ')





    def executeFolhaPag(self):
        df.update_processos_restartfolha()
        while True:
            print()
            print(
                Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador("<", " Iniciando Verificação do Arquivo Folha ",
                                                                                      1))
            self.processoArquivoFolha()
            print(
                Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador("<"," Finalizando Arquivo Folha ",1))
            time.sleep(10)
