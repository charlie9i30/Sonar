import os
import sys
import sonar
import pandas as pd
import time
import re
from tqdm import tqdm
from colorama import Fore, Back, Style
from utils import Funcoes, DataFrame
from datetime import date, datetime, timedelta


class ListarDiretorio(object):
    global stop_rodando_folha;
    global Flag1;
    global special_characters;

    special_characters = '!@#$%^&*()-+?_=,<>/""�';

    Flag1 = Funcoes.Flag()

    def update_progress_bar(self):
        print('\b.', sys.stdout.flush())

    def radar(self, mapeado):
        flag = Funcoes.Flag()
        hoje = datetime.today().strftime('%d/%m/%Y %H:%M:%S')
        minutos_Folha_Ponto = datetime.strptime(hoje, '%d/%m/%Y %H:%M:%S')

        coligada = flag.coligadas_operacional()
        # --------------------------------------------------
        pr = pd.period_range(start='2022-10-01', end=date.today(), freq='M')

        tempo = list(datetime.strptime(
            period.day.__str__().zfill(2) + '-' + period.month.__str__().zfill(
                2) + '-' + period.year.__str__().zfill(4),
            '%d-%m-%Y').date() for period in pr)
        # T1 = date.today()
        # T0 = (T1 + timedelta(days=30))
        # T2 = (T1 - timedelta(days=30))
        # T3 = (T2 - timedelta(days=30))
        # T4 = (T3 - timedelta(days=30))
        # ++++++++++++++++++++++++++++++++++++++++++++++++++
        # tempo = (T4, T3, T2, T1, T0)
        tempo_inicial = datetime.now()
        arq_mais_recente_fo = tempo_inicial + timedelta(days=-365)

        for empresa in coligada:
            map = mapeado + coligada[empresa]
            for mes_atual in tempo:
                cam = map + '\\ANO ' + mes_atual.year.__str__() + '\\' + 'MES ' + mes_atual.month.__str__().zfill(
                    2) + ' ' + flag.nome_mes(
                    mes_atual.month).upper()
                dir_path = cam
                # print(flag.linha_separador(">", '', 2))
                # print(flag.linha_separador(" ", " " + dir_path.__str__() + " ", 2))

                if os.path.exists(dir_path):
                    # print('Listando...')
                    print(flag.linha_separador(" ", "-> FOLHAS DE PONTO: " + coligada[
                        empresa].__str__() + " -> " + flag.nome_mes(
                        mes_atual.month).upper().__str__() + "/" + mes_atual.year.__str__() + " ", 1))

                    for entry in tqdm(os.listdir(dir_path)):
                        time.sleep(0.02)
                        path = os.path.join(dir_path, entry)
                        stats = os.stat(path)
                        arquivo = datetime.fromtimestamp(stats.st_atime)
                        c = arquivo - arq_mais_recente_fo
                        if c.days >= 0 and c.seconds >= 0:
                            arq_mais_recente_fo = arquivo



                else:
                    continue

        # arq_corte = (datetime.now() + timedelta(minutes=-flag.minutos_Folha_Ponto))
        arq_minutes_final = arq_mais_recente_fo - minutos_Folha_Ponto
        arq_minutes = divmod(arq_minutes_final.total_seconds(), 60)

        if (arq_mais_recente_fo < minutos_Folha_Ponto):
            print('         Tempo referência : ' + minutos_Folha_Ponto.__format__('%d/%m/%Y %H:%M:%S').__str__())
            print('     Arquivo mais recente : ' + arq_mais_recente_fo.__format__(
                '%d/%m/%Y %H:%M:%S').__str__() + '. Processando...')
            minutos_Folha_Ponto = arq_mais_recente_fo
            self.listarbanco(mapeado)
        else:
            print('     Data último arquivo : ' + arq_mais_recente_fo.__format__('%d/%m/%Y %H:%M:%S').__str__())

    def radar2023(self, mapeado):
        flag = Funcoes.Flag()
        hoje = datetime.today().strftime('%d/%m/%Y %H:%M:%S')
        minutos_Folha_Ponto = datetime.strptime(hoje, '%d/%m/%Y %H:%M:%S')

        coligada = flag.coligadas_operacional()
        # --------------------------------------------------
        pr = pd.period_range(start='2023-01-01', end='2023-12-31', freq='M')

        tempo = list(datetime.strptime(
            period.day.__str__().zfill(2) + '-' + period.month.__str__().zfill(
                2) + '-' + period.year.__str__().zfill(4),
            '%d-%m-%Y').date() for period in pr)
        # T1 = date.today()
        # T0 = (T1 + timedelta(days=30))
        # T2 = (T1 - timedelta(days=30))
        # T3 = (T2 - timedelta(days=30))
        # T4 = (T3 - timedelta(days=30))
        # ++++++++++++++++++++++++++++++++++++++++++++++++++
        # tempo = (T4, T3, T2, T1, T0)
        tempo_inicial = datetime.now()
        arq_mais_recente_fo = tempo_inicial + timedelta(days=-365)

        for empresa in coligada:
            map = mapeado + coligada[empresa]
            for mes_atual in tempo:
                cam = map + '\\ANO ' + mes_atual.year.__str__() + '\\' + 'MES ' + mes_atual.month.__str__().zfill(
                    2) + ' ' + flag.nome_mes(
                    mes_atual.month).upper()
                dir_path = cam
                # print(flag.linha_separador(">", '', 2))
                # print(flag.linha_separador(" ", " " + dir_path.__str__() + " ", 2))

                if os.path.exists(dir_path):
                    # print('Listando...')
                    print(flag.linha_separador(" ", "-> FOLHAS DE PONTO: " + coligada[
                        empresa].__str__() + " -> " + flag.nome_mes(
                        mes_atual.month).upper().__str__() + "/" + mes_atual.year.__str__() + " ", 1))

                    for entry in tqdm(os.listdir(dir_path)):
                        time.sleep(0.02)
                        path = os.path.join(dir_path, entry)
                        stats = os.stat(path)
                        arquivo = datetime.fromtimestamp(stats.st_atime)
                        c = arquivo - arq_mais_recente_fo
                        if c.days >= 0 and c.seconds >= 0:
                            arq_mais_recente_fo = arquivo



                else:
                    continue

        # arq_corte = (datetime.now() + timedelta(minutes=-flag.minutos_Folha_Ponto))
        arq_minutes_final = arq_mais_recente_fo - minutos_Folha_Ponto
        arq_minutes = divmod(arq_minutes_final.total_seconds(), 60)

        if (arq_mais_recente_fo < minutos_Folha_Ponto):
            print('         Tempo referência : ' + minutos_Folha_Ponto.__format__('%d/%m/%Y %H:%M:%S').__str__())
            print('     Arquivo mais recente : ' + arq_mais_recente_fo.__format__(
                '%d/%m/%Y %H:%M:%S').__str__() + '. Processando...')
            minutos_Folha_Ponto = arq_mais_recente_fo
            self.listarbanco2023(mapeado)
        else:
            self.stop_rodando_folha = [False, True]
            print('     Data último arquivo : ' + arq_mais_recente_fo.__format__('%d/%m/%Y %H:%M:%S').__str__())

    def radar2024(self, mapeado):
        flag = Funcoes.Flag()
        hoje = datetime.today().strftime('%d/%m/%Y %H:%M:%S')
        minutos_Folha_Ponto = datetime.strptime(hoje, '%d/%m/%Y %H:%M:%S')

        coligada = flag.coligadas_operacional()
        # --------------------------------------------------
        pr = pd.period_range(start='2024-01-01', end=date.today(), freq='M')

        tempo = list(datetime.strptime(
            period.day.__str__().zfill(2) + '-' + period.month.__str__().zfill(
                2) + '-' + period.year.__str__().zfill(4),
            '%d-%m-%Y').date() for period in pr)
        # T1 = date.today()
        # T0 = (T1 + timedelta(days=30))
        # T2 = (T1 - timedelta(days=30))
        # T3 = (T2 - timedelta(days=30))
        # T4 = (T3 - timedelta(days=30))
        # ++++++++++++++++++++++++++++++++++++++++++++++++++
        # tempo = (T4, T3, T2, T1, T0)
        tempo_inicial = datetime.now()
        arq_mais_recente_fo = tempo_inicial + timedelta(days=-365)

        for empresa in coligada:
            map = mapeado + coligada[empresa]
            for mes_atual in tempo:
                cam = map + '\\ANO ' + mes_atual.year.__str__() + '\\' + 'MES ' + mes_atual.month.__str__().zfill(
                    2) + ' ' + flag.nome_mes(
                    mes_atual.month).upper()
                dir_path = cam
                # print(flag.linha_separador(">", '', 2))
                # print(flag.linha_separador(" ", " " + dir_path.__str__() + " ", 2))

                if os.path.exists(dir_path):
                    # print('Listando...')
                    print(flag.linha_separador(" ", "-> FOLHAS DE PONTO: " + coligada[
                        empresa].__str__() + " -> " + flag.nome_mes(
                        mes_atual.month).upper().__str__() + "/" + mes_atual.year.__str__() + " ", 1))

                    for entry in tqdm(os.listdir(dir_path)):
                        time.sleep(0.02)
                        path = os.path.join(dir_path, entry)
                        stats = os.stat(path)
                        arquivo = datetime.fromtimestamp(stats.st_atime)
                        c = arquivo - arq_mais_recente_fo
                        if c.days >= 0 and c.seconds >= 0:
                            arq_mais_recente_fo = arquivo



                else:
                    continue

        # arq_corte = (datetime.now() + timedelta(minutes=-flag.minutos_Folha_Ponto))
        arq_minutes_final = arq_mais_recente_fo - minutos_Folha_Ponto
        arq_minutes = divmod(arq_minutes_final.total_seconds(), 60)

        if (arq_mais_recente_fo < minutos_Folha_Ponto):
            print('         Tempo referência : ' + minutos_Folha_Ponto.__format__('%d/%m/%Y %H:%M:%S').__str__())
            print('     Arquivo mais recente : ' + arq_mais_recente_fo.__format__(
                '%d/%m/%Y %H:%M:%S').__str__() + '. Processando...')
            minutos_Folha_Ponto = arq_mais_recente_fo
            self.listarbanco2023(mapeado)
        else:
            self.stop_rodando_folha = [False, True]
            print('     Data último arquivo : ' + arq_mais_recente_fo.__format__('%d/%m/%Y %H:%M:%S').__str__())

    def listarbanco(self, mapeado):
        self.stop_rodando_folha = [True, False]
        flag = Funcoes.Flag()
        with open(mapeado + 'Folhas_de_Ponto.txt', 'w') as extrato:
            try:
                extrato.write(
                    str('-> FOLHAS DE PONTO: Dados para análise e correção:' + '\nIniciado as ' + datetime.today().strftime(
                        '%d/%m/%Y %H:%M:%S').__str__()) + '\n')
                notifica = list()
                coligada = flag.coligadas_operacional()
                linha_cadastrada = ""
                # --------------------------------------------------
                df = DataFrame.DB()
                # Removendo os formulário incompletos....

                df_sobra = pd.DataFrame(df.select_formsregistrosrespostas45_sobra())
                codigos="0"
                if len(df_sobra)>0:
                    for i in df_sobra['registro_id']:
                        if len(codigos)==1:
                            codigos = i.__str__() + ','
                        else:
                            codigos = codigos + i.__str__() + ','

                    df.delete_formsregistrosrespostas45_sobra(codigos[:-1])
                    df.delete_formsregistros45_sobra

                df_bd = pd.DataFrame(df.select_formsregistrosrespostas45())
                # print(df_bd.describe())
                # --------------------------------------------------

                date_str = '01-12-2022'
                date_object = datetime.strptime(date_str, '%d-%m-%Y').date()
                T1 = date_object
                T2 = (T1 - timedelta(days=30))
                pr1 = pd.period_range(start='2022-11-01',end='2022-12-31',freq='M')
                # ++++++++++++++++++++++++++++++++++++++++++++++++++
                tempo = (T2, T1)
                # Formato Antigo.... Foca no Mês/Ano inicial do período

                for empresa in coligada:
                    totalJaCadastrado = 0
                    totalRetornoAtualizado = 0
                    totalCadastroRealizado = 0
                    totalForadoPadraoQRCODE = 0
                    totalDuplicado = 0
                    map = mapeado + coligada[empresa]
                    # print(flag.linha_separador(" ", " " + coligada[empresa].__str__() + " ", 2))
                    # print(tempo)
                    j = -1
                    for mes_atual in tempo:
                        j += 1
                        msg = "->(1º) FOLHAS DE PONTO: " + coligada[empresa].__str__() + " -> " + flag.nome_mes(mes_atual.month).upper().__str__() + "/" + mes_atual.year.__str__()
                        extrato.write(str(msg) + '\n')
                        print(flag.linha_separador(">", '', 2))
                        print(flag.linha_separador(" ", msg, 1))
                        # print(flag.linha_separador("_", '', 2))
                        cam = map + '\\ANO ' + mes_atual.year.__str__() + '\\' + 'MES ' + flag.mes(mes_atual.month)
                        caminhos = cam
                        # Preparando o Dataframe para o Período atual
                        #df_bd_atual = df_bd.loc[df_bd['periodo'] == flag.nome_mes(
                        #    mes_atual.month).__str__() + "-" + mes_atual.year.__str__()]

                        df_bd_atual = df_bd[df_bd['data2'].str.contains(pr1[j].__str__(), na=False)]

                        if os.path.exists(caminhos):
                            for diretorio, subpastas, arquivos in os.walk(caminhos):
                                for arquivo in tqdm(sorted(arquivos)):
                                    time.sleep(0.02)
                                    cadastro_ok = 0
                                    # print(diretorio + ' # ' + arquivo)
                                    tabela_arquivo = arquivo.replace("-", "_").replace(".pdf","_").replace("a", "_").split('_')
                                    # print(flag.linha_separador("_","       " + tabela_arquivo.__str__() + " ",1))
                                    if(tabela_arquivo[0] == 'Thumbs.db'):
                                        continue

                                    if len(tabela_arquivo) >= 9:
                                        if tabela_arquivo[5].__eq__(flag.numero_mes(flag.nome_mes(mes_atual.month))):
                                            try:
                                                codcliente_arq = tabela_arquivo[0].__str__()
                                                codlot_arq = tabela_arquivo[1].__str__()
                                                matricula_arq = tabela_arquivo[2].__str__()
                                                colaborador_arq = tabela_arquivo[3].__str__()
                                                ano_ini_arq = tabela_arquivo[4].__str__()
                                                mes_ini_arq = tabela_arquivo[5].__str__()
                                                dia_ini_arq = tabela_arquivo[6].__str__()
                                                ano_fim_arq = tabela_arquivo[7].__str__()
                                                mes_fim_arq = tabela_arquivo[8].__str__()
                                                dia_fim_arq = tabela_arquivo[9].__str__()
                                            except IndexError:
                                                totalForadoPadraoQRCODE += 1
                                                msg = '     -> Fora do Padrão QRCODE'
                                                extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                if flag.visualizarFP == 1:
                                                    print(flag.linha_separador("_",
                                                                               "       " + tabela_arquivo.__str__() + " ",
                                                                               1))
                                                    print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                    continue

                                            linha_arquivo = empresa.__str__() + '-' + \
                                                            tabela_arquivo[2].__str__() + '-' + \
                                                            tabela_arquivo[7].__str__() + '/' + \
                                                            tabela_arquivo[8].__str__()
                                            mes = mes_atual.month.__str__()
                                            ano = mes_atual.year.__str__()
                                            if len(dia_ini_arq)!=2 \
                                            or len(dia_fim_arq)!=2 \
                                            or len(ano_ini_arq)!=4 \
                                            or len(ano_fim_arq)!=4 \
                                            or ano_fim_arq!=ano \
                                            or int(mes_fim_arq).__str__()!=mes:
                                                totalForadoPadraoQRCODE += 1
                                                msg = '     -> Fora do Padrão QRCODE OU PERÍODO INCOERENTE COM A PASTA'
                                                extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                if flag.visualizarFP == 1:
                                                    print(flag.linha_separador("_", "       " + tabela_arquivo.__str__() + " ", 1))
                                                    print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                continue

                                            # Para confronto
                                            # Verifica se já existe no BD

                                            if(linha_arquivo in df_bd_atual.indiceII.values):
                                                tabela_bd = df_bd_atual[df_bd_atual['indiceII'].str.contains(linha_arquivo.__str__(), na = False)]
                                                tabela_bd = tabela_bd['indice'].iloc[0].__str__().replace("-", "_").replace("a", "_").split('#')

                                                cod = tabela_bd[0]
                                                tabela_bd_II = tabela_bd[1].split('_')

                                                totalJaCadastrado += 1
                                                cadastro_ok = 1
                                                df_bd_duplicado = None
                                                # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + '     -> Arquivo já cadastrado!')
                                                if tabela_bd_II[11].__str__() != "OK":
                                                    # Resposta 421 Retorno da Pasta!
                                                    totalRetornoAtualizado += 1
                                                    # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + '     -> Retorno Atualizado!')
                                                    df.update_formsregistrosrespostas45(cod)

                                                continue

                                            # Inserindo registro
                                            if cadastro_ok == 0:
                                                cadastro_ok = 1
                                                valor_id = 0
                                                linha_cadastrada = linha_arquivo
                                                # -----------------------------------------------------------------------------------
                                                # Realizar cadastro do Registro
                                                try:
                                                    # Vamos validar se o período está coerente com o que foi impresso
                                                    matricula = tabela_arquivo[2]
                                                    periodo = tabela_arquivo[6].__str__() + '/' + \
                                                              tabela_arquivo[5].__str__() + '/' + \
                                                              tabela_arquivo[4].__str__() + ' - ' + \
                                                              tabela_arquivo[9].__str__() + '/' + \
                                                              tabela_arquivo[8].__str__() + '/' + \
                                                              tabela_arquivo[7].__str__()
                                                    func_imp = pd.DataFrame(df.select_impressao(periodo, matricula, empresa))

                                                    if any(c in special_characters for c in colaborador_arq):
                                                        tabela_arquivo[3] = re.sub(r"[^a-zA-Z0-9 ]", "", colaborador_arq)
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Carácter especial no nome do colaborador, impossibilitando inserção'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue
                                                    if len(periodo) != 23:
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Problema nas datas do período'
                                                        extrato.write(str(tabela_arquivo.__str__())+' ' + str(msg)+'\n')
                                                        if flag.visualizarFP==1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    # Buscando Posto do Arquivo e Montando o valor Para Inserção no campo de Resposta
                                                    posto_id = tabela_arquivo[1]

                                                    if len(posto_id) == 0:
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Posto não encontrado'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    # Buscando tipo do posto, caso seja um posto do tipo Relógio de ponto ignora a impressão
                                                    posto_tipo = df.select_postofrequencia_resposta(posto_id, empresa)
                                                    posto_tipo_inss = posto_tipo[posto_tipo['nome'].str.contains('INSS')== False]

                                                    if len(func_imp) == 0 and posto_tipo['frequencia_id'].iloc[0] != 1 and len(posto_tipo_inss) > 0:
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Sem impressão neste período'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    resposta_id = periodo
                                                    resposta_texto = periodo

                                                    # Buscando o Nome da Coligada e Matricula do Arquivo
                                                    nome_coligada = df.select_formscoligadaregistroresposta(empresa)

                                                    # Buscando o ID repostas do Campo Funcionario para Inserção

                                                    func_id = pd.DataFrame(
                                                        df.select_funcionario_respostaid(periodo, matricula, empresa,posto_id))

                                                    if func_id.empty:
                                                        msg = ' -> Alocação não encontrada, Funcionário não localizado.'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    auxVetor = func_id.loc[0, 'id'].__str__().split('.')
                                                    # Buscando Posto do Arquivo e Montando o valor Para Inserção no campo de Resposta
                                                    posto_resposta = df.select_lotacao_resposta(posto_id, empresa)
                                                    # Buscando Funcao do ID retornado da consulta de Funcionario para inserção na resposta
                                                    funcao_id = auxVetor[4]

                                                    if len(funcao_id) == 0:
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Função não encontrada'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    funcao_resposta = df.select_funcao_resposta(funcao_id)
                                                    # Buscando Supervisor do ID retornado da consulta de Funcionario para inserção na resposta
                                                    supervisor_id = auxVetor[2]

                                                    if len(supervisor_id) == 0:
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Supervisor não encontrado'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    supervisor_resposta = df.select_supervisor_resposta(supervisor_id)

                                                    df.insert_registro_formsregistrosrespostas45(24084)
                                                    # Buscando o último registro do ppessoa_id(24084)
                                                    tabela_reg_inserido = pd.DataFrame(df.select_id_formsregistrosrespostas45(24084))
                                                    valor_id = tabela_reg_inserido.loc[0, 'id']

                                                except:
                                                    msg = '-> Ocorreu um erro não mapeado ao tentar realizar a inserção, verique com o suporte!'
                                                    extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                    if flag.visualizarFP == 1:
                                                        print(flag.linha_separador("_",
                                                                                   "       " + tabela_arquivo.__str__() + " ",
                                                                                   1))
                                                        print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)

                                            if(valor_id !=0):
                                               # -----------------------------------------------------------------------------------
                                               # limpar as respostas anteriores deste deste id
                                                df.delete_registro_respostas45(valor_id)

                                               # Realizar cadastro das Respostas'''
                                               # Inserindo a resposta 404 (Empresa)
                                                df.insert_registro_resp_404_formsregistrosrespostas45(valor_id,
                                                                                                      empresa,
                                                                                                      nome_coligada.loc[
                                                                                                          0, 'nome'])
                                                # Inserindo a resposta 405 (Supervisor)
                                                df.insert_registro_resp_405_formsregistrosrespostas45(valor_id,
                                                                                                      supervisor_id,
                                                                                                      supervisor_resposta.loc[
                                                                                                          0, 'nome'])
                                                # Inserindo a resposta 406 (Funcionario)
                                                df.insert_registro_resp_406_formsregistrosrespostas45(valor_id,
                                                                                                      func_id.loc[
                                                                                                          0, 'id'],
                                                                                                      func_id.loc[
                                                                                                          0, 'nome'])
                                                # Inserindo a resposta 407 (Lotacao)
                                                df.insert_registro_resp_407_formsregistrosrespostas45(valor_id,
                                                                                                      posto_id,
                                                                                                      posto_resposta.loc[
                                                                                                          0, 'nome'])
                                                # Inserindo a resposta 408 (Periodo)
                                                df.insert_registro_resp_408_formsregistrosrespostas45(valor_id,
                                                                                                      resposta_id,
                                                                                                      resposta_texto)
                                                # Inserindo a resposta 409 (Função)
                                                df.insert_registro_resp_409_formsregistrosrespostas45(valor_id,
                                                                                                      funcao_resposta.loc[
                                                                                                          0, 'id'],
                                                                                                      funcao_resposta.loc[
                                                                                                          0, 'nome'])
                                                # -----------------------------------------------------------------------------------
                                                # Resposta 411 Horário Britânico
                                                df.insert_registro_resp_411_formsregistrosrespostas45(valor_id)
                                                # -----------------------------------------------------------------------------------
                                                # Resposta 412 Horário Incompleto
                                                df.insert_registro_resp_412_formsregistrosrespostas45(valor_id)
                                                # -----------------------------------------------------------------------------------
                                                # Resposta 413 Ass.: Funcionário
                                                df.insert_registro_resp_413_formsregistrosrespostas45(valor_id)
                                                # -----------------------------------------------------------------------------------
                                                # Resposta 414 Ass.: Supervisor
                                                df.insert_registro_resp_414_formsregistrosrespostas45(valor_id)
                                                # -----------------------------------------------------------------------------------
                                                # Resposta 415 Rasura
                                                df.insert_registro_resp_415_formsregistrosrespostas45(valor_id)
                                                # -----------------------------------------------------------------------------------
                                                # Resposta 416 Hora/Escala
                                                df.insert_registro_resp_416_formsregistrosrespostas45(valor_id)
                                                # -----------------------------------------------------------------------------------
                                                # Resposta 418 Intrajornada
                                                df.insert_registro_resp_418_formsregistrosrespostas45(valor_id)
                                                # -----------------------------------------------------------------------------------
                                                # Resposta 421 Retorno da Pasta!
                                                df.insert_registro_resp_421_formsregistrosrespostas45(valor_id)
                                                # -----------------------------------------------------------------------------------
                                                # Resposta 456 Origem da Folha
                                                df.insert_registro_resp_456_formsregistrosrespostas45(valor_id)
                                                # -----------------------------------------------------------------------------------
                                                # Resposta 3054 Hora Noturna
                                                df.insert_registro_resp_3054_formsregistrosrespostas45(valor_id)
                                                # -----------------------------------------------------------------------------------
                                                # Resposta 3089 Observação
                                                df.insert_registro_resp_3089_formsregistrosrespostas45(valor_id)
                                                # -----------------------------------------------------------------------------------

                                                totalCadastroRealizado += 1

                                                # Buscando o registro cadastrado pela função modeladora
                                                tabela_reg_tratado = pd.DataFrame(
                                                    df.select_id_formsregistrosrespostas45_id(valor_id))

                                                # -----------------------------------------------------------------------------------

                                                msg = '     -> Cadastro Realizado!'
                                                # extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                if flag.visualizarFP == 1:
                                                    print(flag.linha_separador("_", "       " + tabela_arquivo.__str__() + " ", 1))
                                                    print(Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + msg)
                                                # break
                                        else:
                                            totalForadoPadraoQRCODE += 1
                                            msg = '     -> Fora do Padrão QRCODE - Pasta X Mês'
                                            extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                            if flag.visualizarFP == 1:
                                                print(flag.linha_separador("_", "       " + tabela_arquivo.__str__() + " ", 1))
                                                print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                    else:
                                        totalForadoPadraoQRCODE += 1
                                        msg = '     -> Fora do Padrão QRCODE'
                                        extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                        if flag.visualizarFP == 1:
                                            print(flag.linha_separador("_", "       " + tabela_arquivo.__str__() + " ", 1))
                                            print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                        else:
                            continue

                    msg = flag.linha_separador(">", '', 2)
                    extrato.write(str(msg) + '\n')
                    print(flag.linha_separador(">", '', 2))
                    msg = "     -> (1º) FOLHAS DE PONTO (Empresa: " + coligada[empresa].__str__() + ") "
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     ->    Total Cadastro Realizado: ' + totalCadastroRealizado.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTGREEN_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     ->    Total Retorno Atualizado: ' + totalRetornoAtualizado.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTBLUE_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     ->         Total Já Cadastrado: ' + totalJaCadastrado.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTCYAN_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     -> Total Fora do Padrão QRCODE: ' + totalForadoPadraoQRCODE.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     ->             Total Duplicado: ' + totalDuplicado.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = flag.linha_separador("_", '', 2)
                    extrato.write(str(msg) + '\n')
            except df_bd.ErSror as err:
                notifica.clear()
                msg = ' -> Opa problema! :(' + err + ')'
                extrato.write(str(msg) + '\n')
                print(f"Erro: '{err}'")

        flag.mail('Notificação de Erros SONAR', 'Protocolo', 'protocolo@gruposerval.com.br',
                  'Segue em anexo os erros encontrados no Processo ' + datetime.today().strftime(
                      '%d/%m/%Y %H:%M:%S').__str__(),
                  'Folha de Ponto', mapeado + 'Folhas_de_Ponto.txt')


    def listarbanco2023(self,mapeado):
        flag = Funcoes.Flag()
        with open(mapeado + 'Folhas_de_Ponto_2023.txt', 'w') as extrato:
            try:
                extrato.write(
                    str('-> FOLHAS DE PONTO: Dados para análise e correção:' + '\nIniciado as ' + datetime.today().strftime(
                        '%d/%m/%Y %H:%M:%S').__str__()) + '\n')
                notifica = list()
                coligada = flag.coligadas_operacional()
                linha_cadastrada = ""
                # --------------------------------------------------
                df = DataFrame.DB()
                # Removendo os formulário incompletos....

                df_sobra = pd.DataFrame(df.select_formsregistrosrespostas45_sobra())
                codigos = "0"
                if len(df_sobra) > 0:
                    for i in df_sobra['registro_id']:
                        if len(codigos) == 1:
                            codigos = i.__str__() + ','
                        else:
                            codigos = codigos + i.__str__() + ','

                    df.delete_formsregistrosrespostas45_sobra(codigos[:-1])
                    df.delete_formsregistros45_sobra

                df_bd = pd.DataFrame(df.select_formsregistrosrespostas45())

                msg = '     ->    Iniciando 2023...'
                extrato.write(str(msg) + '\n')

                print(Back.LIGHTWHITE_EX + Fore.LIGHTGREEN_EX + Style.BRIGHT + flag.linha_separador(" ",
                                                                                                    '     ->    Iniciando 2023...',
                                                                                                    3))

                # ++++++++++++++++++++++++++++++++++++++++++++++++++
                date_str = '01-01-2023'
                date_object = datetime.strptime(date_str, '%m-%d-%Y').date()
                # --------------------------------------------------

                pr = pd.period_range(start='2023-01-01', end='2023-12-31', freq='M')  # date.today()

                tempo = list(datetime.strptime(
                    period.day.__str__().zfill(2) + '-' + period.month.__str__().zfill(
                        2) + '-' + period.year.__str__().zfill(4),
                    '%d-%m-%Y').date() for period in pr)

                # T1 = date.today()
                # T0 = (T1 + timedelta(days=30))
                # T2 = (T1 - timedelta(days=30))
                # T3 = (T2 - timedelta(days=30))
                # ++++++++++++++++++++++++++++++++++++++++++++++++++
                # tempo = (T1, T0)# T4, T3, T2
                # Formato Antigo.... Foca no Mês/Ano final do período
                for empresa in coligada:
                    totalJaCadastrado = 0
                    totalRetornoAtualizado = 0
                    totalCadastroRealizado = 0
                    totalForadoPadraoQRCODE = 0
                    totalDuplicado = 0
                    map = mapeado + coligada[empresa]
                    # print(flag.linha_separador(" ", " " + coligada[empresa].__str__() + " ", 2))
                    # print(tempo)
                    i = -1
                    for mes_atual in tempo:
                        i += 1
                        msg = "->(2º) FOLHAS DE PONTO: " + coligada[empresa].__str__() + " -> " + flag.nome_mes(
                            mes_atual.month).upper().__str__() + "/" + mes_atual.year.__str__()
                        extrato.write(str(msg) + '\n')

                        print(flag.linha_separador(">", '', 2))
                        print(flag.linha_separador(" ", msg, 1))
                        # print(flag.linha_separador("_", '', 2))
                        cam = map + '\\ANO ' + mes_atual.year.__str__() + '\\' + 'MES ' + flag.mes(mes_atual.month)
                        caminhos = cam
                        # Preparando o Dataframe para o Período atual

                        df_bd_atual = df_bd[df_bd['data2'].str.contains(pr[i].__str__(), na=False)]

                        if os.path.exists(caminhos):
                            for diretorio, subpastas, arquivos in os.walk(caminhos):
                                for arquivo in tqdm(sorted(arquivos)):
                                    time.sleep(0.02)
                                    cadastro_ok = 0
                                    # print(diretorio + ' # ' + arquivo)
                                    tabela_arquivo = arquivo.replace("-", "_").replace(".pdf", "_").replace("a",
                                                                                                            "_").split(
                                        '_')

                                    if(tabela_arquivo[0] == 'Thumbs.db'):
                                        continue

                                    # print(flag.linha_separador("_","       " + tabela_arquivo.__str__() + " ",1))
                                    if len(tabela_arquivo) >= 9:
                                        if tabela_arquivo[8].__eq__(flag.numero_mes(flag.nome_mes(mes_atual.month))):
                                            try:
                                                codcliente_arq = tabela_arquivo[0].__str__()
                                                codlot_arq = tabela_arquivo[1].__str__()
                                                matricula_arq = tabela_arquivo[2].__str__()
                                                colaborador_arq = tabela_arquivo[3].__str__()
                                                ano_ini_arq = tabela_arquivo[4].__str__()
                                                mes_ini_arq = tabela_arquivo[5].__str__()
                                                dia_ini_arq = tabela_arquivo[6].__str__()
                                                ano_fim_arq = tabela_arquivo[7].__str__()
                                                mes_fim_arq = tabela_arquivo[8].__str__()
                                                dia_fim_arq = tabela_arquivo[9].__str__()

                                            except IndexError:
                                                totalForadoPadraoQRCODE += 1
                                                msg = '     -> Fora do Padrão QRCODE'
                                                extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                if flag.visualizarFP == 1:
                                                    print(flag.linha_separador("_",
                                                                               "       " + tabela_arquivo.__str__() + " ",
                                                                               1))
                                                    print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                    continue

                                            linha_arquivo = empresa.__str__() + '-' + \
                                                            matricula_arq + '-' + \
                                                            ano_fim_arq + '/' + \
                                                            mes_fim_arq + 'de' + \
                                                            dia_ini_arq + 'a' + \
                                                            dia_fim_arq
                                            mes = mes_atual.month.__str__()
                                            ano = mes_atual.year.__str__()
                                            if len(dia_ini_arq) != 2 \
                                                    or len(dia_fim_arq) != 2 \
                                                    or len(ano_ini_arq) != 4 \
                                                    or len(ano_fim_arq) != 4 \
                                                    or ano_fim_arq != ano \
                                                    or int(mes_fim_arq).__str__() != mes:
                                                totalForadoPadraoQRCODE += 1
                                                msg = '     -> Fora do Padrão QRCODE OU PERÍODO INCOERENTE COM A PASTA'
                                                extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                if flag.visualizarFP == 1:
                                                    print(flag.linha_separador("_",
                                                                               "       " + tabela_arquivo.__str__() + " ",
                                                                               1))
                                                    print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                continue

                                            # Para confronto
                                            # Verifica se já existe no BD
                                            if (linha_arquivo in df_bd_atual.indiceII.values):
                                                tabela_bd = df_bd_atual[df_bd_atual['indiceII'].str.contains(linha_arquivo.__str__(),na = False)]
                                                tabela_bd = tabela_bd['indice'].iloc[0].__str__().replace("-", "_").replace("a","_").split('#')

                                                cod = tabela_bd[0]
                                                tabela_bd_II = tabela_bd[1].split('_')

                                                totalJaCadastrado += 1
                                                cadastro_ok = 1
                                                df_bd_duplicado = None
                                                # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + '     -> Arquivo já cadastrado!')
                                                if tabela_bd_II[11].__str__() != "OK":
                                                    # Resposta 421 Retorno da Pasta!
                                                    totalRetornoAtualizado += 1
                                                    # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + '     -> Retorno Atualizado!')
                                                    df.update_formsregistrosrespostas45(cod)

                                                continue

                                            # Inserindo registro
                                            if cadastro_ok == 0:
                                                cadastro_ok = 1
                                                valor_id = 0
                                                linha_cadastrada = linha_arquivo

                                                # -----------------------------------------------------------------------------------
                                                # Realizar cadastro do Registro
                                                try:
                                                    # Vamos validar se o período está coerente com o que foi impresso
                                                    matricula = matricula_arq
                                                    periodo = dia_ini_arq + '/' + \
                                                              mes_ini_arq + '/' + \
                                                              ano_ini_arq + ' - ' + \
                                                              dia_fim_arq + '/' + \
                                                              mes_fim_arq + '/' + \
                                                              ano_fim_arq
                                                    func_imp = pd.DataFrame(
                                                        df.select_impressao(periodo, matricula, empresa))
                                                    if any(c in special_characters for c in colaborador_arq):
                                                        tabela_arquivo[3] = re.sub(r"[^a-zA-Z0-9 ]", "", colaborador_arq)
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Carácter especial no nome do colaborador, impossibilitando inserção'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue
                                                    if len(periodo) != 23:
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Problema nas datas do período'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    posto_id = tabela_arquivo[1]

                                                    if len(posto_id) == 0:
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Posto não encontrado'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    posto_tipo = df.select_postofrequencia_resposta(posto_id, empresa)
                                                    posto_tipo_inss = posto_tipo[posto_tipo['nome'].str.contains('INSS')== False]

                                                    if len(func_imp) == 0 and posto_tipo['frequencia_id'].iloc[0] != 1 and len(posto_tipo_inss) > 0:
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Sem impressão neste período'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    resposta_id = periodo
                                                    resposta_texto = periodo

                                                    # Buscando o Nome da Coligada e Matricula do Arquivo
                                                    nome_coligada = df.select_formscoligadaregistroresposta(empresa)

                                                    # Buscando o ID repostas do Campo Funcionario para Inserção
                                                    func_id = pd.DataFrame(
                                                        df.select_funcionario_respostaid(periodo, matricula, empresa,posto_id))

                                                    if func_id.empty:
                                                        msg = ' -> Alocação não encontrada, Funcionário não localizado.'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    auxVetor = func_id.loc[0, 'id'].__str__().split('.')
                                                    # Buscando Posto do Arquivo e Montando o valor Para Inserção no campo de Resposta
                                                    posto_resposta = df.select_lotacao_resposta(posto_id, empresa)

                                                    # Buscando Funcao do ID retornado da consulta de Funcionario para inserção na resposta
                                                    funcao_id = auxVetor[4]

                                                    if len(funcao_id) == 0:
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Função não encontrada'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    funcao_resposta = df.select_funcao_resposta(funcao_id)
                                                    # Buscando Supervisor do ID retornado da consulta de Funcionario para inserção na resposta
                                                    supervisor_id = auxVetor[2]

                                                    if len(supervisor_id) == 0:
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Supervisor não encontrado'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    supervisor_resposta = df.select_supervisor_resposta(supervisor_id)

                                                    df.insert_registro_formsregistrosrespostas45(26187)
                                                    # Buscando o último registro do ppessoa_id(24084)
                                                    tabela_reg_inserido = pd.DataFrame(
                                                        df.select_id_formsregistrosrespostas45(26187))
                                                    valor_id = tabela_reg_inserido.loc[0, 'id']

                                                except:
                                                    msg = ' -> Ocorreu um erro não mapeado ao tentar realizar a inserção, verique com o suporte!'
                                                    extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                    if flag.visualizarFP == 1:
                                                        print(flag.linha_separador("_",
                                                                                   "       " + tabela_arquivo.__str__() + " ",
                                                                                   1))
                                                        print(
                                                            Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)

                                                # -----------------------------------------------------------------------------------
                                                # limpar as respostas anteriores deste deste id

                                                if valor_id != 0:
                                                  df.delete_registro_respostas45(valor_id)

                                                  # Realizar cadastro das Respostas'''
                                                  # Inserindo a resposta 404 (Empresa)
                                                  df.insert_registro_resp_404_formsregistrosrespostas45(valor_id,
                                                                                                        empresa,
                                                                                                        nome_coligada.loc[
                                                                                                            0, 'nome'])
                                                  # Inserindo a resposta 405 (Supervisor)
                                                  df.insert_registro_resp_405_formsregistrosrespostas45(valor_id,
                                                                                                        supervisor_id,
                                                                                                        supervisor_resposta.loc[
                                                                                                            0, 'nome'])
                                                  # Inserindo a resposta 406 (Funcionario)
                                                  df.insert_registro_resp_406_formsregistrosrespostas45(valor_id,
                                                                                                        func_id.loc[
                                                                                                            0, 'id'],
                                                                                                        func_id.loc[
                                                                                                            0, 'nome'])
                                                  # Inserindo a resposta 407 (Lotacao)
                                                  df.insert_registro_resp_407_formsregistrosrespostas45(valor_id,
                                                                                                        posto_id,
                                                                                                        posto_resposta.loc[
                                                                                                            0, 'nome'])
                                                  # Inserindo a resposta 408 (Periodo)
                                                  df.insert_registro_resp_408_formsregistrosrespostas45(valor_id,
                                                                                                        resposta_id,
                                                                                                        resposta_texto)
                                                  # Inserindo a resposta 409 (Função)
                                                  df.insert_registro_resp_409_formsregistrosrespostas45(valor_id,
                                                                                                        funcao_resposta.loc[
                                                                                                            0, 'id'],
                                                                                                        funcao_resposta.loc[
                                                                                                            0, 'nome'])
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 411 Horário Britânico
                                                  df.insert_registro_resp_411_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 412 Horário Incompleto
                                                  df.insert_registro_resp_412_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 413 Ass.: Funcionário
                                                  df.insert_registro_resp_413_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 414 Ass.: Supervisor
                                                  df.insert_registro_resp_414_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 415 Rasura
                                                  df.insert_registro_resp_415_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 416 Hora/Escala
                                                  df.insert_registro_resp_416_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 418 Intrajornada
                                                  df.insert_registro_resp_418_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 421 Retorno da Pasta!
                                                  df.insert_registro_resp_421_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 456 Origem da Folha
                                                  df.insert_registro_resp_456_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------a ------------------------------------------
                                                  # Resposta 3054 Hora Noturna
                                                  df.insert_registro_resp_3054_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 3089 Observação
                                                  df.insert_registro_resp_3089_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------

                                                  # Buscando o registro cadastrado pela função modeladora
                                                  tabela_reg_tratado = pd.DataFrame(
                                                      df.select_id_formsregistrosrespostas45_id(valor_id))

                                                  # -----------------------------------------------------------------------------------

                                                  totalCadastroRealizado += 1
                                                  msg = '     -> Cadastro Realizado!'
                                                  # extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')

                                                  if flag.visualizarFP == 1:
                                                      print(flag.linha_separador("_",
                                                                                 "       " + tabela_arquivo.__str__() + " ",
                                                                                 1))
                                                      print(Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + msg)
                                                  # break

                                        else:
                                            totalForadoPadraoQRCODE += 1
                                            msg = '     -> Fora do Padrão QRCODE Pasta X Mês'
                                            extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                            if flag.visualizarFP == 1:
                                                print(flag.linha_separador("_",
                                                                           "       " + tabela_arquivo.__str__() + " ",
                                                                           1))
                                                print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                    else:
                                        totalForadoPadraoQRCODE += 1
                                        msg = '     -> Fora do Padrão QRCODE'
                                        extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                        if flag.visualizarFP == 1:
                                            print(flag.linha_separador("_", "       " + tabela_arquivo.__str__() + " ",
                                                                       1))
                                            print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                        else:
                            continue

                    msg = flag.linha_separador(">", '', 2)
                    extrato.write(str(msg) + '\n')
                    print(flag.linha_separador(">", '', 2))
                    msg = "     ->(2º) FOLHAS DE PONTO (Empresa: " + coligada[empresa].__str__() + ") "
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     ->    Total Cadastro Realizado: ' + totalCadastroRealizado.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTGREEN_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     ->    Total Retorno Atualizado: ' + totalRetornoAtualizado.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTBLUE_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     ->         Total Já Cadastrado: ' + totalJaCadastrado.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTCYAN_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     -> Total Fora do Padrão QRCODE: ' + totalForadoPadraoQRCODE.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     ->             Total Duplicado: ' + totalDuplicado.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = flag.linha_separador("_", '', 2)
                    extrato.write(str(msg) + '\n')
                #notifica.clear()
                extrato.write(str('\n-> Operação Concluída! As ' + datetime.today().strftime(
                    '%d/%m/%Y %H:%M:%S').__str__()) + '\n')
            except df_bd.ErSror as err:
                notifica.clear()
                msg = ' -> Opa problema! :(' + err + ')'
                extrato.write(str(msg) + '\n')
                print(f"Erro: '{err}'")

            print()

        flag.mail('Notificação de Erros SONAR', 'Protocolo', 'protocolo@gruposerval.com.br',
                  'Segue em anexo os erros encontrados no Processo ' + datetime.today().strftime(
                      '%d/%m/%Y %H:%M:%S').__str__(),
                  'Folha de Ponto', mapeado + 'Folhas_de_Ponto_2023.txt')
        print(flag.linha_separador(">", '', 2))
    def listarbanco2024(self,mapeado):
        flag = Funcoes.Flag()
        with open(mapeado + 'Folhas_de_Ponto_2024.txt', 'w') as extrato:
            try:
                extrato.write(
                    str('-> FOLHAS DE PONTO: Dados para análise e correção:' + '\nIniciado as ' + datetime.today().strftime(
                        '%d/%m/%Y %H:%M:%S').__str__()) + '\n')
                notifica = list()
                coligada = flag.coligadas_operacional()
                linha_cadastrada = ""
                # --------------------------------------------------
                df = DataFrame.DB()
                # Removendo os formulário incompletos....

                df_sobra = pd.DataFrame(df.select_formsregistrosrespostas45_sobra())
                codigos = "0"
                if len(df_sobra) > 0:
                    for i in df_sobra['registro_id']:
                        if len(codigos) == 1:
                            codigos = i.__str__() + ','
                        else:
                            codigos = codigos + i.__str__() + ','

                    df.delete_formsregistrosrespostas45_sobra(codigos[:-1])
                    df.delete_formsregistros45_sobra

                df_bd = pd.DataFrame(df.select_formsregistrosrespostas45())

                msg = '     ->    Iniciando 2024...'
                extrato.write(str(msg) + '\n')

                print(Back.LIGHTWHITE_EX + Fore.LIGHTGREEN_EX + Style.BRIGHT + flag.linha_separador(" ",
                                                                                                    '     ->    Iniciando 2024...',
                                                                                                    3))

                # ++++++++++++++++++++++++++++++++++++++++++++++++++
                # --------------------------------------------------

                pr = pd.period_range(start=flag.folhaData, end=date.today(), freq='M')  # date.today()

                tempo = list(datetime.strptime(
                    period.day.__str__().zfill(2) + '-' + period.month.__str__().zfill(
                        2) + '-' + period.year.__str__().zfill(4),
                    '%d-%m-%Y').date() for period in pr)

                # T1 = date.today()
                # T0 = (T1 + timedelta(days=30))
                # T2 = (T1 - timedelta(days=30))
                # T3 = (T2 - timedelta(days=30))
                # ++++++++++++++++++++++++++++++++++++++++++++++++++
                # tempo = (T1, T0)# T4, T3, T2
                # Formato Antigo.... Foca no Mês/Ano final do período
                for empresa in coligada:
                    totalJaCadastrado = 0
                    totalRetornoAtualizado = 0
                    totalCadastroRealizado = 0
                    totalForadoPadraoQRCODE = 0
                    totalDuplicado = 0
                    map = mapeado + coligada[empresa]
                    # print(flag.linha_separador(" ", " " + coligada[empresa].__str__() + " ", 2))
                    # print(tempo)
                    i = -1
                    for mes_atual in tempo:
                        i += 1
                        msg = "->(3º) FOLHAS DE PONTO: " + coligada[empresa].__str__() + " -> " + flag.nome_mes(
                            mes_atual.month).upper().__str__() + "/" + mes_atual.year.__str__()
                        extrato.write(str(msg) + '\n')

                        print(flag.linha_separador(">", '', 2))
                        print(flag.linha_separador(" ", msg, 1))
                        # print(flag.linha_separador("_", '', 2))
                        cam = map + '\\ANO ' + mes_atual.year.__str__() + '\\' + 'MES ' + flag.mes(mes_atual.month)
                        caminhos = cam
                        # Preparando o Dataframe para o Período atual

                        df_bd_atual = df_bd[df_bd['data2'].str.contains(pr[i].__str__(), na=False)]

                        if os.path.exists(caminhos):
                            for diretorio, subpastas, arquivos in os.walk(caminhos):
                                for arquivo in tqdm(sorted(arquivos)):
                                    time.sleep(0.02)
                                    cadastro_ok = 0
                                    # print(diretorio + ' # ' + arquivo)
                                    tabela_arquivo = arquivo.replace("-", "_").replace(".pdf", "_").replace("a",
                                                                                                            "_").split(
                                        '_')

                                    if(tabela_arquivo[0] == 'Thumbs.db'):
                                        continue

                                    # print(flag.linha_separador("_","       " + tabela_arquivo.__str__() + " ",1))
                                    if len(tabela_arquivo) >= 9:
                                        if tabela_arquivo[8].__eq__(flag.numero_mes(flag.nome_mes(mes_atual.month))):
                                            try:
                                                codcliente_arq = tabela_arquivo[0].__str__()
                                                codlot_arq = tabela_arquivo[1].__str__()
                                                matricula_arq = tabela_arquivo[2].__str__()
                                                colaborador_arq = tabela_arquivo[3].__str__()
                                                ano_ini_arq = tabela_arquivo[4].__str__()
                                                mes_ini_arq = tabela_arquivo[5].__str__()
                                                dia_ini_arq = tabela_arquivo[6].__str__()
                                                ano_fim_arq = tabela_arquivo[7].__str__()
                                                mes_fim_arq = tabela_arquivo[8].__str__()
                                                dia_fim_arq = tabela_arquivo[9].__str__()
                                            except IndexError:
                                                totalForadoPadraoQRCODE += 1
                                                msg = '     -> Fora do Padrão QRCODE OU PERÍODO INCOERENTE COM A PASTA'
                                                extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                if flag.visualizarFP == 1:
                                                    print(flag.linha_separador("_",
                                                                               "       " + tabela_arquivo.__str__() + " ",
                                                                               1))
                                                    print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                    continue

                                            linha_arquivo = empresa.__str__() + '-' + \
                                                            matricula_arq + '-' + \
                                                            ano_fim_arq + '/' + \
                                                            mes_fim_arq + 'de' + \
                                                            dia_ini_arq + 'a' + \
                                                            dia_fim_arq
                                            mes = mes_atual.month.__str__()
                                            ano = mes_atual.year.__str__()
                                            if len(dia_ini_arq) != 2 \
                                                    or len(dia_fim_arq) != 2 \
                                                    or len(ano_ini_arq) != 4 \
                                                    or len(ano_fim_arq) != 4 \
                                                    or ano_fim_arq != ano \
                                                    or int(mes_fim_arq).__str__() != mes:
                                                totalForadoPadraoQRCODE += 1
                                                msg = '     -> Fora do Padrão QRCODE'
                                                extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                if flag.visualizarFP == 1:
                                                    print(flag.linha_separador("_",
                                                                               "       " + tabela_arquivo.__str__() + " ",
                                                                               1))
                                                    print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                continue

                                            # Para confronto
                                            # Verifica se já existe no BD
                                            if (linha_arquivo in df_bd_atual.indiceII.values):
                                                tabela_bd = df_bd_atual[df_bd_atual['indiceII'].str.contains(linha_arquivo.__str__(),na = False)]
                                                tabela_bd = tabela_bd['indice'].iloc[0].__str__().replace("-", "_").replace("a","_").split('#')

                                                cod = tabela_bd[0]
                                                tabela_bd_II = tabela_bd[1].split('_')

                                                totalJaCadastrado += 1
                                                cadastro_ok = 1
                                                df_bd_duplicado = None
                                                # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + '     -> Arquivo já cadastrado!')
                                                if tabela_bd_II[11].__str__() != "OK":
                                                    # Resposta 421 Retorno da Pasta!
                                                    totalRetornoAtualizado += 1
                                                    # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + '     -> Retorno Atualizado!')
                                                    df.update_formsregistrosrespostas45(cod)

                                                continue

                                            # Inserindo registro
                                            if cadastro_ok == 0:
                                                cadastro_ok = 1
                                                valor_id = 0
                                                linha_cadastrada = linha_arquivo

                                                # -----------------------------------------------------------------------------------
                                                # Realizar cadastro do Registro
                                                try:
                                                    # Vamos validar se o período está coerente com o que foi impresso
                                                    matricula = matricula_arq
                                                    periodo = dia_ini_arq + '/' + \
                                                              mes_ini_arq + '/' + \
                                                              ano_ini_arq + ' - ' + \
                                                              dia_fim_arq + '/' + \
                                                              mes_fim_arq + '/' + \
                                                              ano_fim_arq
                                                    func_imp = pd.DataFrame(
                                                        df.select_impressao(periodo, matricula, empresa))
                                                    if any(c in special_characters for c in colaborador_arq):
                                                        tabela_arquivo[3] = re.sub(r"[^a-zA-Z0-9 ]", "", colaborador_arq)
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Carácter especial no nome do colaborador, impossibilitando inserção'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue
                                                    if len(periodo) != 23:
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Problema nas datas do período'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    posto_id = tabela_arquivo[1]

                                                    if len(posto_id) == 0:
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Posto não encontrado'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    posto_tipo = df.select_postofrequencia_resposta(posto_id, empresa)
                                                    posto_tipo_inss = posto_tipo[posto_tipo['nome'].str.contains('INSS')== False]

                                                    if len(func_imp) == 0 and posto_tipo['frequencia_id'].iloc[0] != 1 and len(posto_tipo_inss) > 0:
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Sem impressão neste período'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    resposta_id = periodo
                                                    resposta_texto = periodo

                                                    # Buscando o Nome da Coligada e Matricula do Arquivo
                                                    nome_coligada = df.select_formscoligadaregistroresposta(empresa)

                                                    # Buscando o ID repostas do Campo Funcionario para Inserção
                                                    func_id = pd.DataFrame(
                                                        df.select_funcionario_respostaid(periodo, matricula, empresa,posto_id))

                                                    if func_id.empty:
                                                        msg = ' -> Alocação não encontrada, Funcionário não localizado.'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    auxVetor = func_id.loc[0, 'id'].__str__().split('.')
                                                    # Buscando Posto do Arquivo e Montando o valor Para Inserção no campo de Resposta
                                                    posto_resposta = df.select_lotacao_resposta(posto_id, empresa)

                                                    # Buscando Funcao do ID retornado da consulta de Funcionario para inserção na resposta
                                                    funcao_id = auxVetor[4]

                                                    if len(funcao_id) == 0:
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Função não encontrada'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    funcao_resposta = df.select_funcao_resposta(funcao_id)
                                                    # Buscando Supervisor do ID retornado da consulta de Funcionario para inserção na resposta
                                                    supervisor_id = auxVetor[2]

                                                    if len(supervisor_id) == 0:
                                                        totalForadoPadraoQRCODE += 1
                                                        msg = '     -> Fora do Padrão QRCODE - Supervisor não encontrado'
                                                        extrato.write(
                                                            str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                        if flag.visualizarFP == 1:
                                                            print(flag.linha_separador("_",
                                                                                       "       " + tabela_arquivo.__str__() + " ",
                                                                                       1))
                                                            print(
                                                                Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                        continue

                                                    supervisor_resposta = df.select_supervisor_resposta(supervisor_id)

                                                    df.insert_registro_formsregistrosrespostas45(26188)
                                                    # Buscando o último registro do ppessoa_id(24084)
                                                    tabela_reg_inserido = pd.DataFrame(
                                                        df.select_id_formsregistrosrespostas45(26188))
                                                    valor_id = tabela_reg_inserido.loc[0, 'id']

                                                except:
                                                    msg = ' -> Ocorreu um erro não mapeado ao tentar realizar a inserção, verique com o suporte!'
                                                    extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                    if flag.visualizarFP == 1:
                                                        print(flag.linha_separador("_",
                                                                                   "       " + tabela_arquivo.__str__() + " ",
                                                                                   1))
                                                        print(
                                                            Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)

                                                # -----------------------------------------------------------------------------------
                                                # limpar as respostas anteriores deste deste id

                                                if valor_id != 0:
                                                  df.delete_registro_respostas45(valor_id)

                                                  # Realizar cadastro das Respostas'''
                                                  # Inserindo a resposta 404 (Empresa)
                                                  df.insert_registro_resp_404_formsregistrosrespostas45(valor_id,
                                                                                                        empresa,
                                                                                                        nome_coligada.loc[
                                                                                                            0, 'nome'])
                                                  # Inserindo a resposta 405 (Supervisor)
                                                  df.insert_registro_resp_405_formsregistrosrespostas45(valor_id,
                                                                                                        supervisor_id,
                                                                                                        supervisor_resposta.loc[
                                                                                                            0, 'nome'])
                                                  # Inserindo a resposta 406 (Funcionario)
                                                  df.insert_registro_resp_406_formsregistrosrespostas45(valor_id,
                                                                                                        func_id.loc[
                                                                                                            0, 'id'],
                                                                                                        func_id.loc[
                                                                                                            0, 'nome'])
                                                  # Inserindo a resposta 407 (Lotacao)
                                                  df.insert_registro_resp_407_formsregistrosrespostas45(valor_id,
                                                                                                        posto_id,
                                                                                                        posto_resposta.loc[
                                                                                                            0, 'nome'])
                                                  # Inserindo a resposta 408 (Periodo)
                                                  df.insert_registro_resp_408_formsregistrosrespostas45(valor_id,
                                                                                                        resposta_id,
                                                                                                        resposta_texto)
                                                  # Inserindo a resposta 409 (Função)
                                                  df.insert_registro_resp_409_formsregistrosrespostas45(valor_id,
                                                                                                        funcao_resposta.loc[
                                                                                                            0, 'id'],
                                                                                                        funcao_resposta.loc[
                                                                                                            0, 'nome'])
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 411 Horário Britânico
                                                  df.insert_registro_resp_411_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 412 Horário Incompleto
                                                  df.insert_registro_resp_412_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 413 Ass.: Funcionário
                                                  df.insert_registro_resp_413_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 414 Ass.: Supervisor
                                                  df.insert_registro_resp_414_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 415 Rasura
                                                  df.insert_registro_resp_415_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 416 Hora/Escala
                                                  df.insert_registro_resp_416_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 418 Intrajornada
                                                  df.insert_registro_resp_418_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 421 Retorno da Pasta!
                                                  df.insert_registro_resp_421_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 456 Origem da Folha
                                                  df.insert_registro_resp_456_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 3054 Hora Noturna
                                                  df.insert_registro_resp_3054_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------
                                                  # Resposta 3089 Observação
                                                  df.insert_registro_resp_3089_formsregistrosrespostas45(valor_id)
                                                  # -----------------------------------------------------------------------------------

                                                  # Buscando o registro cadastrado pela função modeladora
                                                  tabela_reg_tratado = pd.DataFrame(
                                                      df.select_id_formsregistrosrespostas45_id(valor_id))

                                                  # -----------------------------------------------------------------------------------

                                                  totalCadastroRealizado += 1
                                                  msg = '     -> Cadastro Realizado!'
                                                  # extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')

                                                  if flag.visualizarFP == 1:
                                                      print(flag.linha_separador("_",
                                                                                 "       " + tabela_arquivo.__str__() + " ",
                                                                                 1))
                                                      print(Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + msg)
                                                  # break

                                        else:
                                            totalForadoPadraoQRCODE += 1
                                            msg = '     -> Fora do Padrão QRCODE Pasta X Mês'
                                            extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                            if flag.visualizarFP == 1:
                                                print(flag.linha_separador("_",
                                                                           "       " + tabela_arquivo.__str__() + " ",
                                                                           1))
                                                print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                    else:
                                        totalForadoPadraoQRCODE += 1
                                        msg = '     -> Fora do Padrão QRCODE'
                                        extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                        if flag.visualizarFP == 1:
                                            print(flag.linha_separador("_", "       " + tabela_arquivo.__str__() + " ",
                                                                       1))
                                            print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                        else:
                            continue

                    msg = flag.linha_separador(">", '', 2)
                    extrato.write(str(msg) + '\n')
                    print(flag.linha_separador(">", '', 2))
                    msg = "     ->(2º) FOLHAS DE PONTO (Empresa: " + coligada[empresa].__str__() + ") "
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     ->    Total Cadastro Realizado: ' + totalCadastroRealizado.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTGREEN_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     ->    Total Retorno Atualizado: ' + totalRetornoAtualizado.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTBLUE_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     ->         Total Já Cadastrado: ' + totalJaCadastrado.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTCYAN_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     -> Total Fora do Padrão QRCODE: ' + totalForadoPadraoQRCODE.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     ->             Total Duplicado: ' + totalDuplicado.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = flag.linha_separador("_", '', 2)
                    extrato.write(str(msg) + '\n')
                #notifica.clear()
                extrato.write(str('\n-> Operação Concluída! As ' + datetime.today().strftime(
                    '%d/%m/%Y %H:%M:%S').__str__()) + '\n')
            except df_bd.ErSror as err:
                notifica.clear()
                msg = ' -> Opa problema! :(' + err + ')'
                extrato.write(str(msg) + '\n')
                print(f"Erro: '{err}'")

            print()

        flag.mail('Notificação de Erros SONAR', 'Protocolo', 'protocolo@gruposerval.com.br',
                  'Segue em anexo os erros encontrados no Processo ' + datetime.today().strftime(
                      '%d/%m/%Y %H:%M:%S').__str__(),
                  'Folha de Ponto', mapeado + 'Folhas_de_Ponto_2024.txt')
        print(flag.linha_separador(">", '', 2))
    def executeFolha(self, mapeado):
        print(
            Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag1.linha_separador("<", " Iniciando - FOLHAS DE PONTO - 2022",
                                                                                   1))
        self.listarbanco(mapeado)
        time.sleep(60)
        while True:
            now = datetime.now()
            agora = int(now.strftime("%H"))
            if Flag1.ini <= agora <= Flag1.fim:
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag1.linha_separador("<",
                                                                                             " Iniciando - FOLHAS DE PONTO - 2022",
                                                                                             1))
                inicio = time.perf_counter()
                self.radar(mapeado)
                fim = time.perf_counter()
                total = round(fim - inicio, 2)
                print(f'Tempo de execução: {total} segundos')

                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag1.linha_separador(">",
                                                                                             " Concluído - FOLHAS DE PONTO - 2022",
                                                                                             3))
                Flag1.roda_pe()
                time.sleep(Flag1.velocFP_FI)
            else:
                time.sleep(1800)
    def executeFolha2023(self, mapeado):

        print(
            Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag1.linha_separador("<", " Iniciando - FOLHAS DE PONTO - 2023",
                                                                                   1))
        self.listarbanco2023(mapeado)
        time.sleep(60)
        while True:
            now = datetime.now()
            agora = int(now.strftime("%H"))
            if Flag1.ini <= agora <= Flag1.fim:
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag1.linha_separador("<",
                                                                                             " Iniciando - FOLHAS DE PONTO - 2023",
                                                                                             1))
                inicio = time.perf_counter()
                self.radar2023(mapeado)
                fim = time.perf_counter()
                total = round(fim - inicio, 2)
                print(f'Tempo de execução: {total} segundos')
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag1.linha_separador(">",
                                                                                             " Concluído - FOLHAS DE PONTO - 2023",
                                                                                             3))
                Flag1.roda_pe()
                time.sleep(Flag1.velocFP_FI)
            else:
                time.sleep(1800)
    def executeFolha2024(self, mapeado):

        print(
            Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag1.linha_separador("<", " Iniciando - FOLHAS DE PONTO - 2024",
                                                                                   1))
        self.listarbanco2024(mapeado)
        time.sleep(60)
        while True:
            now = datetime.now()
            agora = int(now.strftime("%H"))
            if Flag1.ini <= agora <= Flag1.fim:
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag1.linha_separador("<",
                                                                                             " Iniciando - FOLHAS DE PONTO - 2024",
                                                                                             1))
                inicio = time.perf_counter()
                self.radar2024(mapeado)
                fim = time.perf_counter()
                total = round(fim - inicio, 2)
                print(f'Tempo de execução: {total} segundos')
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag1.linha_separador(">",
                                                                                             " Concluído - FOLHAS DE PONTO - 2024",
                                                                                             3))
                Flag1.roda_pe()
                time.sleep(Flag1.velocFP_FI)
            else:
                time.sleep(1800)

