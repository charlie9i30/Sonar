import pandas as pd
from utils import Funcoes
from utils import DataFrame
from colorama import Fore, Back, Style
import time
from utils import Funcoes
from datetime import date, datetime, timedelta

class notificacao(object):
    global df;
    global stop_rodando_notificacao;
    global Flag;


    Flag = Funcoes.Flag()
    df = DataFrame.DB()


    def lista_notificacao(self):

        df_lista_atendimentos = pd.DataFrame(df.select_lista_atendimentos())
        df_lista_comercial = pd.DataFrame(df.select_lista_contratos())
        df_lista_carimbo = pd.DataFrame(df.select_lista_carimbos())

        dados_lista_atendimentos = pd.DataFrame(df_lista_atendimentos, columns=['qtd', 'tipo_id', 'tipo_nome', 'departamento_id', 'departamento_nome'])
        # listaNotificados = pd.DataFrame(df.select_lista_notificados())
        # listadepartamento = dados.departamento_id.unique()
        # list_depart = Funcoes.Flag.concat(listadepartamento,',')
        # listaatendimentos = dados.tipo_id.unique()
        # lista_atend_not = Funcoes.Flag.concat(listaatendimentos,',')
        print()
        print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador(".", " Processando Notificações para o Comercial ", 1))
        self.usuarios_notificados_comercial(df_lista_comercial)
        print()
        print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador(".", " Processando Notificações para os Departamentos ", 1))
        self.usuarios_notificados_departamento(dados_lista_atendimentos)
        print()
        print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador(".", " Processando Notificações para os Usuários Especifícos ", 1)) 
        self.usuarios_notificados_atendimento(dados_lista_atendimentos)
        print()
        print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador(".", " Processando Notificações para Carimbos Pendentes ",1))
        self.usuarios_notificados_carimbo(df_lista_carimbo)
        self.stop_rodando_notificacao = [False, True]


    def usuarios_notificados_departamento(self,dados):
        # NOTIFICA INDIVIDUALMENTE CADA USUÁRIO DO DEPARTAMENTO POIS OS USUÁRIOS ESPECIFICOS SÓ PODEM SER NOTIFICADOS COM SUAS DEMANDAS

        dados = dados.drop_duplicates(subset=["departamento_id"], keep='first')
        row = len(dados.index)
        unidade = 'minute'
        time = -10   #SEMPRE INFORMAR NÚMEROS NEGATIVOS PARA VERIFICAR O ÚLTIMO PERÍODO EM QUE O MESMO FOI NOTIFICADO

        listaNotificados = pd.DataFrame(df.select_lista_notificados(unidade.__str__(),time.__str__()))

        for index in range(row):
            df_bd = pd.DataFrame(df.select_usuarios_departamento(dados['departamento_id'].iloc[index].__str__()))

            linhas = len(df_bd.index)
            for linha in range(linhas):
                if not df_bd['id'].iloc[linha] in listaNotificados.usuario_id.values:
                    titulo = dados['tipo_nome'].iloc[index]
                    texto = 'Olá, existem ' + dados['qtd'].iloc[index].__str__() + ' procotocolos de '+ titulo +' pendentes de atendimento.'
                    usuario_id = df_bd['id'].iloc[linha].__str__()
                    perfil_id = df_bd['perfil_id'].iloc[linha].__str__()
                    df.insert_notificacoes(perfil_id,titulo,texto,usuario_id)
                else:
                    continue
    def usuarios_notificados_comercial(self,dados):
        #NOTIFICAR TODOS OS USUÁRIOS DO COMERCIAL
        row = len(dados.index)
        unidade = 'day'
        time = -1       #SEMPRE INFORMAR NÚMEROS NEGATIVOS PARA VERIFICAR O ÚLTIMO PERÍODO EM QUE O MESMO FOI NOTIFICADO

        listaNotificados = pd.DataFrame(df.select_lista_notificados(unidade.__str__(),time.__str__()))

        for index in range(row):
            df_bd = pd.DataFrame(df.select_usuarios_comercial())

            linhas = len(df_bd.index)
            for linha in range(linhas):
                if not df_bd['id'].iloc[linha] in listaNotificados.usuario_id.values:
                    titulo = dados['cliente_nome'].iloc[index]
                    texto = 'Olá, o contrato ' + dados['apelido'].iloc[index].__str__() + ' se vence em '+ dados['vigencia_fim_dias'].iloc[index].__str__() + ' dias'
                    usuario_id = df_bd['id'].iloc[linha].__str__()
                    perfil_id = df_bd['perfil_id'].iloc[linha].__str__()
                    df.insert_notificacoes(perfil_id,titulo,texto,usuario_id)
                else:
                    continue

    def notificacoes_email_comercial(self,dados):
        row = len(dados.index)

        for index in range(row):
            texto1 = 'O contrato ' + dados['apelido'].iloc[index].__str__() + ' se vence em ' + \
                     dados['vigencia_fim_dias'].iloc[index].__str__() + ' dias'
            Flag.mail_comercial('Notificação Contratos Sistema Tela', 'Comercial',
                                ['comercial@gruposerval.com.br'],
                                'Este é um lembrete gerado automaticamente',
                                texto1)

            #AGUARDANDO MODELAGEM DE DADOS PARA REALIZAR A NOTIFICAÇÃO

    def usuarios_notificados_atendimento(self,dados):

        row = len(dados.index)
        unidade = 'minute'
        time = -10    #SEMPRE INFORMAR NÚMEROS NEGATIVOS PARA VERIFICAR O ÚLTIMO PERÍODO EM QUE O MESMO FOI NOTIFICADO

        listaNotificados = pd.DataFrame(df.select_lista_notificados(unidade.__str__(), time.__str__()))

        for index in range(row):
            df_bd = pd.DataFrame(df.select_usuarios_atendimentos(dados['tipo_id'].iloc[index].__str__()))
            for linha in range(len(df_bd.index)):
                if not df_bd['id'].iloc[linha] in listaNotificados.usuario_id.values:
                    titulo = dados['tipo_nome'].iloc[index]
                    texto = 'Olá, existem ' + dados['qtd'].iloc[index].__str__() + ' procotocolos de ' + titulo + ' pendentes de atendimento.'
                    usuario_id = df_bd['id'].iloc[linha].__str__()
                    perfil_id = df_bd['perfil_id'].iloc[linha].__str__()
                    df.insert_notificacoes(perfil_id, titulo, texto, usuario_id)
                else:
                    continue
    def usuarios_notificados_carimbo(self,dados):
        # NOTIFICA INDIVIDUALMENTE USUÁRIO

        row = len(dados.index)
        unidade = 'minute'
        time = -10   #SEMPRE INFORMAR NÚMEROS NEGATIVOS PARA VERIFICAR O ÚLTIMO PERÍODO EM QUE O MESMO FOI NOTIFICADO

        listaNotificados = pd.DataFrame(df.select_lista_notificados(unidade.__str__(),time.__str__()))

        for index in range(row):
            df_bd = pd.DataFrame(df.select_usuarios_carimbos(dados['pessoa_id'].iloc[index].__str__()))
            for linha in range(len(df_bd.index)):
                if not df_bd['id'].iloc[linha] in listaNotificados.usuario_id.values:
                    titulo = dados['nome'].iloc[linha]
                    texto = 'Olá, existem ' + dados['qtd'].iloc[linha].__str__() + ' carimbos de ' + titulo + ' pendentes de assinatura.'
                    usuario_id = df_bd['id'].iloc[linha].__str__()
                    perfil_id = df_bd['perfil_id'].iloc[linha].__str__()
                    df.insert_notificacoes(perfil_id, titulo, texto, usuario_id)
                else:
                    continue



    def executeNotificacao(self):
        df.delete_limpar_notificacoes()
        while True:
            now = datetime.now()
            agora = int(now.strftime("%H"))
            if Flag.ini <= agora <= Flag.fim:
                print()
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador("<", " Iniciando - Notificações ", 1))
                self.lista_notificacao()
                print()
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador(">", " Concluído - Notificações ", 3))
                time.sleep(30)
            else:
                time.sleep(1800)


    def usuarios_notificaolog(self,usuario_id,notificacao_id):

            df.insert_notificacoeslog(usuario_id,notificacao_id)

