import requests as rq
from utils import DataFrame
from PIL import Image
import pandas as pd
import time
from utils import Funcoes
from colorama import Fore, Back, Style
from datetime import date, datetime, timedelta
import threading
import multiprocessing
import concurrent.futures
import urllib.request
from urllib.parse import urlparse
from urllib.parse import parse_qs
import os

class requests(object):
    global stop_rodando_requests;
    global Flag;

    Flag = Funcoes.Flag()

    def call_requests(self):
        df = DataFrame.DB()
        df.update_filaProcessos_FolhaDePonto()
        listaProcessos = pd.DataFrame(df.select_processos_FolhaDePonto())
        linhas = len(listaProcessos.index)

        for linha in range(linhas):
            status = 0
            df.update_processos_usuariosprocessos(listaProcessos['id'].iloc[linha].__str__(), status.__str__())

            url = listaProcessos['processo'].iloc[linha].__str__()
            parsed_url = urlparse(url)
            query = parse_qs(parsed_url.query)

            print(Flag.linha_separador("_", "  " + query.__str__(), 1))
            try:
                request = rq.get(listaProcessos['processo'].iloc[linha].__str__())
            except:
                status = 1
                df.update_processos_usuariosprocessos(listaProcessos['id'].iloc[linha].__str__(), status.__str__())
                return None
            if (request.status_code == 200):
                df.delete_processos_usuariosprocessos(listaProcessos['id'].iloc[linha].__str__())
                print(Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + '     -> Requisição Realizada!')
                time.sleep(20)
            else:
                status = 1
                df.update_processos_usuariosprocessos(listaProcessos['id'].iloc[linha].__str__(), status.__str__())
                print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + '     -> Falha na Requisição!')
                continue;



    def call_request_thread(self,id,processo):
        df = DataFrame.DB()
        status = 0
        df.update_processos_usuariosprocessos(id, status.__str__())
        url = processo
        parsed_url = urlparse(url)
        query = parse_qs(parsed_url.query)
        print(Flag.linha_separador("_", "  " + query.__str__(), 1))
        try:
            request = rq.get(processo)
        except:
            status = 1
            df.update_processos_usuariosprocessos(id, status.__str__())
            return None
        if (request.status_code == 200):
            df.delete_processos_usuariosprocessos(id)
            print(Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + '     -> Requisição Realizada!')
        else:
            status = 1
            df.update_processos_usuariosprocessos(id, status.__str__())
            print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + '     -> Falha na Requisição!')

    def call_requestdw_thread(self,id,processo):
        df = DataFrame.DB()
        try:
            print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador("_", "  " + processo.__str__(), 1))
            df.update_processosdw_status(id.__str__(), 0)
            request = rq.get(processo.__str__())
        except:
            df.update_processosdw_status(id.__str__(), 1)
        if (request.status_code == 200):
            print(
                Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + '     -> Requisição ' + processo.__str__() + ' realizada!')
            df.update_processoshttp_dw(id.__str__())
            df.update_processosdw_status(id.__str__(), 1)
        else:
            print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + '     -> Falha na Requisição ' + processo.__str__())
            df.update_processosdw_status(id.__str__(), 1)

    def call_request_thread_qrcode(self,req,chapa,codcoligada,cpf,qrlink,mapeado):
        url = req
        parsed_url = urlparse(url)
        query = parse_qs(parsed_url.query)

        try:
            request = rq.get(req)
        except:
            return None
        if (request.status_code == 200):
            '''dir_path = mapeado + '\\' + codcoligada + '.' + chapa + '.png'''
            '''if not os.path.exists(dir_path):
                urllib.request.urlretrieve(qrlink, dir_path)
                print(Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + '     -> QRCODE Baixado! ' + codcoligada + '.' + chapa + '.png')
            '''
            print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + '     -> Requisição do QRCODE Realizada ' + codcoligada + '.' + chapa)

            dir_path_alter = mapeado + '\\' + cpf + '.png'
            if not os.path.exists(dir_path_alter):
                urllib.request.urlretrieve(qrlink, dir_path_alter)
                print(Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + '     -> QRCODE Baixado! ' + cpf +'-QRCODE'+'.png')
            else:
                None
        else:
            print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + '     -> Falha na Requisição!')

    def call_request_thread_foto(self,req,chapa,codcoligada,cpf,qrlink,mapeado):
        url = req
        parsed_url = urlparse(url)
        query = parse_qs(parsed_url.query)

        try:
            request = rq.get(req)
        except:
            return None
        if (request.status_code == 200):
            print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + '     -> Requisição da Foto Realizada ' + codcoligada + '.' + chapa)

            dir_path_alter = mapeado + '\\' + cpf + '.jpeg'
            if not os.path.exists(dir_path_alter):
                urllib.request.urlretrieve(qrlink, dir_path_alter)
                print(Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + '     -> Foto Baixada! ' + cpf +'.jpeg')
            else:
                None
        else:
            print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + '     -> Falha na Requisição!')

    def call_request_procedure(self):
        df = DataFrame.DB()
        listaProcessos = pd.DataFrame(df.select_procedures_usuariosprocessos())
        folha_fila = df.select_fila_usuarios_processos()
        linhas = len(listaProcessos.index)
        listaReq = []
        listaAtt = []

        if (linhas >= 10 and not listaProcessos['processo'].str.contains('proc_ferias_gera_ocorrencia_matricula').any()):
            laco = 3 if linhas > 3 else linhas
            for linhat in range(laco):
                t = threading.Thread(target=Flag.conexaoV, args=['SET NOCOUNT ON; '+listaProcessos['processo'].iloc[linhat].__str__(),' Request -> '])
                listaAtt.append(listaProcessos['id'].iloc[linhat].__str__())
                listaReq.append(t)

            for update in listaAtt:
                status = 0
                df.update_processos_usuariosprocessos(update.__str__(), status.__str__())
            for x1 in listaReq:
                x1.daemon = True
                x1.start()
            for x1 in listaReq:
                x1.join()
            for delete in listaAtt:
                df.delete_processos_usuariosprocessos(delete.__str__())
            return None
        else:
            for linha in range(linhas):
                status = 0
                df.update_processos_usuariosprocessos(listaProcessos['id'].iloc[linha].__str__(), status.__str__())

                try:
                    Flag.conexaoV('SET NOCOUNT ON; '+listaProcessos['processo'].iloc[linha].__str__(),' Request -> ')
                except:
                    status = 1
                    df.update_processos_usuariosprocessos(listaProcessos['id'].iloc[linha].__str__(), status.__str__())
                    return None
                df.delete_processos_usuariosprocessos(listaProcessos['id'].iloc[linha].__str__())
                print(Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + '     -> Requisição Realizada!')

    def call_request_qrcode(self,mapeado):
        df = DataFrame.DB()
        listaLinks = pd.DataFrame(df.select_qrcode_cracha())
        with concurrent.futures.ThreadPoolExecutor() as executor:
            processos = [executor.submit(self.call_request_thread_qrcode,
                             listaLinks['qrcode'].iloc[i].__str__(),
                             listaLinks['chapa'].iloc[i].__str__(),
                             listaLinks['codcoligada'].iloc[i].__str__(),
                             listaLinks['cpf'].iloc[i].__str__(),
                             listaLinks['qrcode_download'].iloc[i].__str__(),mapeado) for i in range(len(listaLinks.index))]

            concurrent.futures.wait(processos, return_when='ALL_COMPLETED')

    def call_request_foto(self,mapeado):
        df = DataFrame.DB()
        listaLinks = pd.DataFrame(df.select_foto_cracha())
        with concurrent.futures.ThreadPoolExecutor() as executor:
            processos = [executor.submit(self.call_request_thread_foto,
                             listaLinks['imagem'].iloc[i].__str__(),
                             listaLinks['chapa'].iloc[i].__str__(),
                             listaLinks['codcoligada'].iloc[i].__str__(),
                             listaLinks['cpf'].iloc[i].__str__(),
                             listaLinks['imagem_download'].iloc[i].__str__(),mapeado) for i in range(len(listaLinks.index))]

            concurrent.futures.wait(processos, return_when='ALL_COMPLETED')




    def call_resquest_dw(self):
        df = DataFrame.DB()
        listaProcessos = pd.DataFrame(df.select_processoshttp_dw())
        linhas = len(listaProcessos.index)
        listaReq = []

        for linhat in range(linhas):
            t = threading.Thread(target=self.call_requestdw_thread,
                                 args=[listaProcessos['id'].iloc[linhat].__str__(),listaProcessos['processo'].iloc[linhat].__str__()])
            listaReq.append(t)

        for x1 in listaReq:
            x1.daemon = True
            x1.start()
        for x1 in listaReq:
            x1.join()

        return None

    def down_request(self):
        df = DataFrame.DB()
        listaSessoes = pd.DataFrame(df.select_lista_sessoes())

        linhas = len(listaSessoes.index)
        for linha in range(linhas):
            df.kill_session_id(listaSessoes['session_id'].iloc[linha].__str__())
            Flag.mail_down_request('Conexão Encerrada pelo SONAR', 'Rogério',
                      ['charles.souza@gruposerval.com.br','rogerio.barbosa@gruposerval.com.br','contato@zoesistemas.com.br'],
                      'O usuário '+ listaSessoes['login_name'] + ' teve sua sessão ' + listaSessoes['session_id'].iloc[linha].__str__() +' encerrada por ter excedido o tempo de execução de consultas de '+Flag.requests_time_conect.__str__()+'min,'
                      ' a conexão encerrada era de: '+listaSessoes['wait_time_min'].iloc[linha].__str__()+'min enquanto tentava acessar o banco '+ listaSessoes['database'],
                      listaSessoes['statement_text'].iloc[linha].__str__())
            break;

    def executeRequest(self):
        while True:
            now = datetime.now()
            agora = int(now.strftime("%H"))
            if Flag.ini <= agora <= Flag.fim:
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador("<", " Iniciando - Requisições ", 1))
                self.call_requests()
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador(">", " Concluído - Requisições ", 3))
                time.sleep(Flag.velocReq)
            else:
                time.sleep(1800)
    def executeRequestProcedure(self):
        while True:
            now = datetime.now()
            agora = int(now.strftime("%H"))
            if Flag.ini <= agora <= Flag.fim:
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador("<", " Iniciando - Requisições de Procedures ", 1))
                self.call_request_procedure()
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador(">", " Concluído - Requisições de Procedures ", 3))
                time.sleep(Flag.velocReq)
            else:
                time.sleep(1800)
    def executeRequestQrcode(self,mapeado):
        while True:
            print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador("<", " Iniciando - Requisições de QRCode ", 1))
            self.call_request_qrcode(mapeado)
            print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador(">", " Concluído - Requisições de QRCode ", 3))
            time.sleep(Flag.velocQR)
    def executeRequestFoto(self,mapeado):
        while True:
            print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador("<", " Iniciando - Requisições de Foto do Colaborador ", 1))
            self.call_request_foto(mapeado)
            print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador(">", " Concluído - Requisições de Foto do Colaborador", 3))
            time.sleep(Flag.velocQR)
    def execute_down_request(self):
        while True:
            now = datetime.now()
            agora = int(now.strftime("%H"))
            if Flag.ini <= agora <= Flag.fim:
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador("<"," Iniciando Verificação para Encerrar Consultas ",1))
                self.down_request()
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador(">", " Verificação de Consultas Concluídas ", 3))
                time.sleep(300)
            else:
                time.sleep(1800)

    def execute_call_requestdw(self):
        while True:
            now = datetime.now()
            agora = int(now.strftime("%H"))
            if Flag.ini <= agora <= Flag.fim:
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador("<"," Iniciando Requisições HTTP DW ",1))
                self.call_resquest_dw()
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag.linha_separador(">"," Concluído - Requisições HTTP DW ",3))
                time.sleep(Flag.velocReq)
            else:
                time.sleep(1800)