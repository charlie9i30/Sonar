import fitz  # Biblioteca PyMuPDF
from PIL import Image, ImageOps
import fitz  # PyMuPDF
import pytesseract
from PIL import Image
import io
import PyPDF4
import re
import os,argparse
import pdfkit
import pyodbc
import time
from utils import Funcoes, DataFrame
from datetime import date, datetime, timedelta
import pytesseract
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

class Aquivos_PDF(object):


    def __init__(self):
        pass
    Flag = Funcoes.Flag()
    df = DataFrame.DB()

    def ler_arquivo(self, diretorio, arq):
        arquivo = diretorio + arq
        # Ler o arquivo PDF
        with open(arquivo, 'rb') as arquivo_pdf:
            reader = fitz.open(arquivo)
            # Iterar sobre as páginas
            for pagina_numero in range(reader.page_count):
                pagina = reader[pagina_numero]


                # Extrair texto e posições
                dados_texto = pagina.get_text("dict")
                # 'dados_texto' contém 'blocks', cada um representando elementos textuais ou imagens
                for block in dados_texto['blocks']:
                    if block['type'] == 0:  # Bloco de texto
                        for line in block['lines']:
                            for span in line['spans']:
                                print(f"Texto: {span['text']}, Posição: {span['bbox']}")
                                # Aqui, 'bbox' é uma tupla (x0, y0, x1, y1) representando a posição do texto




                # Extrair texto da página
                # texto_pagina = pagina.get_text()
                # if len(texto_pagina)>0:
                #     self.df.inserir_dadospdf('rogerio.barbosa','rogerio.barbosa',1, 124, arq.replace('/', ''), pagina_numero + 1, texto_pagina.replace('\n', '¬'))
                # else:
                #     self.ler_pdf_como_imagem(arquivo)



    def folha_analitica_matricula(self, arquivo_pdf, matricula, nome_evento, texto_limite):
        documento = fitz.open(arquivo_pdf)  # Abre o arquivo PDF
        valor_alvo = None  # Para armazenar o valor alvo encontrado

        for numero_pagina in range(documento.page_count):  # Itera por cada página do documento
            pagina = documento[numero_pagina]
            dados_texto = pagina.get_text("dict")

            matricula_encontrada = False
            texto_alvo_encontrado = False

            # Itera por todos os blocos de texto na página
            for block in dados_texto['blocks']:
                if block['type'] == 0:  # Apenas blocos de texto
                    for line in block['lines']:
                        for span in line['spans']:
                            texto_atual = span['text']

                            # Verifica se a matrícula foi encontrada
                            if matricula in texto_atual:
                                matricula_encontrada = True

                            # Após encontrar a matrícula, busca pelo texto alvo
                            if matricula_encontrada and nome_evento in texto_atual:
                                texto_alvo_encontrado = True
                            elif matricula_encontrada and texto_alvo_encontrado:
                                # Assume que o valor alvo está no span seguinte após o texto alvo
                                valor_alvo = texto_atual
                                break  # Encerra a busca após encontrar o valor alvo
                            elif matricula_encontrada and texto_limite in texto_atual:
                                # Encerra a busca se encontrar o texto limite após a matrícula
                                break

                        if valor_alvo or texto_limite in texto_atual:
                            break  # Encerra as iterações nas linhas se o valor foi encontrado ou atingiu o texto limite

                    if valor_alvo or texto_limite in texto_atual:
                        break  # Encerra as iterações nos blocos se o valor foi encontrado ou atingiu o texto limite

            if valor_alvo or texto_limite in texto_atual:
                break  # Encerra as iterações nas páginas se o valor foi encontrado ou atingiu o texto limite

        if valor_alvo:
            print(f"Valor do Evento [{nome_evento}]: {valor_alvo}")
        else:
            print("Evento não encontrado ou texto limite atingido antes de localizar o valor alvo.")

    # Exemplo de chamada da função
    # extrair_valor_apos_texto_alvo('caminho_para_seu_arquivo.pdf', '0046199', 'Base INSS - Envelope', 'Num. Depend. IRRF')










    def ler_pdf_como_imagem(self, caminho_do_pdf):
        # Abre o arquivo PDF
        documento = fitz.open(caminho_do_pdf)

        for numero_pagina in range(len(documento)):
            pagina = documento.load_page(numero_pagina)  # Carrega cada página
            pix = pagina.get_pixmap()  # Gera um pixmap (imagem) da página
            imagem_bytes = pix.tobytes("png")  # Converte o pixmap em bytes de imagem PNG
            imagem = Image.open(io.BytesIO(imagem_bytes))  # Cria uma imagem PIL

            # Aplica OCR na imagem usando pytesseract
            texto = pytesseract.image_to_string(imagem, lang='por')  # Use "lang='eng'" para inglês ou outro idioma conforme necessário

            # Faça algo com o texto, como imprimir ou armazenar
            print(f"Texto da Página {numero_pagina + 1}:\n{texto}")

# Exemplo de uso
# leitor_pdf = PDFReader()
# leitor_pdf.ler_pdf_como_imagem('/caminho/para/o/arquivo.pdf')

