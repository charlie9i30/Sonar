import pandas as pd
import gc
import asyncio
import threading
import time
from tqdm import tqdm
import concurrent.futures
from utils import Funcoes, DataFrame
from colorama import Fore, Back, Style
from datetime import date, datetime, timedelta

flag = Funcoes.Flag()

class DW(object):
    global stop_rodando_dw;
    global df;

    df = DataFrame.DB()

    def listar_tarefas(self):
        count = 0
        #flag = Funcoes.Flag()
        # --------------------------------------------------
        df = DataFrame.DB()
        df_bd = pd.DataFrame(df.select_process_dw())
        #df_bd = df_bd.set_index('id')
        # --------------------------------------------------
        codigo = list()
        comando = list()
        listaReq = []
        listaAtt = []
        if(len(df_bd.index) > 5):
            for linha in range(len(df_bd.index)):
                try:
                    flag.conexaoV('SET NOCOUNT ON; ' + df_bd['processo'].iloc[linha].__str__(), ' DW -> ')
                    time.sleep(300)
                except:
                    continue
        else:
            for linhat in range(len(df_bd.index)):
                t = threading.Thread(target=flag.conexaoV, args=['SET NOCOUNT ON; ' + df_bd['processo'].iloc[linhat].__str__(),'DW -> '])
                listaAtt.append(df_bd['id'].iloc[linhat].__str__())
                listaReq.append(t)

            for update in listaAtt:
                df.update_process_dw(update.__str__())
            for x1 in listaReq:
                x1.daemon = True
                x1.start()
            for x1 in listaReq:
                x1.join()
                gc.collect(generation=2)





    def listar_tarefas_dw(self):
        count = 0
        #flag = Funcoes.Flag()
        # --------------------------------------------------
        df = DataFrame.DB()
        df_bd = pd.DataFrame(df.select_process_dw1())
        #df_bd = df_bd.set_index('id')
        # --------------------------------------------------
        codigo = list()
        comando = list()
        listaReq = []
        listaAtt = []

        for linhat in range(len(df_bd.index)):
            t = threading.Thread(target=flag.conexaoV, args=['SET NOCOUNT ON; ' + df_bd['processo'].iloc[linhat].__str__(),'DW -> '])
            listaAtt.append(df_bd['id'].iloc[linhat].__str__())
            listaReq.append(t)

        for update in listaAtt:
            df.update_process_dw(update.__str__())
        for x1 in listaReq:
            x1.daemon = True
            x1.start()
        for x1 in listaReq:
            x1.join()



    def executeAllDW(self):
        df.update_process_dw_start()
        while True:
            now = datetime.now()
            agora = int(now.strftime("%H"))
            lista_de_processos = df.select_fila_usuarios_processos()
            if (flag.ini-4 <= agora <= flag.fim and len(lista_de_processos.index) == 0):
                print()
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + flag.linha_separador("<", " Iniciando - Data WareHouse ",1))
                self.listar_tarefas()
                time.sleep(flag.velocDW)
            else:
                time.sleep(1800)

    def executeDW(self):
        df.update_process_dw_start()
        while True:
            now = datetime.now()
            agora = int(now.strftime("%H"))
            lista_de_processos = df.select_fila_usuarios_processos()
            if ((agora >= 19) or (agora <= 6)):
                print()
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + flag.linha_separador("<",
                                                                                            " Iniciando - Data WareHouse ",
                                                                                            1))
                self.listar_tarefas_dw()
                time.sleep(flag.velocDW)
            else:
                time.sleep(1800)
