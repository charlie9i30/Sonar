import os
import pandas as pd
import sonar
from colorama import Fore, Back, Style
import time
from tqdm import tqdm
from utils import Funcoes
from utils import DataFrame
from datetime import date, datetime, timedelta


class ListarDiretorio(object):
    global stop_rodando_ficha;
    global operacao;
    global Flag1;

    Flag1 = Funcoes.Flag()
    def radar(self, mapeado):
        flag = Funcoes.Flag()
        coligada = flag.coligadas_operacional()
        hoje = datetime.today().strftime('%d/%m/%Y %H:%M:%S')
        minutos_Ficha_Insp = datetime.strptime(hoje, '%d/%m/%Y %H:%M:%S')
        # --------------------------------------------------
        pr = pd.period_range(start='2022-11-02', end=date.today(), freq='M')

        tempo = list(datetime.strptime(
            period.day.__str__().zfill(2) + '-' + period.month.__str__().zfill(
                2) + '-' + period.year.__str__().zfill(4),
            '%d-%m-%Y').date() for period in pr)

        # T1 = date.today()
        # T0 = (T1 + timedelta(days=30))
        # T2 = (T1 - timedelta(days=30))
        # T3 = (T2 - timedelta(days=30))

        # ++++++++++++++++++++++++++++++++++++++++++++++++++
        # tempo = (T3, T2, T1, T0)
        tempo_inicial = datetime.now()
        arq_mais_recente_fi = tempo_inicial + timedelta(days=-365)


        for empresa in coligada:
            mape = mapeado + coligada[empresa]
            for mes_atual in tempo:
                cam = mape + '\\ANO ' + mes_atual.year.__str__() + '\\' + 'MES ' + flag.mes(mes_atual.month)
                dir_path = cam
                # print(flag.linha_separador(">", '', 2))
                # print(flag.linha_separador(" ", " " + dir_path.__str__() + " ", 2))
                # Preparando o Dataframe para o Período atual
                if os.path.exists(dir_path):
                    # print('Listando...')
                    print(flag.linha_separador(" ", "-> FICHA DE INSPEÇÃO: " + coligada[empresa].__str__() + " -> " + flag.nome_mes(
                       mes_atual.month).upper().__str__() + "/" + mes_atual.year.__str__() + " ", 1))

                    for entry in tqdm(os.listdir(dir_path)):
                        time.sleep(0.02)
                        # print(entry.__str__())
                        path = os.path.join(dir_path, entry)
                        stats = os.stat(path)
                        arquivo = datetime.fromtimestamp(stats.st_atime)
                        c = arquivo - arq_mais_recente_fi
                        if c.days >= 0 and c.seconds >= 0:
                            arq_mais_recente_fi = arquivo

                else:
                    continue

        #arq_corte = (datetime.now() + timedelta(minutes=-flag.minutos_Ficha_Insp))
        arq_minutes_final_fi = arq_mais_recente_fi - minutos_Ficha_Insp
        arq_minutes = divmod(arq_minutes_final_fi.total_seconds(), 60)

        if arq_mais_recente_fi < minutos_Ficha_Insp:
            print('        Tempo referência : ' + minutos_Ficha_Insp.__format__('%d/%m/%Y %H:%M:%S').__str__())
            print('     Data último arquivo : ' + arq_mais_recente_fi.__format__('%d/%m/%Y %H:%M:%S').__str__() + '. Processando...')
            sonar.minutos_Ficha_Insp = arq_mais_recente_fi
            self.listarbanco(mapeado)
        else:
            self.stop_rodando_ficha = [False, True]
            print('     Data último arquivo : ' + arq_mais_recente_fi.__format__('%d/%m/%Y %H:%M:%S').__str__())

    def listarbanco(self, mapeado):
        self.stop_rodando_ficha = [True, False]
        flag = Funcoes.Flag()
        with open(mapeado +'Ficha_Inspecao.txt', 'w') as extrato:
            extrato.write(str('-> FICHA DE INSPEÇÃO: Dados para análise e correção:' + '\n-> Iniciado as ' + datetime.today().strftime('%d/%m/%Y %H:%M:%S').__str__()) + '\n')
            try:
                notifica = list()
                coligada = flag.coligadas_operacional()
                # --------------------------------------------------
                df = DataFrame.DB()
                df_bd = pd.DataFrame(df.select_formsregistrosrespostas40())
                # print(df_bd.describe())
                # --------------------------------------------------
                pr = pd.period_range(start='2022-11-02', end=date.today(), freq='M')

                tempo = list(datetime.strptime(
                    period.day.__str__().zfill(2) + '-' + period.month.__str__().zfill(
                        2) + '-' + period.year.__str__().zfill(4),
                    '%d-%m-%Y').date() for period in pr)
                # T1 = date.today()
                # T0 = (T1 + timedelta(days=30))
                # T2 = (T1 - timedelta(days=30))
                # T3 = (T2 - timedelta(days=30))

                # ++++++++++++++++++++++++++++++++++++++++++++++++++
                # tempo = (T3, T2, T1, T0)

                for empresa in coligada:
                    totaljacadastrado = 0
                    totalretornoatualizado = 0
                    totalcadastrorealizado = 0
                    totalforadopadraoqrcode = 0
                    totalduplicado = 0
                    mape = mapeado + coligada[empresa]
                    # print(flag.linha_separador(" ", " " + coligada[empresa].__str__() + " ", 2))
                    # print(tempo)
                    for mes_atual in tempo:
                        msg = "-> FICHA DE INSPEÇÃO: " + coligada[empresa].__str__() + " -> " + flag.nome_mes(mes_atual.month).upper().__str__() + "/" + mes_atual.year.__str__()
                        extrato.write(str(msg) + '\n')
                        print(flag.linha_separador(">", '', 2))
                        print(flag.linha_separador(" ", "-> FICHA DE INSPEÇÃO: " + coligada[empresa].__str__() + "/" + flag.nome_mes(mes_atual.month).upper().__str__() + "/" + mes_atual.year.__str__() +  " ", 1))
                        # print(flag.linha_separador("_", '', 2))
                        cam = mape + '\\ANO ' + mes_atual.year.__str__() + '\\' + 'MES ' + flag.mes(mes_atual.month)
                        caminhos = cam
                        # Preparando o Dataframe para o Período atual
                        df_bd_atual = df_bd.loc[df_bd['periodo'] == flag.nome_mes(mes_atual.month).upper().__str__() + "-" + mes_atual.year.__str__()]

                        if os.path.exists(caminhos):
                            for diretorio, subpastas, arquivos in os.walk(caminhos):
                                for arquivo in tqdm(sorted(arquivos)):
                                    time.sleep(0.02)
                                    # print(diretorio + ' # ' + arquivo)
                                    tabela_arquivo = arquivo.replace("-", "_").split('_')


                                    if len(tabela_arquivo) > 3:
                                        a = tabela_arquivo[2].__str__()
                                        b = flag.nome_mes(mes_atual.month).upper()
                                        if tabela_arquivo[2].__eq__(flag.nome_mes(mes_atual.month).upper()) or len(tabela_arquivo[0])!=6 or len(tabela_arquivo[1])!=6:

                                            # aa = tabela_arquivo[0].__str__()
                                            # ab = tabela_arquivo[1].__str__()
                                            # ac = tabela_arquivo[2].__str__()
                                            # ad = tabela_arquivo[3].__str__()
                                            # Retirando o Supervisor -> tabela_arquivo[0].__str__() + '_' +
                                            linha_arquivo = tabela_arquivo[1].__str__() + '_' + tabela_arquivo[2].__str__() + '_' + tabela_arquivo[3].__str__()  # Para confronto

                                            # Verifica se já existe no BD
                                            cadastro_ok = 0
                                            for label, content in df_bd_atual['indice'].items():
                                                tabela_bd = content.__str__().replace("-", "_").split('#')
                                                if len(tabela_bd) == 2:
                                                    # Para atualização do Retorno_pasta
                                                    cod = tabela_bd[0]
                                                    tabela_bd_seg = tabela_bd[1].split('_')

                                                    # ba = tabela_bd_II[0].__str__()
                                                    # bb = tabela_bd_II[1].__str__()
                                                    # bc = tabela_bd_II[2].__str__()
                                                    # bd = tabela_bd_II[3].__str__()
                                                    # Retirando o Supervisor -> tabela_bd_II[0].__str__() + '_' +
                                                    linha_bd = tabela_bd_seg[1].__str__() + '_' + tabela_bd_seg[2].__str__() + '_' + tabela_bd_seg[3].__str__()  # Para confronto
                                                    # Confronto
                                                    if linha_bd == linha_arquivo:
                                                        totaljacadastrado += 1
                                                        # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' -> Arquivo já cadastrado!')
                                                        if tabela_bd_seg[len(tabela_bd_seg)-1].__str__() != "OK":
                                                            # Resposta 346 Retorno da Pasta!
                                                            totalretornoatualizado += 1
                                                            # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + '     -> Retorno Atualizado!')
                                                            df.update_formsregistrosrespostas40(cod)

                                                        cadastro_ok = 1
                                                        break

                                            # Inserindo registro
                                            if cadastro_ok == 0:
                                                # -----------------------------------------------------------------------------------
                                                # Realizar cadastro do Registro
                                                # a = tabela_arquivo[3].__str__()
                                                # b = mes_atual.month.__str__()
                                                # c = int(tabela_arquivo[0]).__str__()
                                                try:
                                                     cliente_id=int(tabela_arquivo[0]).__str__()
                                                     posto_id=int(tabela_arquivo[1]).__str__()
                                                except:
                                                    totalforadopadraoqrcode += 1
                                                    msg = '     -> Fora do Padrão QRCODE - Carácter inválido no nome do arquivo'
                                                    extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                    if flag.visualizarFP == 1:
                                                        print(flag.linha_separador("_",
                                                                                   "       " + tabela_arquivo.__str__() + " ",
                                                                                   1))
                                                        print(
                                                            Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                    notifica.append('<br/><b>-></b> ' + tabela_arquivo.__str__())
                                                    continue

                                                # Verificando o ano...
                                                if len(tabela_arquivo[3])!=4 or len(tabela_arquivo)<=3:
                                                    totalforadopadraoqrcode += 1
                                                    msg = '     -> Fora do Padrão QRCODE'
                                                    extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')
                                                    if flag.visualizarFP == 1:
                                                        print(flag.linha_separador("_",
                                                                                   "       " + tabela_arquivo.__str__() + " ",
                                                                                   1))
                                                        print(
                                                            Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                    notifica.append('<br/><b>-></b> ' + tabela_arquivo.__str__())
                                                    continue

                                                tabela_supervisor_localidade = df.select_supervisor_localidade_formsregistrosrespostas40(
                                                                tabela_arquivo[3].__str__(),
                                                                cliente_id,
                                                                posto_id)

                                                # mes_atual.month.__str__(),# Mês
                                                # -----------------------------------------------------------------------------------
                                                # Preparando resposta 319 (Supervisor) Caso não tenha a Localidade é abortado a inserção

                                                if len(tabela_supervisor_localidade) == 0:
                                                    totalforadopadraoqrcode += 1
                                                    msg = ' -> (Erro) Supervisor não localizado - Cód. Localidade(' + posto_id + ')'
                                                    extrato.write(str(tabela_arquivo.__str__())+' ' + str(msg)+'\n')
                                                    if flag.visualizarFP==1:
                                                        print(flag.linha_separador("_","       " + tabela_arquivo.__str__() + " ", 1))
                                                        print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                    continue
                                                resposta_id = tabela_supervisor_localidade['supervisor_id'].iloc[
                                                    0]  # supervisor_id
                                                resposta_texto = tabela_supervisor_localidade['supervisor'].iloc[
                                                    0]  # supervisor

                                                # -----------------------------------------------------------------------------------
                                                # Realizar cadastro do Registro
                                                df.insert_registro_formsregistrosrespostas40()
                                                # Buscando o último registro do ppessoa_id(24084)
                                                tabela_reg_inserido = pd.DataFrame(df.select_id_formsregistrosrespostas40())
                                                valor_id = tabela_reg_inserido.loc[0, 'id']

                                                # ----------------------------------------------------------------------------------
                                                # Inserindo a resposta 319 (Supervisor) Caso não tenha a Localidade é abortado a inserção
                                                df.insert_registro_resp_319_formsregistrosrespostas40(valor_id, resposta_id,
                                                                                                      resposta_texto)

                                                # -----------------------------------------------------------------------------------
                                                # Realizar cadastro das Respostas'''
                                                # Preparando resposta 318 (Período)
                                                periodo = tabela_arquivo[2].lower().__str__() + '-' + tabela_arquivo[3].__str__()
                                                per = periodo.split('-')
                                                mes = per[0].__str__().lower().capitalize()
                                                resposta_id = flag.numero_mes(mes).__str__() + '-' + tabela_arquivo[3].__str__()
                                                resposta_texto = periodo
                                                # Período
                                                df.insert_registro_resp_318_formsregistrosrespostas40(valor_id, resposta_id,
                                                                                                      resposta_texto)
                                                # -----------------------------------------------------------------------------------
                                                # Preparando resposta 317,320,321,341,342,343,344,345
                                                resposta_id = tabela_supervisor_localidade['localidade_id'].iloc[0]  # localidade_id
                                                resposta_texto = tabela_supervisor_localidade['localidade'].iloc[0]  # localidade

                                                df.insert_registro_resp_317_320_321_341345_formsregistrosrespostas40(valor_id, resposta_id, resposta_texto)
                                                # -----------------------------------------------------------------------------------
                                                # Preparando resposta 346
                                                df.insert_registro_resp_346_formsregistrosrespostas40(valor_id)
                                                totalcadastrorealizado += 1
                                                if flag.visualizarFP == 1:
                                                    print(flag.linha_separador("_", "       " + tabela_arquivo.__str__() + " ",1))
                                                    print(Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + ' -> Cadastro Realizado!')
                                                # break

                                        else:
                                            totalforadopadraoqrcode += 1
                                            msg = '     -> Fora do Padrão QRCODE'
                                            if tabela_arquivo[2].__str__() != flag.nome_mes(mes_atual.month).upper():
                                                msg = '     -> Fora do Padrão QRCODE - Erro Mês'
                                                extrato.write(str(tabela_arquivo[0].__str__()) + '_' +str(tabela_arquivo[1].__str__()) + ' ' + str(msg) + '\n')
                                                if flag.visualizarFP == 1:
                                                    print(flag.linha_separador("_", "       " + tabela_arquivo[0].__str__() + '_' + tabela_arquivo[1].__str__() + " ", 1))
                                                    print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                notifica.append('<br/><b>-></b> ' + tabela_arquivo[0].__str__() + '_' + tabela_arquivo[1].__str__() )
                                            else:
                                                extrato.write(str(tabela_arquivo.__str__()) + ' ' + str(msg) + '\n')

                                                if flag.visualizarFP == 1:
                                                    print(flag.linha_separador("_", "       " + tabela_arquivo.__str__() + " ", 1))
                                                    print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + msg)
                                                notifica.append('<br/><b>-></b> ' + tabela_arquivo.__str__())



                    msg = flag.linha_separador(">", '', 2)
                    extrato.write(str(msg) + '\n')
                    print(flag.linha_separador(">", '', 2))
                    msg = "-> FICHA DE INSPEÇÃO (Empresa: " + coligada[empresa].__str__() + ") "
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     ->    Total Cadastro Realizado: ' + totalcadastrorealizado.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTGREEN_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     ->    Total Retorno Atualizado: ' + totalretornoatualizado.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTBLUE_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     ->         Total Já Cadastrado: ' + totaljacadastrado.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTCYAN_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     -> Total Fora do Padrão QRCODE: ' + totalforadopadraoqrcode.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = '     ->             Total Duplicado: ' + totalduplicado.__str__()
                    extrato.write(str(msg) + '\n')
                    print(Back.LIGHTWHITE_EX + Fore.LIGHTRED_EX + Style.BRIGHT + flag.linha_separador(" ", msg, 3))
                    msg = flag.linha_separador("_", '', 2)
                    extrato.write(str(msg) + '\n')
                # flag.mail('Notificação SONAR', 'Rogério', 'rogerio.barbosa@gruposerval.com.br', '<b>Coligada</b>: ' + coligada[empresa].__str__() + " <br/><br/> <b>Situação</b>: " + ' Fora do Padrão QRCODE <br/><br/> <b>Dados</b>: ' + notifica.__str__() , 'Ficha Inspeção')
                notifica.clear()
                extrato.write(str('\n-> Operação Concluída! As ' + datetime.today().strftime('%d/%m/%Y %H:%M:%S').__str__()) + '\n')
                self.stop_rodando_ficha = [False, True]

            except df_bd.ErSror as err:
                notifica.clear()
                msg = ' -> Opa problema! :(' + err + ')'
                extrato.write(str(msg) + '\n')
                print(f"Erro: '{err}'")

            print()

    def executeFicha(self,mapeado):
        print()
        print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag1.linha_separador("<"," Iniciando - Ficha de Inspenção ",1))
        self.listarbanco(mapeado)
        time.sleep(60)
        while True:
            now = datetime.now()
            agora = int(now.strftime("%H"))
            if Flag1.ini <= agora <= Flag1.fim:
                print()
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag1.linha_separador("<"," Iniciando - Ficha de Inspenção ",1))
                inicio = time.perf_counter()
                self.radar(mapeado)
                fim = time.perf_counter()
                total = round(fim - inicio, 2)
                print(f'Tempo de execução: {total} segundos')
                print()
                print(Back.LIGHTWHITE_EX + Fore.BLACK + Style.BRIGHT + Flag1.linha_separador(">", " Concluído - Ficha de Inspenção ",3))
                time.sleep(Flag1.velocFP_FI)
            else:
                time.sleep(30)
