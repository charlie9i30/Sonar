import threading
import colorama
import ctypes
from datetime import date, datetime, timedelta
from utils import Funcoes, DataFrame
from models import Leituras_NF, Ficha_Inspecao, Folha_De_Ponto, Formulario_Email, DataWareHouse, Notificacoes,Requests,ChecarProcessos,ArquivoFolha


# python -m venv env - Cria o Virtualenv
# env\Scripts\activate - Abre o Virtualenv
# pyinstaller.exe --onefile --nowindowed --icon=sonar.ico sonar.py
# python.exe -m pip install --upgrade pip
# pip install pyodbc
# pip install pandas
# pip install sqlalchemy
# pip install configparser
# pip install connectorx==0.3.2a1
# pip install colorama
# pip install progress progressbar2 alive-progress tqdm
# pip install pymem
# pip install requests
# pip install PyPDF4
# pip install pdfkit
# pip install fitz
# pip install Image
# pip install auto-py-to-exe
# pip install pymupdf pytesseract Pillow

# git add .
# git commit -m ""
# git push origin master
# git pull origin master
# DF.select_usuarios()





if __name__ == "__main__":
    ctypes.windll.kernel32.SetConsoleTitleW("SONAR")

    colorama.init(autoreset=True)
    # Inicializando contadores

    FLAG = Funcoes.Flag()
    FI = Ficha_Inspecao.ListarDiretorio()
    FO = Folha_De_Ponto.ListarDiretorio()
    DF = DataFrame.DB()
    DW = DataWareHouse.DW()
    NF = Leituras_NF.Aquivos_PDF()
    NO = Notificacoes.notificacao()
    RE = Requests.requests()
    CH = ChecarProcessos.checarProcessos()
    ARQ = ArquivoFolha.ArquivoFolha()
    FE = Formulario_Email.Formulario()

    # ---------------------------
    caminho_pdf = "./dist/"

    arq = 'FOLHA_ANALITICA II.pdf'
    arquivo = caminho_pdf + arq
    matricula = '0046199'
    nome_evento = "Base INSS - Envelope"
    texto_limite = "Num. Depend. IRRF"
    NF.folha_analitica_matricula(arquivo, matricula, nome_evento, texto_limite)




    # NF.html_pdf(caminho_pdf, input('Qual o nome do arquiva para geração do PDF?'))
    # NF.add_image_to_PDF('./dist/LOCAWEB.pdf', './dist/assinatura.png', (1,4), (10, 10), (10, 10))
    # ---------------------------
    caminho = FLAG.caminhoFP_FI
    # ---------------------------

    FLAG.titulo()

    if(FLAG.rodarRequests == 1):
        REQ = threading.Thread(target=RE.executeRequest)
        REQ.daemon = True
        REQ.start()
    if (FLAG.rodarRequestDW == 1):
        REQ1 = threading.Thread(target=RE.execute_call_requestdw)
        REQ1.daemon = True
        REQ1.start()
    if (FLAG.rodarRequestPROC == 1):
        REQ2 = threading.Thread(target=RE.executeRequestProcedure)
        REQ2.daemon = True
        REQ2.start()
    if (FLAG.rodarRequestCracha == 1):
        REQ3 = threading.Thread(target=RE.executeRequestQrcode, args=[FLAG.caminhoFotos])
        REQ3.daemon = True
        REQ3.start()
        REQ4 = threading.Thread(target=RE.executeRequestFoto, args=[FLAG.caminhoFotos])
        REQ4.daemon = True
        REQ4.start()
    if(FLAG.downRequest == 1):
        REQ4 = threading.Thread(target=RE.execute_down_request)
        REQ4.daemon = True
        REQ4.start()
    if(FLAG.rodarDW == 1):
        DW1 = threading.Thread(target=DW.executeAllDW)
        DW1.daemon = True
        DW1.start()
        DW1 = threading.Thread(target=DW.executeDW)
        DW1.daemon = True
        DW1.start()
    if (FLAG.rodarNotificacoes == 1):
        NOT = threading.Thread(target=NO.executeNotificacao)
        NOT.daemon = True
        NOT.start()
    if (FLAG.rodarfolha ==1):
        FOP = threading.Thread(target=FO.executeFolha, args=[caminho + 'FOLHAS DE PONTO\\'])
        FOP.daemon = True
        FOP.start()
        FOP1 = threading.Thread(target=FO.executeFolha2023, args=[caminho + 'FOLHAS DE PONTO\\'])
        FOP1.daemon = True
        FOP1.start()
        FOP2 = threading.Thread(target=FO.executeFolha2024, args=[caminho + 'FOLHAS DE PONTO\\'])
        FOP2.daemon = True
        FOP2.start()
    if (FLAG.rodarfichainspecao == 1):
        FIP = threading.Thread(target=FI.executeFicha, args=[caminho + 'FICHA DE INSPEÇÃO\\'])
        FIP.daemon = True
        FIP.start()
    if(FLAG.iniciarPingo == 1):
        CHE = threading.Thread(target=CH.iniciarPingo)
        CHE.daemon = True
        CHE.start()
    if(FLAG.rodarFolhaPag == 1):
        ARQFOLHA = threading.Thread(target=ARQ.executeFolhaPag)
        ARQFOLHA.daemon = True
        ARQFOLHA.start()
    if(FLAG.rodarFormularioEmail == 1):
        FORMS_EMAIL = threading.Thread(target=FE.executeFormularioEmail, args=[FLAG.caminhoZimbra])
        FORMS_EMAIL.daemon = True
        FORMS_EMAIL.start()


    while True:
        pass




