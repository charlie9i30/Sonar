import configparser
import base64
import getpass
import time
import pyodbc
from colorama import Fore, Back, Style
from sqlalchemy import text
from sqlalchemy import create_engine, engine
from sqlalchemy.exc import SQLAlchemyError
from datetime import date, datetime


class Flag:

    def __init__(self):
        pass

    cfgII = configparser.ConfigParser()
    cfgII.read('pyt.ini', encoding="utf-8")
    veloc = int(cfgII.get('conec', 'veloc'))
    velocFP_FI = int(cfgII.get('conec', 'velocFP_FI'))
    velocDW = int(cfgII.get('conec', 'velocDW'))
    velocReq = int(cfgII.get('conec', 'velocReq'))
    velocQR = int(cfgII.get('conec', 'velocQR'))
    ini = int(cfgII.get('conec', 'ini'))
    fim = int(cfgII.get('conec', 'fim'))
    visualizarFP = int(cfgII.get('conec', 'visualizarFP'))
    visualizarEmail = int(cfgII.get('conec', 'visualizarEmail'))
    folhaData = cfgII.get('conec', 'folhaData')
    rodarfichainspecao = int(cfgII.get('conec', 'rodarfichainspecao'))
    rodarfolha = int(cfgII.get('conec', 'rodarfolha'))
    rodarFormularioEmail = int(cfgII.get('conec', 'rodarFormularioEmail'))
    rodarDW = int(cfgII.get('conec', 'rodarDW'))
    rodarRequests = int(cfgII.get('conec', 'rodarRequests'))
    rodarRequestDW = int(cfgII.get('conec', 'rodarRequestDW'))
    rodarRequestPROC = int(cfgII.get('conec', 'rodarRequestPROC'))
    rodarRequestCracha = int(cfgII.get('conec', 'rodarRequestCracha'))
    downRequest = int(cfgII.get('conec', 'downRequests'))
    rodarNotificacoes = int(cfgII.get('conec', 'rodarNotificacoes'))
    requests_time_conect = int(cfgII.get('conec', 'requests_time_conect'))
    caminhoFP_FI = cfgII.get('conec', 'caminhoFP_FI')
    caminhoPingo = cfgII.get('conec', 'caminhoPingo')
    caminhoZimbra = cfgII.get('conec', 'caminhoZimbra')
    caminhoFotos = cfgII.get('conec', 'caminhoFotos')
    database = cfgII.get('conec', 'database')
    iniciarPingo = int(cfgII.get('conec', 'iniciarPingo'))
    rodarFolhaPag = int(cfgII.get('conec', 'rodarFolhaPag'))


    totalPendente = 0
    totalOk = 0
    totalProc = 0

    def titulo(self) -> None:

        print(self.linha_separador("-", ' SONAR - 1.00.0016 ', 3))
        print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' Olá, ' + self.user_logado()
              + ' - Data: ' + self.data_atual().__str__())
        print(self.linha_separador("+", '', 2))


    def roda_pe(self) -> None:
        print(self.linha_separador("+", '', 2))
        print(self.linha_separador(" ", 'Concluído ' + ' - Data: ' + self.data_atual().__str__() + " # Próxima Rotina em (" + self.veloc.__str__() + ") segundos.", 3))
        print(self.linha_separador(">", '', 2))

    def data_atual(self):
        dias = [
            'Segunda-feira',
            'Terça-feira',
            'Quarta-feira',
            'Quinta-Feira',
            'Sexta-feira',
            'Sábado',
            'Domingo'
        ]
        dt_atual = datetime.today().strftime('%d/%m/%Y %H:%M:%S')
        indice_da_semana = datetime.today().weekday()
        # print(indice_da_semana)

        dia_da_semana = dias[indice_da_semana]
        return dt_atual.__str__() + ' - ' + dia_da_semana.__str__()

    def date_para_str(self, data: date) -> str:
        return data.strftime('%d/%m/%y')

    def str_para_date(self, data: str) -> date:
        return datetime.strptime(data, '%d/%m/%y')

    def formata_float_str_moeda(self, valor: float) -> str:
        return f'R$ {valor:,.2f}'

    def user_logado(self):
        usuario_logado = getpass.getuser().upper()
        return usuario_logado

    def nome_mes(self, mes):
        x = ["", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]
        return x[mes]

    def numero_mes(self, mes):
        x = ["", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]
        y = x.index(mes)
        return y

    def mes(self, mes):
        x = {1: "01 JANEIRO", 2: "02 FEVEREIRO", 3: "03 MARÇO", 4: "04 ABRIL", 5: "05 MAIO", 6: "06 JUNHO", 7: "07 JULHO", 8: "08 AGOSTO", 9: "09 SETEMBRO", 10: "10 OUTUBRO", 11: "11 NOVEMBRO", 12: "12 DEZEMBRO"}
        # Para ler um atributo diretamente do dataframe basta utilizar o seguinte metodo dataframe.get("nome da coluna").get(posicao dentro da coluna,caso nao exista informar a mensagem)
        return x.get(mes)

    def coligadas_operacional(self):
        x = {1: 'SERVIÇOS', 2: 'SEGURANÇA', 5: 'SERVIARM', 6: 'AVANCE'}
        return x

    def clypt(self, texto):
        try:
            encoded_data = base64.b64encode(texto)
            return encoded_data
        except encoded_data.Error as err:
            print(f"Erro: '{err}'")
    def deClypt(self, texto):
        try:
            out = base64.b64decode(texto)
            return out.decode("utf-8", errors='ignore')  # "utf-8", errors='ignore'
        except out.Error as err:
            print(f"Erro: '{err}'")

    def conexao(self):
        # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' -> conexao: Iniciando...')
        cfg = configparser.ConfigParser()
        cfg.read('pyt.ini', encoding="utf-8")
        user = self.deClypt(cfg.get('conec', 'usersys'))
        passw = self.deClypt(cfg.get('conec', 'passsys'))
        server = cfg.get('conec', 'serv')
        database = cfg.get('conec', 'database')
        drive = 'ODBC Driver 17 for SQL Server'
        # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' -> conexaoII: Leu o arquivo...')
        # -----------------------
        try:
            engine = create_engine(f'mssql+pyodbc://{user}:{passw}@{server}/{database}?driver={drive}')
            conn = engine.connect()
            # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' -> conexao:Ok')
        except SQLAlchemyError as e:
            error = str(e.__dict__['orig'])
            print(Back.LIGHTRED_EX + Fore.BLACK + Style.BRIGHT + ' -> Houve problema!!! ' + error.__str__())
            return error

        return conn

    def conexaoII(self, comando):
        # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' -> conexaoII: Iniciando...')
        cfg = configparser.ConfigParser()
        cfg.read('pyt.ini', encoding="utf-8")
        user = self.deClypt(cfg.get('conec', 'usersys'))
        passw = self.deClypt(cfg.get('conec', 'passsys'))
        server = cfg.get('conec', 'serv')
        database = cfg.get('conec', 'database')
        drive = 'ODBC Driver 17 for SQL Server'
        # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' -> conexaoII: Leu o arquivo...')
        # -----------------------
        engine = create_engine(f'mssql://{user}:{passw}@{server}/{database}?driver={drive}', pool_reset_on_return=None, connect_args={'options': '-c lock_timeout=1500000 -c statement_timeout=1500000'})
        with engine.begin() as conn:
            #try:
            inicio = time.perf_counter()
            print()
            print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + 'DW -> ' + comando.__str__() + ' -> Processando...')
            comando = 'SET NOCOUNT ON; '+comando
            dados = conn.execute(text(comando))

            fim = time.perf_counter()
            total = round(fim - inicio, 2)
            if total >= 60:
                total = (total/60)
                print()
                print(Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + 'DW -> ' + comando.__str__() + " -> Concluído em  = {:.2f}".format(total) + ' minutos')
            else:
                print()
                print(Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + 'DW -> ' + comando.__str__() + " -> Concluído em  = {:.2f}".format(total) + ' segundos')

                # return dados
            '''
            except SQLAlchemyError as e:
                error = str(e.__dict__['orig'])
                print(Back.LIGHTRED_EX + Fore.BLACK + Style.BRIGHT + comando.__str__() + ' -> Houve problema!!! ' + error.__str__())
            return error
            '''


        # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' -> conexaoII:Ok')

    def conexaoIII(self, comando):
        # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' -> conexaoIII: Iniciando...')
        cfg = configparser.ConfigParser()
        cfg.read('pyt.ini', encoding="utf-8")
        user = self.deClypt(cfg.get('conec', 'usersys'))
        passw = self.deClypt(cfg.get('conec', 'passsys'))
        server = self.deClypt(cfg.get('conec', 'serv'))
        database = cfg.get('conec', 'database')
        drive = 'ODBC Driver 17 for SQL Server'
        # -----------------------
        cnxn = pyodbc.connect(f'DRIVER={drive};SERVER={server};DATABASE={database};UID={user};PWD={passw}')
        cursor = cnxn.cursor()
        # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' -> conexaoIII:Ok')

    def conexaoIV(self):
        # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' -> conexao: Iniciando...')
        cfg = configparser.ConfigParser()
        cfg.read('pyt.ini', encoding="utf-8")
        user = self.deClypt(cfg.get('conec', 'usersys'))
        passw = self.deClypt(cfg.get('conec', 'passsys'))
        server = cfg.get('conec', 'serv')
        database = cfg.get('conec', 'database')
        drive = 'ODBC Driver 17 for SQL Server'
        # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' -> conexaoII: Leu o arquivo...')
        # -----------------------
        try:
            engine = create_engine(f'mssql+pyodbc://{user}:{passw}@{server}/{database}?driver={drive}')
            conn = engine.connect()
            # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' -> conexao:Ok')
        except SQLAlchemyError as e:
            error = str(e.__dict__['orig'])
            print(Back.LIGHTRED_EX + Fore.BLACK + Style.BRIGHT + ' -> Houve problema!!! ' + error.__str__())
            return error

        return conn

    def conexaoV(self,comando,impressao = None):
        # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' -> conexao: Iniciando...')
        cfg = configparser.ConfigParser()
        cfg.read('pyt.ini', encoding="utf-8")
        user = self.deClypt(cfg.get('conec', 'usersys'))
        passw = self.deClypt(cfg.get('conec', 'passsys'))
        server = cfg.get('conec', 'serv')
        database = cfg.get('conec', 'database')
        drive = 'ODBC Driver 17 for SQL Server'

        cnxn = pyodbc.connect(f'DRIVER={drive};SERVER={server};DATABASE={database};UID={user};PWD={passw}')
        cnxn.autocommit = True
        cnxn.timeout = 0
        cursor = cnxn.cursor()
        inicio = time.perf_counter()
        print()
        print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + impressao + comando.__str__() + ' -> Processando...')
        try:
            cursor.execute(comando)
        except pyodbc.NotSupportedError as error:
            with open('errobd.txt', 'w') as extrato:
                extrato.write(str(impressao.__str__() +'Dados para análise e correção:' + '\n-> Iniciado as ' + datetime.today().strftime('%d/%m/%Y %H:%M:%S').__str__()) + '\n')
                msg = ' -> Erro Encontrado! :(' + error.__str__() + ')'
                extrato.write(str(msg) + '\n')
                print(f"Erro Encontrado: '{error.__str__()}'")
        except pyodbc.IntegrityError as error:
            with open('errobd.txt', 'w') as extrato:
                extrato.write(str(impressao.__str__() +'Dados para análise e correção:' + '\n-> Iniciado as ' + datetime.today().strftime('%d/%m/%Y %H:%M:%S').__str__()) + '\n')
                msg = ' -> Erro Encontrado! :(' + error.__str__() + ')'
                extrato.write(str(msg) + '\n')
                print(f"Erro Encontrado: '{error.__str__()}'")
        except pyodbc.DataError as error:
            with open('errobd.txt', 'w') as extrato:
                extrato.write(str(impressao.__str__() +'Dados para análise e correção:' + '\n-> Iniciado as ' + datetime.today().strftime('%d/%m/%Y %H:%M:%S').__str__()) + '\n')
                msg = ' -> Erro Encontrado! :(' + error.__str__() + ')'
                extrato.write(str(msg) + '\n')
                print(f"Erro Encontrado: '{error.__str__()}'")
        except pyodbc.ProgrammingError as error:
            with open('errobd.txt', 'w') as extrato:
                extrato.write(str(impressao.__str__() +'Dados para análise e correção:' + '\n-> Iniciado as ' + datetime.today().strftime('%d/%m/%Y %H:%M:%S').__str__()) + '\n')
                msg = ' -> Erro Encontrado! :(' + error.__str__() + ')'
                extrato.write(str(msg) + '\n')
                print(f"Erro Encontrado: '{error.__str__()}'")
        except pyodbc.OperationalError as error:
            with open('errobd.txt', 'w') as extrato:
                extrato.write(str(impressao.__str__() +'Dados para análise e correção:' + '\n-> Iniciado as ' + datetime.today().strftime('%d/%m/%Y %H:%M:%S').__str__()) + '\n')
                msg = ' -> Erro Encontrado! :(' + error.__str__() + ')'
                extrato.write(str(msg) + '\n')
                print(f"Erro Encontrado: '{error.__str__()}'")
        except pyodbc.Error as error:
            with open('errobd.txt', 'w') as extrato:
                extrato.write(str(impressao.__str__() +'Dados para análise e correção:' + '\n-> Iniciado as ' + datetime.today().strftime('%d/%m/%Y %H:%M:%S').__str__()) + '\n')
                msg = ' -> Erro Encontrado! :(' + error.__str__() + ')'
                extrato.write(str(msg) + '\n')
                print(f"Erro Encontrado: '{error.__str__()}'")

        fim = time.perf_counter()
        total = round(fim - inicio, 2)
        if total >= 60:
            total = (total / 60)
            print()
            print(
                Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + impressao + comando.__str__() + " -> Concluído em  = {:.2f}".format(
                    total) + ' minutos')
        else:
            print()
            print(
                Back.LIGHTGREEN_EX + Fore.BLACK + Style.BRIGHT + impressao + comando.__str__() + " -> Concluído em  = {:.2f}".format(
                    total) + ' segundos')

    def conexaoVI(self):
        # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' -> conexao: Iniciando...')
        cfg = configparser.ConfigParser()
        cfg.read('pyt.ini', encoding="utf-8")
        user = self.deClypt(cfg.get('conec', 'usersys'))
        passw = self.deClypt(cfg.get('conec', 'passsys'))
        server = cfg.get('conec', 'serv')
        database = cfg.get('conec', 'database')
        drive = 'ODBC Driver 17 for SQL Server'
        # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' -> conexaoII: Leu o arquivo...')
        # -----------------------
        f'mssql://{user}:{passw}@{server}/{database}?driver={drive}'

        try:
            '''
            conn = str(engine.URL.create("mssql", database=database, host=server,
                                                              username=user,
                                                              password=passw,
                                                              query={'driver': 'ODBC Driver 17 for SQL Server'}))
            '''
            conn = f'mssql://{user}:{passw}@{server}/{database}?driver={drive}'
            # print(Back.LIGHTYELLOW_EX + Fore.BLACK + Style.BRIGHT + ' -> conexao:Ok')
        except SQLAlchemyError as e:
            error = str(e.__dict__['orig'])
            print(Back.LIGHTRED_EX + Fore.BLACK + Style.BRIGHT + ' -> Houve problema!!! ' + error.__str__())
            return error

        return conn



    def linha_separador(self, caracter, titulo, posicao):
        print()
        tam = 120
        tit = len(titulo)
        if not (tit % 2 == 0):
            tit = tit - 1
        total = (tam - tit)

        car = caracter
        i = 0
        if len(titulo) > 1:

            while i < (total/2):
                caracter += car
                i += 1
            if posicao == 2:
                msg = caracter + '{' + titulo + '}' + caracter
            elif posicao == 1:
                msg = '{' + titulo + '}' + caracter + caracter
            elif posicao == 3:
                msg = caracter + caracter + '{' + titulo + '}'
        else:
            while i < (total + 3):
                caracter += car
                i += 1

            msg = caracter
        print()
        return msg

    def mail(self, subject, sender_name, receiver_email, msg, tabela, files = None):
        import smtplib
        from email.mime.text import MIMEText
        from email.mime.multipart import MIMEMultipart
        from email.mime.application import MIMEApplication
        from os.path import basename
        port = 587
        smtp_server = "smtp.gruposerval.com.br"
        login = "develop@gruposerval.com.br"  # paste your login generated by Mailtrap
        password = "!@Serval1970@!"  # paste your password generated by Mailtrap
        sender_email = "Sistema SONAR<develop@gruposerval.com.br>"
        message = MIMEMultipart("multipart")
        message["Subject"] = subject
        message["From"] = sender_email
        message["To"] = receiver_email
        # write the HTML part
        html = """\
        <html>
            <body>
            <p>Olá, <i><b>""" + sender_name.__str__() + """</b></i><br><br/>
            <b>Processo: """ + tabela.__str__() + """ </b><br/><br/>
            """ + msg.__str__() + """ </p><br/>
            
            Não responda este e-mail, gerado automaticamente! <br/><br/>
            Att,<br/>
            Sistema TELA - Copyright © 2023<br/> 
            Apoio Sistema <b>SONAR</b> - Copyright © 2023
          </body>
        </html>
        """
        # convert both parts to MIMEText objects and add them to the MIMEMultipart message
        #part1 = MIMEText(text, "plain")
        part2 = MIMEText(html, "html","utf-8")

        message.attach(MIMEText(msg))

        f = files
        with open(f, "rb") as fil:
            part = MIMEApplication(
                fil.read(),
                Name=basename(f)
            )
        # After the file is closed
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
        message.attach(part)

        #message.attach(part1)
        message.attach(part2)
        # send your email
        with smtplib.SMTP(smtp_server, port) as server:
            server.login(login, password)
            server.sendmail(
                sender_email, receiver_email, message.as_string()
            )
        print()
        print('Notificação via e-mail enviado!' + receiver_email.__str__())

    def mail_down_request(self, subject, sender_name, receiver_email, msg, tabela):
        import smtplib
        from email.mime.text import MIMEText
        from email.mime.multipart import MIMEMultipart
        port = 587
        smtp_server = "smtp.gruposerval.com.br"
        login = "develop@gruposerval.com.br"  # paste your login generated by Mailtrap
        password = "!@Serval1970@!"  # paste your password generated by Mailtrap
        sender_email = "Sistema SONAR<develop@gruposerval.com.br>"
        message = MIMEMultipart("alternative")
        message["Subject"] = subject
        message["From"] = sender_email
        message["To"] = ", ".join(receiver_email)
        texto = "Olá Mundo!!!"
        # write the text/plain part
        text = """\
        Ola,
        Vamos detalhar o motivo aqui: text"""
        # write the HTML part
        html = """\
        <html>
            <body>
            <p>Olá, <i><b>""" + sender_name.__str__() + """</b></i><br><br/>
            <b>Processo Encerrado: """ + tabela.__str__() + """ </b><br/><br/>
            """ + msg[0].__str__() + """ </p><br/>

            Não responda este e-mail, gerado automaticamente! <br/><br/>
            Att,<br/>
            Sistema TELA - Copyright © 2023<br/> 
            Apoio Sistema <b>SONAR</b> - Copyright © 2023
          </body>
        </html>
        """
        # convert both parts to MIMEText objects and add them to the MIMEMultipart message
        # part1 = MIMEText(text, "plain")
        part2 = MIMEText(html, "html","utf-8")
        # message.attach(part1)
        message.attach(part2)
        # send your email
        with smtplib.SMTP(smtp_server, port) as server:
            server.login(login, password)
            server.sendmail(
                sender_email, receiver_email, message.as_string()
            )

    def mail_comercial(self, subject, sender_name, receiver_email, msg, tabela):
        import smtplib
        from email.mime.text import MIMEText
        from email.mime.multipart import MIMEMultipart
        port = 587
        smtp_server = "smtp.gruposerval.com.br"
        login = "develop@gruposerval.com.br"  # paste your login generated by Mailtrap
        password = "!@Serval1970@!"  # paste your password generated by Mailtrap
        sender_email = "Sistema SONAR<develop@gruposerval.com.br>"
        message = MIMEMultipart("alternative")
        message["Subject"] = subject
        message["From"] = sender_email
        message["To"] = ", ".join(receiver_email)
        texto = "Olá Mundo!!!"
        # write the text/plain part
        text = """\
        Ola,
        Vamos detalhar o motivo aqui: text"""
        # write the HTML part
        html = """\
        <html>
            <body>
            <p>Saudações, <i><b>""" + sender_name.__str__() + """</b></i><br><br/>
            <b>Processo Encerrado: """ + tabela.__str__() + """ </b><br/><br/>
            """ + msg.__str__() + """ </p><br/>

            Não responda este e-mail, gerado automaticamente! <br/><br/>
            Att,<br/>
            Sistema TELA - Copyright © 2023<br/> 
            Apoio Sistema <b>SONAR</b> - Copyright © 2023
          </body>
        </html>
        """
        # convert both parts to MIMEText objects and add them to the MIMEMultipart message
        # part1 = MIMEText(text, "plain")
        part2 = MIMEText(html, "html","utf-8")
        # message.attach(part1)
        message.attach(part2)
        # send your email
        with smtplib.SMTP(smtp_server, port) as server:
            server.login(login, password)
            server.sendmail(
                sender_email, receiver_email, message.as_string()
            )
    def progress_bar(progress, total):
        percent = 100 * (progress / float(total))
        bar = '█' * int(percent) + '-' * (100 - int(percent))
        print(f"\r|{bar}| {percent:.2f}%", end="\r")

    def concat(valor, separador):
        lista = iter(valor)
        string = str(next(lista))
        for i in lista:
            string += str(separador) + str(i)
        return string

