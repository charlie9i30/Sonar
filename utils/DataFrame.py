import pandas as pd
import configparser
import pyodbc
from utils import Funcoes
from sqlalchemy import text
import connectorx as cx


class DB(object):

    def __init__(self):
        pass

    Flag = Funcoes.Flag()

    # Regra de Negócio

    def select_process_dw(self):
        sql = "SELECT id,processo,minutos_exec,status,ativo,DATEADD(MINUTE,minutos_exec,updated_at) 'next_time',GETDATE()'now' \
               FROM [DW].dbo.processos_dw with (nolock) \
               WHERE ativo = 1 AND status=1 AND OPERADOR=2 \
               AND DATEADD(MINUTE,minutos_exec,updated_at)<=GETDATE() AND processo not like '%http%' AND processo not like '%dw%' ORDER BY id desc"
        dados = self.execsql_select_connectorx(sql)
        return dados
    def select_process_dw1(self):
        sql = "SELECT id,processo,minutos_exec,status,ativo,DATEADD(MINUTE,minutos_exec,updated_at) 'next_time',GETDATE()'now' \
               FROM [DW].dbo.processos_dw with (nolock) \
               WHERE ativo = 1 AND status=1 AND OPERADOR=2 \
               AND DATEADD(MINUTE,minutos_exec,updated_at)<=GETDATE() AND processo like '%dw%' ORDER BY id desc"
        dados = self.execsql_select_connectorx(sql)
        return dados

    def update_process_dw_start(self):
        sql = "UPDATE [DW].dbo.processos_dw SET STATUS=1,ATIVO = 1,TOTAL_META=0,TOTAL_PENDENTE=0 WHERE OPERADOR=2;"
        dados = self.execsq_insert_update(sql)
        return dados

    def update_process_dw(self, codigo):
        sql = "UPDATE [DW].dbo.processos_dw SET updated_at=GETDATE(),STATUS=0 WHERE ID = " + codigo.__str__() + ";"
        dados = self.execsq_insert_update(sql)
        return dados

    def select_formsregistrosrespostas40(self):
        sql = "select id, qrcode,retorno_pasta,localidade,periodo,supervisor,criado_at,atualizado_at,indice from [TelaWeb].dbo.fc_forms_registros_respostas_40();"
        dados = self.execsql_select(sql)
        return dados

    def select_id_formsregistrosrespostas40(self):

        # Usuário (ppessoa_id) Padrão
        sql = "select top 1 id \
        from [TelaWeb].[dbo].[forms_registros] (nolock) \
        where pessoa_id=24084 and form_id=40 order by id desc"
        dados = self.execsql_select(sql)
        return dados


    def select_supervisor_localidade_formsregistrosrespostas40(self, ano, cliente_id, localidade_id):
        # De acordo com a impressoes_log
        sql = "Select top 1 convert(VARCHAR,codcoligada)+'.'+ \
        Right(replicate('0000',4) + convert(VARCHAR,supervisor_id),4) AS supervisor_id, \
        supervisor_nome  +' ('+ CONVERT(VARCHAR,supervisor_id) + ')' AS supervisor, \
        convert(VARCHAR,codcoligada)+'.'+ Right(replicate('0000',4) + convert(VARCHAR,supervisor_id),4)+'.'+\
        Right(replicate('0000',6) + convert(VARCHAR,localidade_id),6) as localidade_id, \
        localidade +' ('+ CONVERT(VARCHAR,localidade_id) + ')' AS localidade \
        from vwalocacoes_all \
        where YEAR(data_inicio) <= " + ano.__str__() + " \
        and cliente_id=" + cliente_id.__str__() + " \
        and localidade_id=" + localidade_id.__str__() + " \
        order by updated_at desc"
        dados = self.execsql_select(sql)
        return dados

    def insert_registro_formsregistrosrespostas40(self):
        sql = "insert into forms_registros(created_at,	updated_at,	updated_por,	status_id,	pessoa_id,	\
            pessoa_origem,	form_id,	obs,	created_por,	atendimento_id) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA','3','24084','0','40',NULL,\
            'COLABORADOR TESTE - SERVAL LIMPEZA','0');"
        dados = self.execsq_insert_update(sql)
        return dados

    def insert_registro_resp_318_formsregistrosrespostas40(self, codigo, resposta_id, resposta_texto_318):
        sql = "insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id,\
            resposta_id,resposta_texto,resposta_codigo,created_por) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 40, 318, \
            '" + resposta_id.__str__() + "','" + resposta_texto_318 + "',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados

    def insert_registro_resp_319_formsregistrosrespostas40(self, codigo, resposta_id, resposta_texto_319):
        sql = "insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
            resposta_id,resposta_texto,resposta_codigo,created_por) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 40, 319, \
                '" + resposta_id + "','" + resposta_texto_319 + "',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados

    def insert_registro_resp_317_320_321_341345_formsregistrosrespostas40(self, codigo, resposta_id, resposta_texto_320):
        sql = "insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
            resposta_id,resposta_texto,resposta_codigo,created_por) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 40, 317, \
            NULL,NULL,NULL,'COLABORADOR TESTE - SERVAL LIMPEZA'); \
            insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
            resposta_id,resposta_texto,resposta_codigo,created_por) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 40, 320,\
            '" + resposta_id.__str__() + "','" + resposta_texto_320.__str__() + "',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA'); \
            insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 40, 321, \
            NULL, NULL, NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA'); \
            insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 40, 341, \
            NULL, NULL, NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA'); \
            insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 40, 342, \
            NULL, NULL, NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA'); \
            insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 40, 343, \
            NULL, NULL, NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA'); \
            insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 40, 344, \
            NULL, NULL, NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA'); \
            insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 40, 345, \
            NULL, NULL, NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados

    def insert_registro_resp_346_formsregistrosrespostas40(self, codigo):
        sql = "insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 40, 346, \
            14956, 'OK', NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados

    def select_formscoligadaregistroresposta(self, coligada):
        sql = "select convert(varchar,codcoligada)+' - '+ nomefantasia as nome " \
              "from corporermv12.dbo.gcoligada (nolock) " \
              "where codcoligada=" + coligada.__str__()
        dados = self.execsql_select_connectorx(sql)
        return dados


    def select_formsregistrosrespostas45_sobra(self):
        sql = "SELECT registro_id,count(*)qtd FROM forms_registros_respostas (nolock) \
               WHERE forms_registros_respostas.registro_id IN( \
               SELECT registro_id \
               from forms_registros_respostas(nolock) \
               where form_id = 45 \
               group by registro_id \
               HAVING count(*)<17) \
               AND forms_registros_respostas.updated_por = 'COLABORADOR TESTE - SERVAL LIMPEZA'	\
               GROUP BY registro_id "
        dados = self.execsql_select_connectorx(sql)
        return dados

    def select_formsregistrosrespostas113_sobra(self):
        sql = "SELECT registro_id,count(*)qtd \
            from forms_registros_respostas(nolock) \
            where form_id = 113 \
            and updated_por='COLABORADOR TESTE - SERVAL LIMPEZA' \
            group by registro_id \
            HAVING count(*)<7 "
        dados = self.execsql_select_connectorx(sql)
        return dados


    def select_formsregistrosrespostas45(self):
        sql = "select id,qrcode,periodo,indice,retornopasta,data1,data2,indiceII  \
                from forms_registros_respostas_indice_45 (nolock) \
                order by id"
        #order by data2 desc
        # where data2='2023-01-10'
        dados = self.execsql_select_connectorx(sql)
        return dados


    def select_id_formsregistrosrespostas45(self,codigo):
        # Usuário (ppessoa_id) Padrão
        sql = "select top 1 id \
        from [TelaWeb].[dbo].[forms_registros] (nolock) \
        where pessoa_id=" + codigo.__str__() + " and form_id=45 order by id desc"
        dados = self.execsql_select_connectorx(sql)
        return dados

    def select_id_formsregistrosrespostas45_id(self, registro):
        # Usuário (ppessoa_id) Padrão
        sql = "INSERT INTO [TelaWeb].dbo.forms_registros_respostas_indice_45 \
         select * from [TelaWeb].dbo.fc_forms_registros_respostas_45_id(" + registro.__str__() + ")"
        dados = self.execsq_insert_update(sql)
        return dados
    def select_impressao(self, periodo, matricula, coligada):
        sql = "select * from impressoes_log (nolock) \
               where extras = '" + periodo.__str__() + "'\
               and matricula = '" + matricula.__str__() + "' \
               and coligada_id = " + coligada.__str__()
        dados = self.execsql_select_connectorx(sql)
        return dados

    def select_funcionario_respostaid(self, periodo, matricula, coligada,posto_id):
        '''
        sql = "select convert(varchar,vwalocacoes.codcoligada) +'.'+ \
               convert(varchar,replicate('0', 7 - len(vwalocacoes.matricula)) +  rtrim(vwalocacoes.matricula)) +'.'+ \
               convert(varchar,replicate('0', 3 - len(vwalocacoes.supervisor_id)) + \
               rtrim(vwalocacoes.supervisor_id)) +'.'+  convert(varchar,replicate('0', 7 - len(vwalocacoes.posto_id)) + \
               rtrim(vwalocacoes.posto_id))  +'.'+  convert(varchar,replicate('0', 11 - len(vwalocacoes.id)) + \
               rtrim(vwalocacoes.id))   as id,  vwalocacoes.colaborador_nome +' ('+vwalocacoes.matricula+')' as nome, \
               max(impressoes_log.id)  as max_id \
               from vwalocacoes_ultima_ativa_inativa vwalocacoes \
               inner join impressoes_log (nolock) on impressoes_log.coligada_id=vwalocacoes.codcoligada and impressoes_log.matricula=vwalocacoes.matricula \
               where vwalocacoes.matricula not like 'd%' and vwalocacoes.matricula not like 'p%' and impressoes_log.extras = '" + periodo.__str__() + "'\
               and vwalocacoes.matricula = '" + matricula.__str__() + "' and vwalocacoes.codcoligada = " + coligada.__str__() + " \
               group by convert(varchar,vwalocacoes.codcoligada) +'.'+ \
               convert(varchar,replicate('0', 7 - len(vwalocacoes.matricula)) + \
               rtrim(vwalocacoes.matricula)) +'.'+   convert(varchar,replicate('0', 3 - len(vwalocacoes.supervisor_id)) + \
               rtrim(vwalocacoes.supervisor_id)) +'.'+  convert(varchar,replicate('0', 7 - len(vwalocacoes.posto_id)) + \
               rtrim(vwalocacoes.posto_id)) +'.' + convert(varchar,replicate('0', 11 - len(vwalocacoes.id)) + rtrim(vwalocacoes.id)), \
              vwalocacoes.colaborador_nome +' ('+vwalocacoes.matricula+')' order by nome"
        '''
        sql = "SELECT \
                       convert(varchar,alocacoes.codcoligada) +'.'+ \
                       convert(varchar,replicate('0', 7 - len(alocacoes.matricula)) +  rtrim(alocacoes.matricula)) +'.'+ \
                       convert(varchar,replicate('0', 3 - len(postos.supervisor_id)) + \
                       rtrim(postos.supervisor_id)) +'.'+  convert(varchar,replicate('0', 7 - len(postos.id)) + \
                       rtrim(postos.id))  +'.'+  convert(varchar,replicate('0', 4 - len(codfuncao)) + \
                       rtrim(vwrmpfunc_all.codfuncao))   as id,  vwrmpfunc_all.nome +' ('+alocacoes.matricula+')' as nome \
                       FROM alocacoes WITH (nolock) \
                       INNER JOIN \
                        (SELECT codcoligada, matricula, MAX(id) AS id \
                        FROM alocacoes WITH (nolock) \
                        WHERE (matricula NOT LIKE 'D%') AND (matricula NOT LIKE 'P%') AND posto_id = " + posto_id.__str__() + " \
                        GROUP BY codcoligada, matricula)aloca_max ON aloca_max.codcoligada = alocacoes.codcoligada \
                        AND aloca_max.matricula = alocacoes.matricula AND aloca_max.id = alocacoes.id \
                       INNER JOIN postos WITH (nolock) ON postos.id = alocacoes.posto_id AND postos.codcoligada = alocacoes.codcoligada \
                       INNER JOIN vwrmpfunc_all ON vwrmpfunc_all.codcoligada = alocacoes.codcoligada AND vwrmpfunc_all.chapa = alocacoes.matricula \
                       WHERE alocacoes.matricula = '" + matricula.__str__() + "' AND alocacoes.codcoligada = " + coligada.__str__() + " \
                        GROUP BY convert(varchar,alocacoes.codcoligada) +'.'+ \
                                       convert(varchar,replicate('0', 7 - len(alocacoes.matricula)) +  rtrim(alocacoes.matricula)) +'.'+ \
                                       convert(varchar,replicate('0', 3 - len(postos.supervisor_id)) + \
                                       rtrim(postos.supervisor_id)) +'.'+  convert(varchar,replicate('0', 7 - len(postos.id)) + \
                                       rtrim(postos.id))  +'.'+  convert(varchar,replicate('0', 4 - len(codfuncao)) + \
                                       rtrim(vwrmpfunc_all.codfuncao)),  vwrmpfunc_all.nome +' ('+alocacoes.matricula+')'"
        dados = self.execsql_select(sql)
        return dados

    def select_postofrequencia_resposta(self, posto_id,coligada_id):
        sql = "SELECT frequencia_id,nome FROM vwpostos WHERE id =" + posto_id.__str__() + " AND coligada_id=" + coligada_id.__str__()
        dados = self.execsql_select_connectorx(sql)
        return dados
    def select_supervisor_resposta(self, supervisor_id):
        sql = " select convert(varchar,supervisor_id) as id, supervisor_nome +' ('+convert(varchar,supervisor_id)+')' as nome \
                from vwpostos   where supervisor_id=" + supervisor_id + " group by convert(varchar,supervisor_id), \
                supervisor_nome + ' (' + convert(varchar, supervisor_id) + ')'"
        dados = self.execsql_select(sql)
        return dados

    def select_funcao_resposta(self, funcao_id):
        '''sql = "select codfuncao as id, codfuncao_nome collate latin1_general_ci_ai +' ('+codfuncao+')' as nome \
               from vwalocacoes_all  where id = convert(int," + funcao_id.__str__() + ") \
               group by codfuncao, codfuncao_nome collate latin1_general_ci_ai + ' (' + codfuncao + ')'"
        '''
        sql = "SELECT codigo AS id,nome +' ('+codigo+')' AS nome  FROM vwrmpfuncao  \
               WHERE codigo = " + funcao_id.__str__() + " \
              GROUP BY codigo,nome +' ('+codigo+')'"
        dados = self.execsql_select_connectorx(sql)
        return dados

    def select_lotacao_resposta(self, posto_id, coligada):
        sql = "select convert(varchar,id) as id, nome +' ('+convert(varchar,id)+')' as nome  \
                from vwpostos where coligada_id = " + coligada.__str__() + "AND id = " + posto_id.__str__() + " \
                group by convert(varchar,id), nome +' ('+convert(varchar,id)+')'"
        dados = self.execsql_select(sql)
        return dados

    def select_supervisor_localidade_formsregistrosrespostas45(self, ano, mes, cliente_id, localidade_id):
        # De acordo com a impressoes_log
        sql = "Select top 1 convert(VARCHAR,codcoligada)+'.'+ \
           Right(replicate('0000',4) + convert(VARCHAR,supervisor_id),4) AS supervisor_id, \
           supervisor_nome  +' ('+ CONVERT(VARCHAR,supervisor_id) + ')' AS supervisor, \
           convert(VARCHAR,codcoligada)+'.'+ Right(replicate('0000',4) + convert(VARCHAR,supervisor_id),4)+'.'+\
           Right(replicate('0000',6) + convert(VARCHAR,localidade_id),6) as localidade_id, \
           localidade +' ('+ CONVERT(VARCHAR,localidade_id) + ')' AS localidade \
           from vwalocacoes_all \
           where YEAR(data_inicio) <= " + ano.__str__() + " \
           and MONTH(data_inicio) <= " + mes.__str__() + " \
           and cliente_id=" + cliente_id.__str__() + " \
           and localidade_id=" + localidade_id.__str__() + " \
           order by updated_at desc"
        dados = self.execsql_select(sql)
        return dados

    def insert_registro_forms113(self):
        sql = "insert into forms_registros(created_at,	updated_at,	updated_por,	status_id,	pessoa_id,	\
            pessoa_origem,	form_id,	obs,	created_por,	atendimento_id) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA','3','24084','0','113',NULL,\
            'COLABORADOR TESTE - SERVAL LIMPEZA','0');"
        dados = self.execsq_insert_update(sql)
        return dados

    def insert_registro_formsopcoes113(self,email):
        sql = "INSERT INTO \
                forms_opcoes \
                (created_at,updated_at,created_por,updated_por,form_id,campo_id,item_id,item_nome,ativo,item_id_relac) \
                VALUES \
                (GETDATE(),GETDATE(),'rogerio.barbosa','ROGÉRIO FARIAS BARBOSA',113,1970,(SELECT max(item_id)+1 AS item_id FROM forms_opcoes (nolock) WHERE form_id = 113 AND campo_id = 1970),'" + email.__str__() +"',1,NULL) "
        dados = self.execsq_insert_update(sql)
        return dados
    def insert_registrosrespostas_form113(self,codigo,nome,email,login_rede,caixa_atual,cota,item_id_opcoes):
        sql = "insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
            resposta_id,resposta_texto,resposta_codigo,created_por) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 113, 1974, \
            17,'FORTALEZA - TIC',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA'); \
            insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
            resposta_id,resposta_texto,resposta_codigo,created_por) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 113, 1976, \
            NULL,'"+ nome.__str__() +"',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA'); \
            insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
            resposta_id,resposta_texto,resposta_codigo,created_por) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 113, 1977, \
            NULL,'" + login_rede.__str__() + "',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA'); \
            insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
            resposta_id,resposta_texto,resposta_codigo,created_por) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 113, 1970, \
            '" + item_id_opcoes.__str__() + "','" + email.__str__() + "',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA'); \
            insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
            resposta_id,resposta_texto,resposta_codigo,created_por) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 113, 1973, \
            1,'IMAP',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA');  \
            insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
            resposta_id,resposta_texto,resposta_codigo,created_por) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 113, 1975, \
            1,'Ativo',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA');  \
            insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
            resposta_id,resposta_texto,resposta_codigo,created_por) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 113, 2061, \
            73204,'Não',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA'); \
            insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
            resposta_id,resposta_texto,resposta_codigo,created_por) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 113, 2063, \
            73201,'Ativo',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA'); \
            insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
            resposta_id,resposta_texto,resposta_codigo,created_por) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 113, 2064, \
            NULL,'" + caixa_atual.__str__() + "',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA'); \
            insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
            resposta_id,resposta_texto,resposta_codigo,created_por) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 113, 2062, \
            NULL,'" + cota.__str__() + "',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA');"

        dados = self.execsq_insert_update(sql)
        return dados

    def update_registros_respostas_form113_status_desativar(self, registro_id):
        sql = "	UPDATE forms_registros_respostas SET resposta_id = 73202,resposta_texto = 'Inativo' WHERE registro_id =" + registro_id.__str__() +" AND campo_id = 2063 \
                UPDATE forms_registros_respostas SET resposta_id = 73204,resposta_texto = 'Não' WHERE registro_id =" + registro_id.__str__() +" AND campo_id = 2061 "
        dados = self.execsq_insert_update(sql)
        return dados
    def update_registros_respostas_form113_status_ativar(self, registro_id):
        sql = "	UPDATE forms_registros_respostas SET resposta_id = 73201,resposta_texto = 'Ativo' WHERE registro_id =" + registro_id.__str__() +" AND campo_id = 2063 \
                UPDATE forms_registros_respostas SET resposta_id = 73203,resposta_texto = 'Sim' WHERE registro_id =" + registro_id.__str__() +" AND campo_id = 2061 "
        dados = self.execsq_insert_update(sql)
        return dados
    def update_registros_respostas_form113_nome_cota(self, registro_id,cota,caixa_atual,nome_exibicao):
        sql = "	UPDATE forms_registros_respostas SET resposta_texto = '" + cota.__str__() + "' WHERE registro_id =" + registro_id.__str__() + " AND campo_id = 2062 \
                UPDATE forms_registros_respostas SET resposta_texto = '" + caixa_atual.__str__() + "' WHERE registro_id =" + registro_id.__str__() + " AND campo_id = 2064  \
                UPDATE forms_registros_respostas SET resposta_texto = '" + nome_exibicao.__str__() + "' WHERE registro_id =" + registro_id.__str__() + " AND campo_id = 1976"
        dados = self.execsq_insert_update(sql)
        return dados
    def select_id_formsopcoes113(self,email):
        sql = "	SELECT item_id as id,item_nome as nome FROM forms_opcoes (nolock) WHERE form_id=113  AND campo_id=1970 \
				AND item_nome = '" + email.__str__() + "'"
        dados = self.execsql_select_connectorx(sql)
        return dados

    def select_registros_respostas_form113(self):
        sql = "	select * from fc_forms_registros_respostas_113() order by id "
        dados = self.execsql_select(sql)
        return dados

    def select_registros_respostas_by_id(self,id):
        sql = "	SELECT * FROM forms_registros_respostas (nolock) where form_id = 113 and registro_id = " + id.__str__()
        dados = self.execsql_select_connectorx(sql)
        return dados

    def select_id_formsregistros113(self):

        # Usuário (ppessoa_id) Padrão
        sql = "select top 1 id \
          from [TelaWeb].[dbo].[forms_registros] (nolock) \
          where pessoa_id=24084 and form_id=113 order by id desc"
        dados = self.execsql_select(sql)
        return dados

    def select_formsopcoes_113(self):
        sql = "SELECT item_id, item_nome FROM [TelaWeb].[dbo].[forms_opcoes] WHERE form_id=113 AND campo_id=1970;"
        dados = self.execsql_select_connectorx(sql)
        return dados

    def select_lista_carimbos(self):
        sql = "SELECT \
                COUNT(*) AS qtd, \
                vo.status, \
                vo.status_nome, \
                vo.nome, \
                vop.pessoa_id \
                FROM vwcarimbos_operacoes vo \
                INNER JOIN vwcarimbos_operacoes_membros vop ON vop.operacao_id = vo.id \
                WHERE vo.status = 0 \
                GROUP BY vo.status, \
                vo.status_nome, \
                vo.nome, \
                vop.pessoa_id"
        dados = self.execsql_select_connectorx(sql)
        return dados
    def select_lista_atendimentos(self):
        sql="SELECT COUNT (ID) AS qtd,tipo_id,tipo_nome,departamento_id,departamento_nome \
                FROM vwatendimentos_registros WHERE created_at >= DATEADD(day,-20,GETDATE()) \
		        and status_id in (0,1,2) \
		        GROUP BY tipo_id,tipo_nome,departamento_id,departamento_nome"
        dados = self.execsql_select_connectorx(sql)
        return dados

    def select_lista_contratos(self):
        sql = "SELECT * FROM vwcontrato WHERE DATEDIFF(DAY,GETDATE(),ISNULL(vigencia_final,vigencia_fim)) \
               BETWEEN 0 AND 60 AND ATIVO = 1"
        dados = self.execsql_select_connectorx(sql)
        return dados

    def delete_limpar_notificacoes(self):
        sql = "DELETE FROM notificacoes WHERE ID IN (SELECT nt.id FROM notificacoes (nolock) nt \
               LEFT JOIN notificacoes_log (nolock) nl ON nl.notificacao_id = nt.id \
               WHERE nl.usuario_id IS NULL AND nt.created_at >= DATEADD(DAY,-60,GETDATE()-1))"
        dados = self.execsq_insert_update(sql)
        return dados

    def select_lista_notificados(self,unidade,time):
        sql = "SELECT nl.usuario_id FROM notificacoes (nolock) nt \
                  LEFT JOIN notificacoes_log (nolock) nl ON nl.notificacao_id = nt.id \
               WHERE nl.created_at >= DATEADD("+ unidade +","+ time +",GETDATE()) \
               UNION \
               SELECT para FROM notificacoes (nolock) nt \
                  LEFT JOIN notificacoes_log (nolock) nl ON nl.notificacao_id = nt.id \
               WHERE nl.usuario_id IS null"
        dados = self.execsql_select(sql)
        return dados

    def select_usuarios_departamento(self,departamento_id):
        sql="SELECT vw.id,vw.pessoa_id,vw.loginrede,vw.perfil_id,vw.perfil_nome,at.id as atendimento_id,at.nome as atendimento,ad.id as departamento_id, \
                ad.nome as departamento FROM vwusuarios vw \
                left join usuarios_atendimentos_filas (nolock) u on u.usuario_id = vw.id \
                left join atendimentos_tipos (nolock) at on at.id = u.tipo_id \
                left join usuariosdepartamentos (nolock) ud on ud.usuario_id = vw.id \
                left join atendimentos_departamentos (nolock) ad on ad.id = ud.departamento_id \
                where vw.notificacoes = 1 and at.id is null and ad.id IN ("+ departamento_id +")"
        dados = self.execsql_select_connectorx(sql)
        return dados
    def select_usuarios_carimbos(self,pessoa_id):
        sql="SELECT vw.id,vw.pessoa_id,vw.loginrede,vw.perfil_id,vw.perfil_nome,at.id as atendimento_id,at.nome as atendimento,ad.id as departamento_id, \
                ad.nome as departamento FROM vwusuarios vw \
                left join usuarios_atendimentos_filas (nolock) u on u.usuario_id = vw.id \
                left join atendimentos_tipos (nolock) at on at.id = u.tipo_id \
                left join usuariosdepartamentos (nolock) ud on ud.usuario_id = vw.id \
                left join atendimentos_departamentos (nolock) ad on ad.id = ud.departamento_id \
                where vw.notificacoes = 1 and at.id is null and vw.pessoa_id = "+ pessoa_id.__str__()
        dados = self.execsql_select_connectorx(sql)
        return dados
    def select_usuarios_comercial(self):
        sql = "SELECT vw.id,vw.pessoa_id,vw.loginrede,vw.perfil_id,vw.perfil_nome,at.id as atendimento_id,at.nome as atendimento,ad.id as departamento_id, \
                ad.nome as departamento FROM vwusuarios vw \
                left join usuarios_atendimentos_filas (nolock) u on u.usuario_id = vw.id \
                left join atendimentos_tipos (nolock) at on at.id = u.tipo_id \
                left join usuariosdepartamentos (nolock) ud on ud.usuario_id = vw.id \
                left join atendimentos_departamentos (nolock) ad on ad.id = ud.departamento_id \
                where vw.notificacoes = 1 and ad.id IN (5)"
        dados = self.execsql_select_connectorx(sql)
        return dados

    def select_usuarios_atendimentos(self,tipo_id):
        sql = "SELECT vw.id,vw.pessoa_id,vw.loginrede,vw.perfil_id,vw.perfil_nome,at.id as atendimento_id,at.nome as atendimento,ad.id as departamento_id, \
                ad.nome as departamento FROM vwusuarios vw \
                left join usuarios_atendimentos_filas (nolock) u on u.usuario_id = vw.id \
                left join atendimentos_tipos (nolock) at on at.id = u.tipo_id \
                left join usuariosdepartamentos (nolock) ud on ud.usuario_id = vw.id \
                left join atendimentos_departamentos (nolock) ad on ad.id = ud.departamento_id \
                where vw.notificacoes = 1 and at.id is not null and at.id IN("+ tipo_id +")"
        dados = self.execsql_select_connectorx(sql)
        return dados
    def select_maxid_notificao(self):
        sql="SELECT MAX(ID) as id FROM notificacoes (nolock)"
        dados = self.execsql_select(sql)
        return dados

    def select_maxid_notificaolog(self):
        sql = "SELECT MAX(ID) as id FROM notificacoes_log (nolock)"
        dados = self.execsql_select(sql)
        return dados
    def select_processos_FolhaDePonto(self):
        sql="SELECT id,ativo,processo FROM usuariosprocessos (nolock) WHERE processo LIKE '%http%' and ativo = 1 ORDER BY id"
        dados = self.execsql_select_connectorx(sql)
        return dados
    def select_procedures_usuariosprocessos(self):
        sql="SELECT id,ativo,processo FROM usuariosprocessos (nolock) WHERE (processo LIKE '%EXEC%') and (processo NOT LIKE '%arquivo_folha%')  and ativo = 1  \
             ORDER BY case when processo LIKE '%arquivofolha%' THEN 0 \
                           when processo LIKE '%planejamento%' THEN 1 \
                           when processo LIKE '%atestado%' THEN 2 \
                        ELSE 3 END, id"
        dados = self.execsql_select_connectorx(sql)
        return dados
    def select_qrcode_cracha(self):
        sql="SELECT codcoligada,\
        empresa, \
        CONVERT(int,(chapa)) AS chapa, \
        nome, \
        data_admissao, \
        funcao, \
        codsecao, \
        secao, \
        posto, \
        escala, \
        carteiratrab, \
        pispasep, \
        tipo_sanguineo, \
        registro_pf, \
        cnv_validade, \
        data_reciclagem, \
        identidade, \
        cpf, \
        qrcode, \
        qrcode_download \
        FROM vw_lista_cracha"
        dados = self.execsql_select_connectorx(sql)
        return dados

    def select_foto_cracha(self):
        sql="SELECT codcoligada,\
        empresa, \
        CONVERT(int,(chapa)) AS chapa, \
        nome, \
        data_admissao, \
        funcao, \
        codsecao, \
        secao, \
        posto, \
        escala, \
        carteiratrab, \
        pispasep, \
        tipo_sanguineo, \
        registro_pf, \
        cnv_validade, \
        data_reciclagem, \
        identidade, \
        cpf, \
        imagem, \
        imagem_download \
        FROM vw_lista_cracha  \
        where imagem is not null "
        dados = self.execsql_select_connectorx(sql)
        return dados
    def select_lista_sessoes(self):
        sql="SELECT * FROM ( \
                SELECT \
                SUBSTRING (REPLACE (REPLACE (SUBSTRING (ST.text, (req.statement_start_offset/2) + 1, \
                ((CASE statement_end_offset WHEN -1 THEN DATALENGTH(ST.text) \
                ELSE req.statement_end_offset END - req.statement_start_offset)/2) + 1) , CHAR(10), ' '), CHAR(13), ' '), 1, 1024)  AS statement_text, \
                db.name 'database', req.database_id, req.session_id, se.login_name, \
                req.total_elapsed_time AS duration_ms, req.cpu_time AS cpu_time_ms, \
                ((req.total_elapsed_time - req.cpu_time) / 1000) wait_time_sec, \
                (((req.total_elapsed_time - req.cpu_time) / (1000 * 60)) % 60) AS wait_time_min, req.logical_reads \
                FROM sys.dm_exec_requests AS req \
                CROSS APPLY sys.dm_exec_sql_text(req.sql_handle) AS ST \
                LEFT JOIN sys.databases db on db.database_id=req.database_id \
                LEFT JOIN sys.dm_exec_sessions se on se.session_id=req.session_id \
                where req.database_id = 5 and ST.text not like '%usuarios_localizacao%' AND ST.text not like '%dadosfolhatxt_buffer%' AND ST.text NOT LIKE '%usuariosprocessos%')T \
                WHERE T.wait_time_min >=" + self.Flag.requests_time_conect.__str__()
        dados = self.execsql_select_sys(sql)
        return dados
    def select_processo_folha(self):
        sql = "SELECT T1.id,T1.usuario_id,total,  \
                CASE WHEN T1.total > 100 THEN FLOOR((T1.total-T1.atual)*0.050) \
                ELSE T1.total \
                END bloco,   \
                T1.processo,T1.matricula,T1.coligada_id,T1.atual,T2.nome \
                FROM usuariosprocessos(NOLOCK) T1  \
                inner join usuarios(NOLOCK) T2 on T2.id = T1.usuario_id  \
                WHERE (T1.ativo = 1  OR T1.atual = 0)  \
                AND (T1.processo like '%vt_va%' \
                OR T1.processo like '%arquivo_folha%') \
                AND T1.processo NOT like '%impressoes%' \
                AND T1.processo NOT like '%arquivo_folha_gerenciais%' \
                ORDER BY case when processo LIKE '%arquivo_folha_variaveis%' \
                THEN 0 ELSE 1 END,id"
        dados = self.execsql_select_connectorx(sql)
        return dados

    def select_buffer_vt_va(self,usuario_id,coligada_id):
        sql= "SELECT usuario_id, MIN(ROW_NUM)atual,max(ROW_NUM)total,FLOOR((max(ROW_NUM)-MIN(ROW_NUM))*0.075) bloco, coligada_id \
                FROM arquivovtvatxt WITH(nolock)  \
                WHERE usuario_id =" + usuario_id + "AND coligada_id =" + coligada_id + "  \
                GROUP BY usuario_id, coligada_id"
        dados = self.execsql_select_connectorx(sql)
        return dados

    def select_buffer_folha(self,usuario_id,coligada_id):
        sql= "SELECT usuario_id, MIN(ROW_NUM)atual,max(ROW_NUM)total,FLOOR((max(ROW_NUM)-MIN(ROW_NUM))*0.050) bloco, coligada_id \
                FROM dadosfolhatxt_buffer_coligada"+ coligada_id +" WITH(nolock)  \
                WHERE usuario_id =" + usuario_id + "AND coligada_id =" + coligada_id + "  \
                GROUP BY usuario_id, coligada_id"
        dados = self.execsql_select_connectorx(sql)
        return dados


    def kill_session_id(self,id):
        sql = "KILL " + id.__str__()+";"
        self.Flag.conexaoV(sql)

    def select_processoshttp_dw(self):
        sql="SELECT id,created_at,updated_at,updated_por,processo,minutos_exec,data_exec,status,ativo,total_meta,total_pendente,created_por,operador,descricao \
             FROM [DW].dbo.processos_dw (nolock)  \
             WHERE processo LIKE '%http%' AND operador = 2 AND ativo = 1 AND DATEADD(MINUTE,minutos_exec,updated_at)<=GETDATE() "
        dados = self.execsql_select_connectorx(sql)
        return dados

    def select_fila_usuarios_processos(self):
        sql = "SELECT id,created_at,updated_at,updated_por,usuario_id,processo,total,atual,ativo,matricula,coligada_id,created_por  \
               FROM usuariosprocessos (nolock)  \
               WHERE processo LIKE '%arquivo%' and ativo = 0"
        dados = self.execsql_select_connectorx(sql)
        return dados

    def update_processos_folha(self,id):
        sql = "UPDATE usuariosprocessos SET ativo=0  \
                 WHERE  id=" + id.__str__()
        dados = self.execsq_insert_update(sql)
        return dados
    def update_processos_restartfolha(self):
        sql = "UPDATE usuariosprocessos SET ativo=1  \
                 WHERE processo LIKE '%folha%' \
                AND processo NOT like '%impressoes%' \
                AND ativo = 0"
        dados = self.execsq_insert_update(sql)
        return dados

    def update_processoshttp_dw(self,id):
        sql="UPDATE [DW].dbo.processos_dw SET updated_at = GETDATE(),data_exec = GETDATE()  WHERE id =" + id + ";"
        dados = self.execsq_insert_update(sql)
        return dados
    def update_filaProcessos_FolhaDePonto(self):
        sql = "UPDATE usuariosprocessos SET ativo= 1 WHERE processo LIKE '%http%'"
        dados = self.execsq_insert_update(sql)
        return dados
    def update_processos_usuariosprocessos(self, id, status):
        sql = "UPDATE usuariosprocessos SET ativo=" + status + " WHERE id=" + id + ";"
        dados = self.execsq_insert_update(sql)
        return dados
    def update_processosdw_status(self, id, status):
        sql="UPDATE [DW].dbo.processos_dw SET status = " + status.__str__() + " WHERE id =" + id + ";"
        dados = self.execsq_insert_update(sql)
        return dados
    def delete_processos_usuariosprocessos(self, id):
        sql = "DELETE FROM usuariosprocessos WHERE id = " + id + ";"
        dados = self.execsq_insert_update(sql)
        return dados

    def delete_registro_respostas45(self, codigo):
        sql = "delete forms_registros_respostas where form_id=45 and registro_id=" + codigo.__str__() + ";"

        dados = self.execsq_insert_update(sql)
        return dados


    def insert_registro_formsregistrosrespostas45(self,codigo):
        sql = "insert into forms_registros(created_at,	updated_at,	updated_por,	status_id,	pessoa_id,	\
              pessoa_origem,	form_id,	obs,	created_por,	atendimento_id) \
              values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA','3'," + codigo.__str__() +",'0','45',NULL,\
              'COLABORADOR TESTE - SERVAL LIMPEZA','0');"
        dados = self.execsq_insert_update(sql)
        return dados

    def insert_notificacoes(self,perfil_id,titulo,texto,para):
        sql=" INSERT INTO \
              notificacoes (created_at,created_por,updated_at,updated_por,ativo,de,de_perfil_id,para,para_perfil_id,titulo,texto,anexo) \
              VALUES (getdate(),'Adminstrador Interno',getdate(),'Adminstrador Interno',1,1,1,"+ para +","+ perfil_id +",'"+ titulo +"','"+ texto +"',NULL)"
        dados = self.execsq_insert_update(sql)
        return dados

    def insert_notificacoeslog(self,usuario_id,notificacao_id):
        sql = " INSERT INTO \
			    notificacoes_log (created_at,created_por,updated_at,notificacao_id,usuario_id) \
			    VALUES (getdate(),'COLABORADOR TESTE - SERVAL LIMPEZA',getdate(),"+notificacao_id+"," + usuario_id + ")"
        dados = self.execsq_insert_update(sql)
        return dados
    def insert_registro_resp_indice_formsregistrosrespostas45(self, codigo, mes, ano, supervisor, periodo, funcionario,localidade,retornopasta,qrcode,indice,data1,data2):
        sql = "insert into forms_registros_respostas_indice_45(id,criado_at,atualizado_at,mes,ano,supervisor,periodo,funcionario,localidade,retornopasta,qrcode,indice,data1,data2) \
            values( " + codigo.__str__() + "," + mes + "," + ano.__str__() + "," + supervisor.__str__() + "," + periodo.__str__() + "," + funcionario.__str__() + "," + localidade.__str__() + "," + retornopasta.__str__() + "," + qrcode.__str__() + "," + indice.__str__() + "," + data1.__str__() + "," + data2.__str__() + ");"
        dados = self.execsq_insert_update(sql)
        return dados
    def insert_registro_resp_404_formsregistrosrespostas45(self, codigo, resposta_id, resposta_texto_404):
        sql = "insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
            resposta_id,resposta_texto,resposta_codigo,created_por) \
            values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 45, 404, \
               '" + resposta_id.__str__() + "','" + resposta_texto_404 + "',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA');"

        dados = self.execsq_insert_update(sql)
        return dados

    def insert_registro_resp_405_formsregistrosrespostas45(self, codigo, resposta_id, resposta_texto_405):
        sql = "insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
               resposta_id,resposta_texto,resposta_codigo,created_por) \
               values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 45, 405, \
                   '" + resposta_id.__str__() + "','" + resposta_texto_405 + "',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados

    def insert_registro_resp_406_formsregistrosrespostas45(self, codigo, resposta_id, resposta_texto_406):
        sql = "insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
                  resposta_id,resposta_texto,resposta_codigo,created_por) \
                  values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 45, 406, \
                      '" + resposta_id.__str__() + "','" + resposta_texto_406 + "',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados

    def insert_registro_resp_407_formsregistrosrespostas45(self, codigo, resposta_id, resposta_texto_407):
        sql = "insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id, \
                resposta_id,resposta_texto,resposta_codigo,created_por) \
                values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 45, 407, \
                    '" + resposta_id.__str__() + "','" + resposta_texto_407 + "',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados

    def insert_registro_resp_408_formsregistrosrespostas45(self, codigo, resposta_id, resposta_texto_408):
        sql = "insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id,\
               resposta_id,resposta_texto,resposta_codigo,created_por) \
               values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 45, 408, \
               '" + resposta_id.__str__() + "','" + resposta_texto_408 + "',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados

    def insert_registro_resp_409_formsregistrosrespostas45(self, codigo, resposta_id, resposta_texto_409):
        sql = "insert into forms_registros_respostas(created_at,updated_at,updated_por,registro_id,form_id,campo_id,\
               resposta_id,resposta_texto,resposta_codigo,created_por) \
               values(GETDATE(),GETDATE(),'COLABORADOR TESTE - SERVAL LIMPEZA'," + codigo.__str__() + ", 45, 409, \
               '" + resposta_id.__str__() + "','" + resposta_texto_409 + "',NULL,'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados
    def insert_registro_resp_411_formsregistrosrespostas45(self, codigo):
        sql = "insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 45, 411, \
            '', NULL, NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados
    def insert_registro_resp_412_formsregistrosrespostas45(self, codigo):
        sql = "insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 45, 412, \
            '', NULL, NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados
    def insert_registro_resp_413_formsregistrosrespostas45(self, codigo):
        sql = "insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 45, 413, \
            '', NULL, NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados
    def insert_registro_resp_414_formsregistrosrespostas45(self, codigo):
        sql = "insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 45, 414, \
            '', NULL, NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados
    def insert_registro_resp_415_formsregistrosrespostas45(self, codigo):
        sql = "insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 45, 415, \
            '', NULL, NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados
    def insert_registro_resp_416_formsregistrosrespostas45(self, codigo):
        sql = "insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 45, 416, \
            '', NULL, NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados
    def insert_registro_resp_418_formsregistrosrespostas45(self, codigo):
        sql = "insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 45, 418, \
            '', NULL, NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados
    def insert_registro_resp_421_formsregistrosrespostas45(self, codigo):
        sql = "insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 45, 421, \
            14956, 'OK', NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados
    def insert_registro_resp_3054_formsregistrosrespostas45(self, codigo):
        sql = "insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 45, 3054, \
            '', NULL, NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados
    def insert_registro_resp_3089_formsregistrosrespostas45(self, codigo):
        sql = "insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 45, 3089, \
            '', NULL, NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados

    def insert_registro_resp_456_formsregistrosrespostas45(self, codigo):
        sql = "insert into forms_registros_respostas(created_at, updated_at, updated_por, registro_id, form_id, campo_id, \
            resposta_id,resposta_texto, resposta_codigo, created_por) \
            values(GETDATE(), GETDATE(), 'COLABORADOR TESTE - SERVAL LIMPEZA', " + codigo.__str__() + ", 45, 456, \
            '', NULL, NULL, 'COLABORADOR TESTE - SERVAL LIMPEZA');"
        dados = self.execsq_insert_update(sql)
        return dados

    def delete_formsregistrosrespostas45_sobra(self, codigos):
        sql = "delete forms_registros_respostas where form_id=45 and registro_id  in (" + codigos.__str__() + ");"
        dados = self.execsq_insert_update(sql)
        return dados

    def delete_formsregistros45_sobra(self):
        sql = "delete forms_registros where form_id=45 and \
              id not in (select registro_id from forms_registros_respostas where form_id=45);"
        dados = self.execsq_insert_update(sql)
        return dados

    def delete_formsregistrosrespostas113_sobra(self, codigos):
        sql = "delete forms_registros_respostas where form_id=113 and registro_id  in (" + codigos.__str__() + ");"
        dados = self.execsq_insert_update(sql)
        return dados

    def delete_formsregistros113_sobra(self):
        sql = "delete forms_registros where form_id=113 and \
              id not in (select registro_id from forms_registros_respostas where form_id=113);"
        dados = self.execsq_insert_update(sql)
        return dados

    def update_formsregistrosrespostas45(self, cod_atualizar):
        sql = "UPDATE [TelaWeb].[dbo].[forms_registros_respostas] SET resposta_texto='OK' \
            WHERE FORM_ID=45 AND CAMPO_ID =421 AND REGISTRO_ID IN (" + cod_atualizar.__str__() + ");"
        dados = self.execsq_insert_update(sql)
        return dados

    def update_formsregistrosrespostas40(self, cod_atualizar):
        sql = "UPDATE [TelaWeb].[dbo].[forms_registros_respostas] SET resposta_texto='OK' \
            WHERE FORM_ID=40 AND CAMPO_ID =346 AND REGISTRO_ID IN (" + cod_atualizar.__str__() + ");"
        dados = self.execsq_insert_update(sql)
        return dados

    def inserir_dadospdf(self, created_por,updated_por,contrato_id,form_id, arquivo, pagina, dados):
        sql = "insert into [Gestor].[dbo].[dados_pdf](created_at,created_por,updated_at,updated_por,contrato_id,form_id,arquivo,pagina,dados) \
        VALUES (\
        GETDATE(),\
        '" + created_por.__str__() + "',\
        GETDATE(),\
        '" + updated_por.__str__() + "',\
        '" + contrato_id.__str__() + "',\
        '" + form_id.__str__() + "',\
        '" + arquivo.__str__() + "',\
        '" + pagina.__str__() + "',\
        '" + dados.__str__() + "');"
        dados = self.execsq_insert_update(sql)
        return dados

    # Atualizações ---------------------------------------

    # Busca no BD

    def execsql_select_pyodbc(self, sql):
        cfg = configparser.ConfigParser()
        cfg.read('pyt.ini', encoding="utf-8")
        user = self.Flag.deClypt(cfg.get('conec', 'usersys'))
        passw = self.Flag.deClypt(cfg.get('conec', 'passsys'))
        server = (cfg.get('conec', 'serv'))
        database = (cfg.get('conec', 'database'))
        drive = 'ODBC Driver 17 for SQL Server'
        # -----------------------
        cnxn = pyodbc.connect(f'DRIVER={drive};SERVER={server};DATABASE={database};UID={user};PWD={passw}')
        cursor = cnxn.cursor()
        cursor.execute(sql)
        while row:
            print(row[6])
            row = cursor.fetchone()
    def execsql_select_connectorx(self, sql):
        try:
            conn = self.Flag.conexaoVI()
            dados = cx.read_sql(conn, sql)
            return dados
        except pyodbc.Error as err:
            print(f"Erro: '{err}'")
    def execsql_select(self, sql):
        try:
            conn = self.Flag.conexao()
            dados = pd.read_sql(text(sql), conn)
            conn.close()
            return dados
        except pyodbc.Error as err:
            print(f"Erro: '{err}'")
    def execsql_select_sys(self, sql):
        try:
            conn = self.Flag.conexaoIV()
            dados = pd.read_sql(text(sql), conn)
            conn.close()
            return dados
        except pyodbc.Error as err:
            print(f"Erro: '{err}'")

    # Inserindo ---------------------------------------
    def execsq_insert_update_sys(self, sql):
        conn = self.Flag.conexaoIV()

        conn.execute(sql)
        conn.execute(text(sql)).rowcount
        conn.commit()
        conn.close()

    def execsq_insert_update(self, sql):
        conn = self.Flag.conexao()
        conn.execute(text(sql)).rowcount
        conn.commit()
        conn.close()

