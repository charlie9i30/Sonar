import sys
import time
import fitz
# python utils/carimbo.py dist/ass_9.png;dist/curriculo.pdf;dist/curriculo_assinado.pdf;1

print('Começando...')
ass = (sys.argv[1].split(";"))[0]
pdf1 = (sys.argv[1].split(";"))[1]
pdf2 = (sys.argv[1].split(";"))[2]
pos = int((sys.argv[1].split(";"))[3])

# image file
img = open(ass, "rb").read()

# img_xref = 0
print('Abriu a imagem de assinatura')

# retrieve the first page of the PDF
doc = fitz.open(pdf1)
# loop pages for stamp image
print(fitz)
for page in doc:

    if not (page.is_wrapped):
        page.wrap_contents()

    page.clean_contents()
    # Get the current dimensions of the page
    page_width = page.rect.width
    page_height = page.rect.height
    page_tes = page.insert_image
    print('page_width:' + page_width.__str__())
    print('page_height:' + page_height.__str__())

    # inferior esquerdo - default
    # left = float((page_width / 12) - 15)
    # center = 300
    # right = float((2 * (page_width / 12)) - 15)
    # bottom = 200
    # top = page_height + (page_height / 6)

    # left = float((page_width / 12) - 15)
    # center = float((page_width / 12) + 40)
    # right = float((2 * (page_width / 12)) - 15)
    # bottom = 200
    # top = page_height + (page_height / 6)

    # A rectangle is called finite if x0 <= x1 and y0 <= y1 (i.e. the bottom
    # right point is “south-eastern” to the top left one), otherwise infinite. # Of the four alternatives above, only one is finite (disregarding
    # degenerate cases).
    # A rectangle is called empty if x0 = x1 or y0 = y1, i.e. if its area is zero.
    largura = float((page_width))
    altura = float((page_height))
    # print('Largura: ' + largura.__str__())
    # print('Altura: ' + altura.__str__())

    if pos == 1:
        # superior esquerdo - default (x0, y0, x1, y1)
        x0_img = (largura * 0.017)
        y0_img = (altura * 0.25)
        x1_img = (largura * (-0.50))
        y1_img = (altura * 0.48)

        rect1 = fitz.Rect(x0_img, x1_img, y0_img, y1_img) # Assinatura
        page.insert_image(rect1, stream=img) # , xref=img_xref
        rect2 = fitz.Rect(x0_img + (largura * 0.084), y0_img + (altura * (-0.18)), page_width, page_height) # Data
        page.insert_textbox(rect2, time.strftime("%d/%m/%Y %H:%M:%S"), fontsize=9, fontname='Times-Roman')

    if pos == 2:  # OK
        # superior meio
        x0_img = (largura * 0.35)
        y0_img = (altura * 0.50)
        x1_img = (largura * (-0.50))
        y1_img = (altura * 0.48)

        rect1 = fitz.Rect(x0_img, x1_img, y0_img, y1_img) # Assinatura
        page.insert_image(rect1, stream=img)
        rect2 = fitz.Rect(x0_img + (largura * 0.084), y0_img + (altura * (-0.43)), page_width, page_height) # Data
        page.insert_textbox(rect2, time.strftime("%d/%m/%Y %H:%M:%S"), fontsize=9, fontname='Times-Roman')

    if pos == 3:
        # superior direito
        x0_img = (largura * 0.68)
        y0_img = (altura * 0.75)
        x1_img = (largura * (-0.50))
        y1_img = (altura * 0.475)
        rect1 = fitz.Rect(x0_img, x1_img, y0_img, y1_img) # Assinatura
        page.insert_image(rect1, stream=img)
        rect2 = fitz.Rect(x0_img + (largura * 0.084), y0_img + (altura * (-0.68)), page_width, page_height) # Data
        page.insert_textbox(rect2, time.strftime("%d/%m/%Y %H:%M:%S"), fontsize=9, fontname='Times-Roman')

    if pos == 4:
        # centro esquerdo - default
        x0_img = (largura * 0.017)
        y0_img = (altura * 0.25)
        x1_img = (largura * 0.67)
        y1_img = (altura * 0.63)
        rect1 = fitz.Rect(x0_img, x1_img, y0_img, y1_img) # Assinatura
        page.insert_image(rect1, stream=img)
        rect2 = fitz.Rect(x0_img + (largura * 0.084), y0_img + (altura * 0.34), page_width, page_height) # Data
        page.insert_textbox(rect2, time.strftime("%d/%m/%Y %H:%M:%S"), fontsize=9, fontname='Times-Roman')

    if pos == 5:
        # centro centro - default
        x0_img = (largura * 0.35)
        y0_img = (altura * 0.50)
        x1_img = (largura * 0.67)
        y1_img = (altura * 0.63)
        rect1 = fitz.Rect(x0_img, x1_img, y0_img, y1_img)  # Assinatura
        page.insert_image(rect1, stream=img)
        rect2 = fitz.Rect(x0_img + (largura * 0.084), y0_img + (altura * 0.088), page_width, page_height) # Data
        page.insert_textbox(rect2, time.strftime("%d/%m/%Y %H:%M:%S"), fontsize=9, fontname='Times-Roman')

    if pos == 6:
        # centro direito - default Ok
        x0_img = (largura * 0.68)
        y0_img = (altura * 0.75)
        x1_img = (largura * 0.67)
        y1_img = (altura * 0.63)
        rect1 = fitz.Rect(x0_img, x1_img, y0_img, y1_img)  # Assinatura
        page.insert_image(rect1, stream=img)
        rect2 = fitz.Rect(x0_img + (largura * 0.084), y0_img + (altura * (-0.15)), page_width, page_height) # Data
        page.insert_textbox(rect2, time.strftime("%d/%m/%Y %H:%M:%S"), fontsize=9, fontname='Times-Roman')

    if pos == 7:
        # inferior direito - default
        x0_img = (largura * 0.017)
        y0_img = (altura * 0.25)
        x1_img = largura
        y1_img = altura
        rect1 = fitz.Rect(x0_img, x1_img, y0_img, y1_img)  # Assinatura
        page.insert_image(rect1, stream=img)
        rect2 = fitz.Rect(x0_img + (largura * 0.084), y0_img + (altura * 0.66), page_width, page_height) # Data
        page.insert_textbox(rect2, time.strftime("%d/%m/%Y %H:%M:%S"), fontsize=9, fontname='Times-Roman')
        # print(page_height - 45)
    if pos == 8:
        # inferior centro - default
        x0_img = (largura * 0.35)
        y0_img = (altura * 0.50)
        x1_img = largura
        y1_img = altura
        rect1 = fitz.Rect(x0_img, x1_img, y0_img, y1_img)  # Assinatura
        page.insert_image(rect1, stream=img)
        rect2 = fitz.Rect(x0_img + (largura * 0.084), y0_img + (altura * 0.41), page_width, page_height) # Data
        page.insert_textbox(rect2, time.strftime("%d/%m/%Y %H:%M:%S"), fontsize=9, fontname='Times-Roman')

    if pos == 9:
        # inferior direito - default
        x0_img = (largura * 0.68)
        y0_img = (altura * 0.75)
        x1_img = largura
        y1_img = altura
        rect1 = fitz.Rect(x0_img, x1_img, y0_img, y1_img)  # Assinatura
        page.insert_image(rect1, stream=img)
        rect2 = fitz.Rect(x0_img + (largura * 0.084), y0_img + (altura * 0.17), page_width, page_height) # Data
        page.insert_textbox(rect2, time.strftime("%d/%m/%Y %H:%M:%S"), fontsize=9, fontname='Times-Roman')


doc.save(pdf2)
#sucesso

print('Sucesso')

